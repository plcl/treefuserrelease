//
//  FuseTransformation.cpp
//  libtooling
//
//  Created by lsakka on 4/4/16.
//  Copyright Â© 2016 lsakka. All rights reserved.
//

#include "fuse_transformation.h"
#include "dependence_analyser.h"
#include "dependence_graph.h"
#include "treefuse_writer.h"
#include <algorithm>
#include <ctime>



bool CanFuseCandidateSearchTraversal::VisitFunctionDecl(FunctionDecl * funcDecl){
    
    currentFunDecl=funcDecl;
    //cout<<"inside"<<currentFunDecl->getNameAsString()<<endl;
    return true;
}

bool CanFuseCandidateSearchTraversal::VisitCompoundStmt(CompoundStmt * compundStmt){
    
    vector<clang::CallExpr *>  canFuseList;
    
    for(clang::CompoundStmt::const_body_iterator it= compundStmt->body_begin();it!=compundStmt->body_end();it++){
        
        if((*it)->getStmtClass() == clang::Stmt::StmtClass::CallExprClass ){
            
            clang::CallExpr * currentStmt = dyn_cast<clang::CallExpr>(*it);
            
            //why i am doing this?
            if(canFuseList.size()==0){
                if( areValidCandidates(currentStmt,currentStmt) ){
                    canFuseList.push_back(currentStmt);
                }
            }
            else{
                if(  areValidCandidates(canFuseList[0],currentStmt)){
                    canFuseList.push_back(currentStmt);

                }else{
                    fuse(canFuseList);

                }
            }
        }else{
            
            fuse(canFuseList);
        }
    }
    fuse(canFuseList);
    return true;
}

bool CanFuseCandidateSearchTraversal::areValidCandidates(clang::CallExpr * call1, clang::CallExpr * call2){
    
    //decl based can fuse is true for both of them
    //the both start traversing the tree from the same point
    //HACK!
    if(call1->getCalleeDecl()==NULL || call2->getCallee()==NULL)
        return false;
    clang::FunctionDecl* call1Decl=dyn_cast  <clang::FunctionDecl> (call1->getCalleeDecl());
    clang::FunctionDecl * call2Decl=dyn_cast <clang::FunctionDecl> (call2->getCalleeDecl());
    
    //both are valud fuse
    if(!this->functionsInfo_->isValidFuse(call1Decl)||
       !this->functionsInfo_->isValidFuse(call2Decl))
        return false;
    
    
    
    //both operate on the same tree
    AccessPath p1(call1->getArg(0),NULL);//dummmy access path
    AccessPath p2(call2->getArg(0),NULL);//dummmy access path
    
    if(p1.splittedAccessPath.size()!= p2.splittedAccessPath.size())
        return false;
    
    for(int i=0; i<p1.splittedAccessPath.size(); i++){
        if(p1.splittedAccessPath[i].second!=p2.splittedAccessPath[i].second)
            return false;
    }
    // Logger::getStaticLogger().log(p1.getAsStr()+","+p2.getAsStr()+"\n");
    
    return true ;
    
}

bool allPredVisited(map<DG_Node * , bool> & visisted, DG_Node * node){
    
    if(node->isMerged){
        
        for(set<DG_Node *>::iterator it=node->mergeInfo->mergedNodes.begin();it!=node->mergeInfo->mergedNodes.end() ; it++)
        {
            
            for(map<DG_Node *,DepInfo>::iterator it2=(*it)->predList.begin();
                it2!=(*it)->predList.end(); it2++)
            {
                //the node is part of the merged nodes
                if(node->mergeInfo->isInMergedNodes( it2->first) )
                    continue;
                
                else
                {
                    if(visisted[it2->first]==false)
                        return false;
                }
            }
        }
        
    }
    else{
        
        for(map<DG_Node * , DepInfo>::iterator it=node->predList.begin(); it!=node->predList.end(); it++)
        {
            
            if(visisted[it->first]==false)
                return false;
        }
    }
    return true;
}

void mergeAllCalls(DDG * depGraph){
    
    map< clang::FieldDecl * ,vector<DG_Node * > > tmpMap;
    for(int i=0; i<depGraph->nodes.size();i++){
        if(depGraph->nodes[i]->StatmentInfo->isCallStmt){
            tmpMap[depGraph->nodes[i]->StatmentInfo->calledChild].push_back(depGraph->nodes[i]);
        }
    }
    
    for( map< clang::FieldDecl * ,vector<DG_Node * > >::iterator it=tmpMap.begin(); it!=tmpMap.end(); it++)
    {
        for(int i=1; i<it->second.size(); i++){
            depGraph->merge(it->second[i-1], it->second[i]);
        }
        
    }
    
}

void greedy1(DDG * depGraph){
    
    map< clang::FieldDecl * ,vector<DG_Node * > > calledChildToCallNodes;
    for(int i=0; i<depGraph->nodes.size();i++){
        if(depGraph->nodes[i]->StatmentInfo->isCallStmt){
            calledChildToCallNodes[depGraph->nodes[i]->StatmentInfo->calledChild].push_back(depGraph->nodes[i]);
        }
    }
    
    vector<map< clang::FieldDecl * ,vector<DG_Node * > >::iterator > itList;
    
    for( map< clang::FieldDecl * ,vector<DG_Node * > >::iterator it=calledChildToCallNodes.begin(); it!=calledChildToCallNodes.end(); it++)
    {
        itList.push_back(it);
        
    }
    //   srand ( time(NULL) );
    //  random_shuffle(itList.begin(), itList.end());
    //  random_shuffle(itList.begin(), itList.end());
    
    
    for(int ii=0; ii<itList.size(); ii++){
        
        map< clang::FieldDecl * ,vector<DG_Node * > >::iterator it=itList[ii];
        
        vector<DG_Node * > & nodeLst=it->second;
        
        reverse(nodeLst.begin(), nodeLst.end());
        
        //   random_shuffle(nodeLst.begin(), nodeLst.end());
        //  random_shuffle(nodeLst.begin(), nodeLst.end());
        // random_shuffle(nodeLst.begin(), nodeLst.end());
        for(int i=0; i<nodeLst.size(); i++){
            if(nodeLst[i]->isMerged)
                continue;
            
            else{
                for(int j=i+1; j<nodeLst.size(); j++){
                    if(nodeLst[j]->isMerged)
                        continue;
                    else{
                        depGraph->merge(nodeLst[i], nodeLst[j]);
                        if(depGraph->hasCycle()|| depGraph->hasWrongFuse(nodeLst[i]->mergeInfo)){
                            //  cout<<"unmerge!"<<endl;
                            depGraph->unmerge(nodeLst[j]);
                        }
                    }
                    
                }
                
            }
            
        }
    }
}

void findToplogicalOrder_rec(vector<DG_Node*  > & topOrder,  map<DG_Node * , bool>& visited,DG_Node * node){
    
    if(!allPredVisited(visited,node))
        return;
    
    
    topOrder.push_back(node);
    if(node->isMerged){
        
        //mark all merged nodes as visited
        for(set<DG_Node *>::iterator it=node->mergeInfo->mergedNodes.begin();
            it!=node->mergeInfo->mergedNodes.end() ; it++){
            
            assert(visited[*it]==false);
            visited[*it]=true ;
            
            
        }
        
        for(set<DG_Node *>::iterator it=node->mergeInfo->mergedNodes.begin();it!=node->mergeInfo->mergedNodes.end() ; it++)
        {
            for(map<DG_Node *,DepInfo>::iterator it2=(*it)->succList.begin(); it2!=(*it)->succList.end(); it2++)
            {
                
                //the node is part of the merged nodes
                if(node->mergeInfo->isInMergedNodes( it2->first) )
                    continue;
                else
                {
                    
                    //WRONG ASSERTION
                    if(visited[it2->first]==false)
                        findToplogicalOrder_rec(topOrder,visited,it2->first);
                }
            }
        }
        
    }else{
        
        visited[node]=true;
        for(map<DG_Node *,DepInfo>::iterator it=node->succList.begin(); it!=node->succList.end(); it++)
        {
            if(visited[it->first]==false)
                findToplogicalOrder_rec(topOrder, visited, it->first);
        }
    }
}

vector<DG_Node*  > findToplogicalOrder(DDG * depGraph){
    
    map<DG_Node * , bool> visited;
    vector<DG_Node*  > topOrder;
    
    
    
    bool allVisited=false;
    while(allVisited==false)
    {
        allVisited=true;
        for(int i=0; i<depGraph->nodes.size(); i++ )
        {
            if(visited[depGraph->nodes[i]]==false)
            {
                allVisited=false;
                findToplogicalOrder_rec(topOrder, visited, depGraph->nodes[i]);
                
            }
        }
    }
    return topOrder;
}

void CanFuseCandidateSearchTraversal::fuse(vector<clang::CallExpr *> & canFuseList){
    
    if(canFuseList.size()>1){
        //cout<<canFuseList.size()<<endl;
        Logger::getStaticLogger().logInfo("creating DDG for can Fuse List");
        DependenceAnalyser depAnalyser;
        
        //map each call in the fuse list to its FunctionAnalyser corresponding object!
        vector<FunctionAnalyser * > tList;
        
        for(int i=0; i<canFuseList.size(); i++){
            clang::FunctionDecl * tmp= dyn_cast  <clang::FunctionDecl> (canFuseList[i]->getCalleeDecl());
            tList.push_back(this->functionsInfo_->functionsInformation[tmp]);
        }
        
        
        //create dependence graph
        DDG * depGraph=depAnalyser.createDependnceGraph(tList);

        //depGraph->dump();
        // depGraph->dumpToPrint();
        //perform fusion (move to Treefuse synthesizer)
        
        //perfor greedy fusion TODO: rename lol
        greedy1(depGraph);
        depGraph->dumpMergeInfo();
        
        //check no that no illegal fusion is there.
        if(depGraph->hasCycle()){
            Logger::getStaticLogger().logError("dep graph as cycle");
            
        }else if(depGraph->hasWrongFuse()){
            Logger::getStaticLogger().logError("dep graph has wrong merging");
            
        }
        else{
            
            //generate a topological sort
            Logger::getStaticLogger().logDebug("generating topological sort");
            
            vector<DG_Node* > topOrder=findToplogicalOrder(depGraph);
            
            //write back the new source code
            
            //writeTransformation(canFuseList, topOrder, this->context, rewriter, currentFunDecl);
            
             TreeFuseWriter fuseWriter(canFuseList,this->currentFunDecl, this->context,this->rewriter,topOrder);
             fuseWriter.writeBackFusedVersion();
            // writeTransformation(canFuseList, topOrder, this->context, this->rewriter,this->currentFunDecl);
        }
        
    }
    
      canFuseList.clear();
}



