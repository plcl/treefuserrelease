//
//  accesspath.cpp
//  libtooling
//
//  Created by lsakka on 1/7/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#include "access_path.h"
#include "function_analyser.h"



AccessPath::AccessPath(clang::Expr * s,FunctionAnalyser * enclosingFunction_, StrictAccessInfo * attribute){
    
    s=s->IgnoreImplicit();
    accessPathStartNode=s;
    enclosingFunction=enclosingFunction_;
    if(enclosingFunction_==NULL)
        isDummy=true;
    
    isDeclAccessPath=false;
    
    if(s->getStmtClass()==clang::Stmt::StmtClass::MemberExprClass )
        this->collectAPTerms_VisitMemberExpr(dyn_cast<clang::MemberExpr>(s));
    
    else if(s->getStmtClass()==clang::Stmt::StmtClass::CXXMemberCallExprClass){
        assert( attribute != NULL );
        clang::CXXMemberCallExpr * cxxMemberCall = dyn_cast<clang::CXXMemberCallExpr>(s);
        if(! hasStrictAccessAnnotation( cxxMemberCall->getCalleeDecl())){
            this->_isLegal=false;
            Logger::getStaticLogger().logError("CXXMemberCallExpr must has strict_access attribute with onTree flag.. function calls with no strict access annotation are not allowed");
            
        }else{
            
            if(attribute->isGlobal){
                s->dump();
                Logger::getStaticLogger().logError("CXXMemberCallExpr must has strict_access attribute with isGlobal=0 flag");
                this->_isLegal=false;
            }
            else{
                //no need to append any sybomols
                StrictAccessInfo  attr=*attribute;
                
                this->attr=attr;
                this->isStrictAccessCall=true;
                this->accessPath= "strictOnTreeAccess"+to_string(attr.id)+"("+cxxMemberCall->getCalleeDecl()->getAsFunction()->getNameAsString()+")";
                //just go and read the Tree Access Part
                int i=0;
                
                
                for(clang::Stmt::child_iterator it2=(*cxxMemberCall->child_begin())->child_begin();it2!=(*cxxMemberCall->child_begin())->child_end(); it2++){
                    // not really a loop haha
                    assert(i==0);
                    
                    if((*it2)->IgnoreImplicit()->getStmtClass()==clang::Stmt::StmtClass::MemberExprClass)
                        collectAPTerms_VisitMemberExpr(dyn_cast<clang::MemberExpr>((*it2)->IgnoreImplicit()));
                    
                    else if((*it2)->IgnoreImplicit()->getStmtClass()==clang::Stmt::StmtClass::DeclRefExprClass){
                        collectAPTerms_VisitDeclRefExpr(dyn_cast<clang::DeclRefExpr>((*it2)->IgnoreImplicit()));
                        
                    }else{
                        Logger::getStaticLogger().logError("AccessPath::AccessPath unsupported type inside access path after CXXMemberCallExpr:"+string((*it2)->IgnoreImplicit()->getStmtClassName()));
                        _isLegal=false;
                        
                        i++;
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    else if(s->getStmtClass()==clang::Stmt::StmtClass::DeclRefExprClass )
        this->collectAPTerms_VisitDeclRefExpr(dyn_cast<clang::DeclRefExpr>(s));
    else{
        
        Logger::getStaticLogger().logError("AccessPath::AccessPath : unsupported stmtclass in accesspath: "+string(s->getStmtClassName()));
        this->_isLegal=false;
    }
    
    if(!this->_isLegal){
        Logger::getStaticLogger().logError("AccessPath::AccessPath :access path is not legal");
    }
    
    if(!isDummy){
        setValueStartIndex();
        
        
        //special check
        //Too many hacks this is to make sure that if the accespath is onTree it is really on Tree
        if(!isOnTree() && isStrictAccessCall && !attr.isGlobal){
            Logger::getStaticLogger().logError("AccessPath::Accesspath on tree restrict access  is not on tree!!");
            _isLegal=false;
        }
        if(isOnTree() && isStrictAccessCall){
            //all accesses or on some child and starts from the root
            
            if(splittedAccessPath[0].second!=this->enclosingFunction->rootDecl){
                Logger::getStaticLogger().logError("AccessPath::Accesspath on tree restrict accesse is not on tree!!");
                _isLegal=false;
            }
            for(int i=1; i<splittedAccessPath.size(); i++){
                if( (!this->enclosingFunction->isInChildList(splittedAccessPath[i].second))){
                    Logger::getStaticLogger().logError("AccessPath::Accesspath on tree restrict accesse is not on tree!!");
                    //    splittedAccessPath[i].second->dump();
                    _isLegal=false;
                }
            }
            
            
            
        }
        //check that all tree accesses used in the access path are child members
        for(int i=1; i<this->getValueStartIndex(); i++){
            if( (!this->enclosingFunction->isInChildList(splittedAccessPath[i].second))){
                Logger::getStaticLogger().logError("AccessPath::VisitMemberExpr not tree node access");
                //    splittedAccessPath[i].second->dump();
                _isLegal=false;
            }
        }
    }
}

AccessPath::AccessPath(clang::VarDecl * decl,FunctionAnalyser * enclosingFunction_){
    
    enclosingFunction=enclosingFunction_;
    
    if(enclosingFunction_==NULL)
        isDummy=true;
    
    this->accessPath=decl->getNameAsString();
    this->splittedAccessPath.push_back(make_pair(decl->getNameAsString(), decl));
    
    valueStartIndex=0;
    isDeclAccessPath=true;
}

AccessPath::AccessPath(clang::FunctionDecl * decl,FunctionAnalyser * enclosingFunction_){
    
    enclosingFunction=enclosingFunction_;
    
    if(enclosingFunction_==NULL)
        isDummy=true;
    
    // this->attr=getStrictAccessInfo( decl);
    
    this->accessPath="strictOffTreeAccess"+to_string(attr.id)+"("+decl->getNameAsString()+")";
    //this->splittedAccessPath.push_back(make_pair(decl->getNameAsString(), decl));
    valueStartIndex=0;
    isStrictAccessCall=true;
}




bool AccessPath::hasValuePart()const{
    return valueStartIndex != -1;
}
bool AccessPath::isLegal()const{
    return _isLegal ;
}
int AccessPath::getValueStartIndex()const{
    return valueStartIndex;
}
bool AccessPath::isOnTree()const{
    return _isLegal && valueStartIndex!=0;
}
bool AccessPath::isOffTree()const{
    return  _isLegal && valueStartIndex==0;
    
}
bool AccessPath::isLocal()const{
    return !isStrictAccessCall && _isLegal && isOffTree()  && splittedAccessPath[0].second->getDeclContext()->isFunctionOrMethod ();
}
bool AccessPath::isGlobal()const{
    return _isLegal && isOffTree() && !isLocal();
}

bool AccessPath::onlyUses(  clang::VarDecl * rootDecl,  const set<clang::FieldDecl *> &  uniqueChildSymbol){
    
    if((splittedAccessPath[0].second != rootDecl))
        return false;
    
    for(int i=1; i<splittedAccessPath.size(); i++){
        if(!clang::ValueDecl::classofKind(clang::Decl::Kind::Field)){
            //WHAT IS THIS!
            cout<<"tmp log:<<not only uses becouse of non field access decl"<<endl;
        }
        if(uniqueChildSymbol.find(dyn_cast<clang::FieldDecl>(splittedAccessPath[i].second))==uniqueChildSymbol.end())
            return false;
    }
    
    
    return true;
}

int AccessPath::getValuePathSize()const{
    if(valueStartIndex==-1)
        return 0;
    else
        return splittedAccessPath.size()-valueStartIndex;
}
int AccessPath::getTreeAccessPathSize()const{
    if(valueStartIndex==-1)
        return splittedAccessPath.size();
    else
        return valueStartIndex;
    
}
int AccessPath::getTreeAccessPathEndIndex()const{
    if(valueStartIndex==-1)
        return splittedAccessPath.size()-1;
    else
        return valueStartIndex-1;
    
}
clang::ValueDecl * AccessPath::getDeclAtIndex(int i) const{
    return splittedAccessPath[i].second;
    
}
const string & AccessPath::getAsStr() const{
    return accessPath;
}

int AccessPath::getDepth() const{
    
    return  splittedAccessPath.size();
    
}

void AccessPath::setValueStartIndex(){
    
    for(int i=0; i<splittedAccessPath.size(); i++){
        clang::ValueDecl * decl=splittedAccessPath[i].second;
        
        if(enclosingFunction->rootDecl!=decl && ! enclosingFunction->isInChildList(decl)){
            valueStartIndex=i;
        }
    }
}



/*
 bool AccessPath::isChildAccessSymbol(string symbol)
 {
 for(int i=0; i<enclosingFunction->orderedCalledChildsList.size(); i++){
 
 if(enclosingFunction->orderedCalledChildsList[i].compare(symbol)==0)
 return  true;
 }
 return false;
 }*/
/*
 bool AccessPath::isChildDecl(string symbol)
 {
 for(int i=0; i<enclosingFunction->orderedCalledChildsList.size(); i++){
 
 if(enclosingFunction->orderedCalledChildsList[i].compare(symbol)==0)
 return  true;
 }
 return false;
 }*/
/*
 bool AccessPath::isRootAccessSymbol(string symbol)
 {
 return enclosingFunction->getRootSymbol().compare(symbol)==0;
 
 }
 */



void AccessPath::appendSymbol(clang::ValueDecl * declAccess){
    
    string accessSymbol=declAccess->getNameAsString();
    
    if(!isDummy){
        
        //rename root symbol
        if(this->enclosingFunction->rootDecl == declAccess){
            accessSymbol="^";
        }
    }
    if(splittedAccessPath.size()!=0|| this->isStrictAccessCall){
        accessPath=accessSymbol+"."+accessPath;
        splittedAccessPath.insert(splittedAccessPath.begin(), make_pair(accessSymbol, declAccess));
        
    }else{
        accessPath=accessSymbol;
        splittedAccessPath.insert(splittedAccessPath.begin(), make_pair(accessSymbol, declAccess));
        
    }
}


bool AccessPath::collectAPTerms_VisitMemberExpr(clang::MemberExpr * S){
    if(S->getMemberDecl()->isFunctionOrFunctionTemplate()){
        _isLegal=false;
        Logger::getStaticLogger().logError("functions not allowed in access paths inside fuse methods");
        return false;
    }else{
        //change tp Post order to increase eppend performance<<o(N) to o(1)
        appendSymbol(S->getMemberDecl());
        
    }
    int i=0;
    for(clang::Stmt::child_iterator it=S->child_begin();it!=S->child_end(); it++){
        
        assert(i==0);
        if((*it)->IgnoreImplicit()->getStmtClass()==clang::Stmt::StmtClass::MemberExprClass)
            collectAPTerms_VisitMemberExpr(dyn_cast<clang::MemberExpr>((*it)->IgnoreImplicit()));
        
        else if((*it)->IgnoreImplicit()->getStmtClass()==clang::Stmt::StmtClass::DeclRefExprClass){
            collectAPTerms_VisitDeclRefExpr(dyn_cast<clang::DeclRefExpr>((*it)->IgnoreImplicit()));
            
        }else{
            Logger::getStaticLogger().logError("AccessPath::VisitMemberExpr unsupported type inside access path:"+string((*it)->IgnoreImplicit()->getStmtClassName()));
            _isLegal=false;
            return false;
        }
        
        i++;
    }
    
    return false;
    
}

bool AccessPath::collectAPTerms_VisitDeclRefExpr(clang::DeclRefExpr * S){
    appendSymbol(S->getDecl());
    assert(S->child_begin()==S->child_end());
    return true;
}


//return true if this access path represents a tree node that uses the provided child symbols and the root symbol

bool accessPath_compare::operator()(const AccessPath *lhs, const AccessPath *rhs) const {
    bool areSame=true;
    if(lhs->splittedAccessPath.size()!=rhs->splittedAccessPath.size()){
        areSame=false;
    }else{
        for(int i=0; i<lhs->splittedAccessPath.size();i++ )
        {
            if(lhs->splittedAccessPath[i].second!=rhs->splittedAccessPath[i].second)
                areSame=false;
        }
        
    }
    
    if(areSame){
        //make sure they are not both
        //this means they have same splittedAccessPath
        if(lhs->isStrictAccessCall && rhs->isStrictAccessCall){
            
            if(lhs->isOnTree() && rhs->isOnTree()){
                
                if(lhs->attr.id!=rhs->attr.id)
                    areSame=false;
            }
            else if(lhs->isOffTree() && rhs->isOffTree()){
                
                if(lhs->attr.id!=rhs->attr.id)
                    areSame=false;
            }
            else{//they have different types
                areSame=false;
            }
        }
        //one of them is strict access
        else if(lhs->isStrictAccessCall || rhs->isStrictAccessCall)
            areSame=false;
        
    }
    if(areSame){
        return false;
    }
    
    return lhs<rhs;
}
