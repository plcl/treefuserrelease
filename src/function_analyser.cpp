//
//  FunctionAnalyser.cpp
//  libtooling
//
//  Created by lsakka on 12/16/15.
//  Copyright © 2015 lsakka. All rights reserved.
//


//update this for better precision and things

#include "function_analyser.h"
#include "logger.h"
#include "access_path.h"
#include <vector>
#include <set>
#include <utility>
#include <sstream>
#include <string>
#include "record_analyser.h"



int FunctionAnalyser::getNumberOfRecursiveCalls()const{
    return calledChildrenOrderedList.size();
}

clang::FunctionDecl * FunctionAnalyser::getFunctioneDecl() const {
    return this->funDecNode;
}

const clang::VarDecl * FunctionAnalyser::getRootDecl() const {
    return rootDecl;
}

const clang::RecordDecl * FunctionAnalyser::getRootRecordDecl() const {
    return rootNodeRecordDecl;
}

const vector<clang::FieldDecl *> &  FunctionAnalyser::getCalledChildsList()const {
    return calledChildrenOrderedList;
}

FunctionAnalyser::FunctionAnalyser(clang::FunctionDecl * funDeclaration){
        this->funDecNode=funDeclaration;
        semanticsStasified=checkFuseSema();
  
        return;
    }

bool FunctionAnalyser::isInCalledChildList(clang::FieldDecl * childDecl )const {
    for(int i=0; i<calledChildrenOrderedList.size(); i++){
        if(childDecl== calledChildrenOrderedList[i]){
            return true;
        }
    }
    return false;
}

bool FunctionAnalyser::isInChildList(clang::ValueDecl * decl) const{
    
    return RecordsAnalyser::getRecordInfo(rootNodeRecordDecl).isChildAccessDecl(decl);
    
}

//handle general  statmetnt not sub expression!
bool FunctionAnalyser::collectAccessPath_handleStmt(clang::Stmt * stmt){
    
    stmt=stmt->IgnoreImplicit();
    switch (stmt->getStmtClass()) {
        case clang::Stmt::CompoundStmtClass:
            
            if(collectAccessPath_VisitCompoundStmt(dyn_cast<clang::CompoundStmt>(stmt))==false)
                return false;
            
            break;
        case clang::Stmt::StmtClass::CallExprClass:
            if(collectAccessPath_VisitCallExpr(dyn_cast<clang::CallExpr >(stmt))==false)
                return false;
            
            break;
            
        case clang::Stmt::StmtClass::BinaryOperatorClass:
            if( collectAccessPath_VisitBinaryOperator(dyn_cast<clang::BinaryOperator >(stmt))==false)
                return false;
            
            break;
            
            
        case clang::Stmt::StmtClass::IfStmtClass:
            insideIfDepth++;
            if( collectAccessPath_VisitIfStmt(dyn_cast<clang::IfStmt >(stmt))==false){
                insideIfDepth--;
                return false;
            }
            insideIfDepth--;
            break;
        case clang::Stmt::StmtClass::ReturnStmtClass:
            curStatmentInfo->hasReturn=true;
            break;
            
        case clang::Stmt::Stmt::NullStmtClass:
            break;
            
        case clang::Stmt::StmtClass::DeclStmtClass:
            if(collectAccessPath_VisitDeclsStmt(dyn_cast<clang::DeclStmt >(stmt))==false)
                return false;
            
            break;
        case clang::Stmt::StmtClass::CXXMemberCallExprClass:
            if(collectAccessPath_VisitCXXMemberCallExpr(dyn_cast<clang::CXXMemberCallExpr >(stmt))==false)
                return false;
            
            break;
        default:
            
            //stmt->dump();
            Logger::getStaticLogger().logError("FunctionAnalyser::handleStmt: unsupported statment  ");
            return false;
            
            break;
    }
    
    return true;
}

bool FunctionAnalyser::collectAccessPath_VisitCXXMemberCallExpr(clang::CXXMemberCallExpr *s ){
    
    assert(s->getStmtClass()==clang::Stmt::StmtClass::CXXMemberCallExprClass);

        
    clang::CXXMemberCallExpr * cxxMemberCall = dyn_cast<clang::CXXMemberCallExpr>(s);
    if(! hasStrictAccessAnnotation( cxxMemberCall->getCalleeDecl())){
            Logger::getStaticLogger().logError("CXXMemberCallExpr must has strict_access attribute with onTree flag.. function calls with no strict access annotation are not allowed");
            
            return false;
    }
    
    vector<StrictAccessInfo>  attrList=getStrictAccessInfo( cxxMemberCall->getCalleeDecl());
    for (int i=0 ; i<attrList.size(); i++){
        StrictAccessInfo attr =attrList[i];
        AccessPath * tmp=new AccessPath(s, this,&attr);
        if(tmp->isLegal()==false){
            delete tmp;
            return false;
        }
        else{
            addAccessPath(tmp, !tmp->attr.isReadOnly);
        }
    }

        
    //add accesspaths for each of the arguments
    for(int i=0; i<s->getNumArgs(); i++){
        if(s->getArg(i)->IgnoreImplicit()->getStmtClass()==clang::Stmt::StmtClass::MemberExprClass ||
           s->getArg(i)->IgnoreImplicit()->getStmtClass()==clang::Stmt::StmtClass::DeclRefExprClass){
            
            AccessPath *  tmp=new AccessPath(s->getArg(i)->IgnoreImplicit(),this);
            addAccessPath(tmp, false);
            
            if(!tmp->isLegal()){
                delete tmp;
                return false;
            }
            
        }else{
            if(collectAccessPath_handleSubExpr(s->getArg(i))==false)
            {
                Logger::getStaticLogger().logError("FunctionAnalyser::collectAccessPath_VisitCallExpr :unsuppotrted argument type");
                return false;
            }
            
        }
    }
    
    return true;

}

bool FunctionAnalyser::collectAccessPath_handleSubExpr(clang::Expr * expr){
    
    expr=expr->IgnoreImplicit();
    switch (expr->getStmtClass()) {
            
        case clang::Stmt::BinaryOperatorClass:
            if(!collectAccessPath_VisitBinaryOperator(dyn_cast<clang::BinaryOperator>(expr)))
                return false;
            break;
            
        case clang::Stmt::StmtClass::CXXConstructExprClass:
        {
            
            CXXConstructExpr * constructor=dyn_cast<CXXConstructExpr >(expr);
            //means the its the default empty constructor
            return constructor->getConstructor()->isTrivial();
            
        }
        case clang::Stmt::StmtClass::ParenExprClass:{
            clang::ParenExpr *pExpr=dyn_cast<clang::ParenExpr>(expr);
            
            return collectAccessPath_VisitParenExpr(pExpr);
        }
            
        case clang::Stmt::StmtClass::GNUNullExprClass:
        case clang::Stmt::StmtClass::IntegerLiteralClass:
        case clang::Stmt::StmtClass::FloatingLiteralClass:
        case clang::Stmt::StmtClass::CXXBoolLiteralExprClass:
        case clang::Stmt::StmtClass::NullStmtClass:
            break;
       
        case clang::Stmt::StmtClass::CallExprClass:
            if(collectAccessPath_VisitCallExpr(dyn_cast<clang::CallExpr >(expr))==false)
                return false;
            
            break;
        case clang::Stmt::StmtClass::CXXMemberCallExprClass:
            if(collectAccessPath_VisitCXXMemberCallExpr(dyn_cast<clang::CXXMemberCallExpr >(expr))==false)
                return false;
            
            break;
        default:
            
            Logger::getStaticLogger().logError("FunctionAnalyser::handleSubExpr: unsupported subexpr :"+string(expr->getStmtClassName()));
            expr->dump();
            return false;
                }
    return true;
    
}

bool FunctionAnalyser::checkFuseSema(){
    //to be filled if the function is considered
    
    //must be a global function
    if(!funDecNode->isGlobal()){
        Logger::getStaticLogger().logError("error 0: Fuse method must be global ");
        return false;
    }
    
    //check Fuse Semantics
    //1- must have at least one parameter
    if(funDecNode->parameters().size()==0 ){
        Logger::getStaticLogger().logError("error 1: Fuse method first parameter must be pointer to attr(tree_node) ");
        return false;
        
    }
    
    //return type must be void
    if(!funDecNode->getReturnType()->isVoidType()){
        Logger::getStaticLogger().logError("error 1.1: Fuse method must be of type void");
        return false;
    }
    
    //first parameter must be pointer
    if(!funDecNode->parameters()[0]->getType()->isPointerType() ){
        Logger::getStaticLogger().logError("error 2: Fuse method first parameter must be pointer to attr(tree_node) ");
        return false;
        
    }
    
    
    //first parameter must be tree_node type
    this->rootNodeRecordDecl=funDecNode->parameters()[0]->getType()->getPointeeCXXRecordDecl();
    this->rootDecl=funDecNode->parameters()[0];
    
    if(!RecordsAnalyser::getRecordInfo(this->rootNodeRecordDecl).isTreeNode()){
        Logger::getStaticLogger().logError("error 2: Fuse method first parameter must be pointer to attr(tree_node) ");
        return false;
    }
    
    //2- all other parameters must be scalers
    for(int i=1; i<funDecNode->parameters().size(); i++){
        if(! RecordsAnalyser::isScaler(funDecNode->parameters()[i]  )){
            Logger::getStaticLogger().logError("error 2: Fuse method has nonscaler paramater ");

            return false;
        }
    }
    
    //3- must be a recursive method that calls itself
    for(clang::StmtIterator it= funDecNode->getBody()->child_begin();it!=funDecNode->getBody()->child_end(); it++){
        
        if((*it)->getStmtClass()==clang::Stmt::StmtClass::CallExprClass){
            clang::CallExpr * call=dyn_cast<clang::CallExpr >(*it);
            
            if(call->getCalleeDecl()==funDecNode)//calling itslef
            {
                
                ///TODO : change this into paremetrs is dummy, or into static method, create dymmy access path and
                //create access path 
                AccessPath arg0(call->getArg(0),NULL);//dummmy access path
                if(!arg0.isLegal()){
                    
                    Logger::getStaticLogger().logError("error 3.5: encounter illegal access path in self call");
                    return false;
                    
                }
                
                //it must be direct child
                if(arg0.getDepth()!=2){
                    Logger::getStaticLogger().logError("error 5: Fuse method recursive call first parameters is not on a first order child");
                    return false;
                }
                
                //check that it uses tree childs only and it starts from root
                if(!arg0.onlyUses(rootDecl,RecordsAnalyser::getChildAccessDecls(rootNodeRecordDecl))){
                    Logger::getStaticLogger().logError("error 4: Fuse method recursive call first parameters is a not child node on the tree, amke sure unique_child attr is there ");
                    return false;
                }
                
                
                
                clang::FieldDecl * childDecl=dyn_cast<clang::FieldDecl>(arg0.splittedAccessPath[1].second);
              //  assert(childDecl!=NULL);
                
                if(this->isInCalledChildList(childDecl)){
                    //Relaxed restriction June 19/*
                    //check that child node is not visited twice
                    // Logger::getStaticLogger().logError("error 6: Two Recursive calls Accessing visitng same child");
                    // return false;
                }
                else{
                    this->addToCalledChildList(childDecl);
                }
            }
            else if(hasStrictAccessAnnotation(call->getCalleeDecl())){
                //ignore all is good
            }
            else{
                
                Logger::getStaticLogger().logError("error 7: Fuse Methods Body not allowed to have function calls");
                return false;
            }
        }
        
    }
    
    //----------------------------------------------------------------
    if(this->getNumberOfRecursiveCalls()==0){
        Logger::getStaticLogger().logError("error 7:No recursive calls were found");
        return false;
    }
    
    
    //build access paths and check the rest of the semantics recursilvly
    
    
   // assert(funDecNode->getBody()->IgnoreImplicit()->getStmtClass()==clang::Stmt::StmtClass::CompoundStmtClass);
    
   return  collectAccessPath_VisitCompoundStmt(dyn_cast<clang::CompoundStmt>(funDecNode->getBody()));
 
    
}

void FunctionAnalyser::addAccessPath(AccessPath * accessPath, bool isWrite){
    
    bool res;
        res= curStatmentInfo->accessPaths.insert(accessPath, isWrite);
    
//    if(res)
//    Logger::getStaticLogger().logDebug("a new access path found is added ["+
//                                       this->funDecNode->getNameAsString()+
//                                       ( (isWrite==true)? ",write,":",read," )+
//                                        to_string(this->statments.size()+1)+
//                                       (accessPath->isOnTree()?",ontree":",offtree")+ "]:"+
//                                        accessPath->getAsStr());
//    
}

bool FunctionAnalyser::collectAccessPath_VisitBinaryOperator(clang::BinaryOperator * binaryExp){
    
    //check the right hand side
    if(binaryExp->getRHS()->IgnoreImplicit()->getStmtClass()==clang::Stmt::StmtClass::MemberExprClass ||
       binaryExp->getRHS()->IgnoreImplicit()->getStmtClass()==clang::Stmt::StmtClass::DeclRefExprClass){
        
        AccessPath *  tmp=new AccessPath(binaryExp->getRHS(),this);
        addAccessPath(tmp, false);
        
        if(!tmp->isLegal()){
            delete tmp;
            return false;
        }
    }else{
        if( collectAccessPath_handleSubExpr(binaryExp->getRHS())==false)
        {
            
            Logger::getStaticLogger().logError("Error1 in  FunctionAnalyser::collectAccessPath_VisitBinaryOperator : BIN RHS unsupported stmt class");
            return false;
        }
    }
    
    //check the left hand side
    if(binaryExp->getLHS()->IgnoreImplicit()->getStmtClass()==clang::Stmt::StmtClass::MemberExprClass ||
       binaryExp->getLHS()->IgnoreImplicit()->getStmtClass()==clang::Stmt::StmtClass::DeclRefExprClass){
        
        AccessPath *  tmp=new AccessPath(binaryExp->getLHS(),this);
        if(!tmp->isLegal()){
         
            delete tmp;
            return false;
        }
        
        //the only right case
        if(binaryExp->isAssignmentOp())
            addAccessPath(tmp, true);
        else
            addAccessPath(tmp, false);
        //not allwed to write to tree node (no value access)
        
        if( tmp->getValueStartIndex()==-1 && binaryExp->isAssignmentOp()){
            Logger::getStaticLogger().logError("collectAccessPath_VisitBinaryOperator: writing to tree nodes not allowed , not allowed access path");
            return false;
        }
        
    }else{
        if(collectAccessPath_handleSubExpr(binaryExp->getLHS())==false)
        {
            Logger::getStaticLogger().logError("Error1 in  FunctionAnalyser::collectAccessPath_VisitBinaryOperator : BIN LHS unsupported stmt class");
            return false;
        }
    }
    
    return true;
}

bool FunctionAnalyser::collectAccessPath_VisitCallExpr(clang::CallExpr * S){
    
    //can only call itself
    if(funDecNode!=S->getCalleeDecl()){
        
        //or strict call global
        if(hasStrictAccessAnnotation(S->getCalleeDecl())){
            // a special type of access paths is to be added here
            vector<StrictAccessInfo>  atrrList=getStrictAccessInfo(S->getDirectCallee());
            for(int i=0 ; i<atrrList.size(); i++){
                AccessPath* tmp = new AccessPath(S->getDirectCallee(), this);
                tmp->attr =atrrList[i];
                if(tmp->isLegal()==false){
                    delete tmp;
                    return false;
                }
                if(!tmp->attr.isGlobal){
                    S->dump();
                    Logger::getStaticLogger().logError("FunctionAnalyser::collectAccessPath_VisitCallExpr:  strict access call expr must be global for for non CXXMemberCallExpr ");
                    return false;
                }
                bool isWrite=false  ;
                
                if(!tmp->attr.isReadOnly)
                    isWrite=true;
                
                addAccessPath(tmp, isWrite);
            }
            
        }
        else{
            
            Logger::getStaticLogger().logError("FunctionAnalyser::collectAccessPath_VisitCallExpr:  this check must be reomevd from here , this is not the place for these checks Error1 in VisitCallExpr ");
            return false;
            
        }
    }
    if(funDecNode==S->getCalleeDecl() && insideIfDepth!=0){
        Logger::getStaticLogger().logError("FunctionAnalyser::collectAccessPath_VisitCallExpr: not allowed to have conditioned recursive call ");
        return false;
        
        
    }
    
    for(int i=0; i<S->getNumArgs(); i++){
        if(S->getArg(i)->IgnoreImplicit()->getStmtClass()==clang::Stmt::StmtClass::MemberExprClass ||
           S->getArg(i)->IgnoreImplicit()->getStmtClass()==clang::Stmt::StmtClass::DeclRefExprClass){
            
            AccessPath *  tmp=new AccessPath(S->getArg(i)->IgnoreImplicit(),this);
            addAccessPath(tmp, false);
            
            if(!tmp->isLegal()){
                delete tmp;
                return false;
            }
            
        }else{
            if(collectAccessPath_handleSubExpr(S->getArg(i))==false)
            {
                Logger::getStaticLogger().logError("FunctionAnalyser::collectAccessPath_VisitCallExpr :un suppotrted argument type");
                return false;
            }
            
        }
    }
    
    return true;
}

bool FunctionAnalyser::collectAccessPath_VisitCompoundStmt(clang::CompoundStmt* S){
    
    assert(S!=NULL);
    //chech each stmt alone
    clang::StmtIterator	 it=S->child_begin();
    while(it!=S->child_end()){
        
        
        //then we are in the main body becouse we are not inside and if statement
        
        if(this->insideIfDepth==0)
        {
            bool isRecursiveCall;
            
            if((*it)->IgnoreImplicit()->getStmtClass()==clang::Stmt::StmtClass::CallExprClass &&
               dyn_cast<clang::CallExpr>(*it)->getCalleeDecl()==this->funDecNode)
                isRecursiveCall=true;
            else
                isRecursiveCall=false;
            
            curStatmentInfo=new StatmentInfo((*it),this,isRecursiveCall,this->statments.size());
           
            this->statments.push_back(curStatmentInfo);
            
            if (isRecursiveCall){
                AccessPath arg0((dyn_cast<clang::CallExpr>(*it))->getArg(0),NULL);//dummmy access path
                curStatmentInfo->calledChild=dyn_cast<clang::FieldDecl>(arg0.splittedAccessPath[1].second);
            }

        }
    
        if(collectAccessPath_handleStmt(*it)==false)
            return false;
        
        it++;
    }
    
    return true;
}

bool FunctionAnalyser::collectAccessPath_VisitIfStmt(clang::IfStmt * S){
    
    //check the condition part first
    if(S->getCond()!=NULL ){
        
        clang::Expr * cond=S->getCond()->IgnoreImplicit();
        
        if(cond->getStmtClass()==clang::Stmt::StmtClass::MemberExprClass || cond->getStmtClass()==clang::Stmt::StmtClass::DeclRefExprClass){
            
            AccessPath *  tmp=new AccessPath(cond,this);
            addAccessPath(tmp, false);
            
            if(!tmp->isLegal()){
                delete tmp;
                return false;
            }
            
        }else{
            if(collectAccessPath_handleSubExpr(cond)==false)
            {
                Logger::getStaticLogger().logError("FunctionAnalyser::collectAccessPath_VisitIfStmt : unsupported expr inside condition part");
                return false;
            }
        }
    }
    //check then part
    if(S->getThen()!=NULL)
    {
        
        clang::Stmt * then=S->getThen()->IgnoreImplicit();
        
        if(collectAccessPath_handleStmt(then)==false)
        {
            Logger::getStaticLogger().logError("FunctionAnalyser::collectAccessPath_VisitIfStmt : unsupported stmt type in then part");
            return false;
        }
    }
    
    //check else part
    if(S->getElse()!=NULL)
    {
        clang::Stmt * _else=S->getElse()->IgnoreImplicit();
        
        if(collectAccessPath_handleStmt(_else)==false)
        {
            Logger::getStaticLogger().logError("FunctionAnalyser::collectAccessPath_VisitIfStmt : unsupported stmt type in else part");
            return false;
        }
    }
    return true;
    
}

bool FunctionAnalyser::collectAccessPath_VisitParenExpr(clang::ParenExpr * S){
    
    if(S->getSubExpr()->getStmtClass()==clang::Stmt::StmtClass::MemberExprClass){
        AccessPath * tmp= new AccessPath(S->getSubExpr(), this) ;
        
    
        if(tmp->isLegal()==false){
            delete tmp;
            return false;
        }
        addAccessPath(tmp, false);

    }else{
        if(collectAccessPath_handleSubExpr(S->getSubExpr())==false)
        {
            Logger::getStaticLogger().logError("FunctionAnalyser::collectAccessPath_VisitParenExpr: not allowed subexpression ");
            return false;
        }
   }
    return true;
}
bool FunctionAnalyser::collectAccessPath_VisitDeclsStmt(clang::DeclStmt * S){
   // S->dump();
    
    
  for(clang::DeclStmt::decl_iterator it=S->decl_begin(); it!=S->decl_end(); it++){
        
        clang::Decl * decl =*it;
        
        clang::VarDecl *  varDecl=dyn_cast<clang::VarDecl>(decl);
        
        assert(varDecl!=NULL);
        
        if(!(varDecl->getType()->isBuiltinType() || varDecl->getType()->isClassType() || varDecl->getType()->isStructureType())){
            Logger::getStaticLogger().logError("FunctionAnalyser::collectAccessPath_VisitDeclsStmt  : declaration  type is not allowed");
            return false;
        }
        
        //add write access path for declaration
        AccessPath * tmp= new AccessPath(varDecl, this) ;
      if(tmp->isLegal()==false){
          delete tmp;
          return false;
      }
        addAccessPath(tmp, true);
      
        
        //check the initialzing part
        clang::Expr * exprInit =varDecl->getInit();
        if(exprInit!=NULL){
            exprInit =varDecl->getInit()->IgnoreImpCasts();
            if(exprInit->getStmtClass()==clang::Stmt::StmtClass::MemberExprClass ||
               exprInit->getStmtClass()==clang::Stmt::StmtClass::DeclRefExprClass){
                
                AccessPath * tmp= new AccessPath(exprInit, this) ;
                
                addAccessPath(tmp, false);
                
                if(tmp->isLegal()==false)
                    return false;
                
                
            }
            else{
                if(collectAccessPath_handleSubExpr(exprInit)==false)
                {
                    Logger::getStaticLogger().logError("FunctionAnalyser::collectAccessPath_VisitDeclsStmt : declaration initialization not allowed ");
                    return false;
                }
            }
        }
    }
    return true ;
}

FunctionAnalyser::~FunctionAnalyser(){
    
    for(int i=0; i<= this->statments.size(); i++)
    {
        //before that delete access paths :DDD :D
        delete statments[i];
    }
    
}

//*****************************************************

