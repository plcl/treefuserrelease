//
//  AccessPathSet.cpp
//  libtooling
//
//  Created by lsakka on 2/10/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#include "access_path.h"

bool AccessPathSet::insert(AccessPath * accessPath,bool isWrite){
    
    if(isWrite)
        return writeSet.insert(accessPath).second;
    else
        return readSet.insert(accessPath).second;

}

void AccessPathSet::freeAccessPaths(){

    set<AccessPath * , accessPath_compare>::iterator it;
    for(it=readSet.begin(); it!=readSet.end(); it++)
        delete *it;
    
    for(it=writeSet.begin(); it!=writeSet.end(); it++)
        delete *it;    
}

