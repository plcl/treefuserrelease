//
//  Annotations.cpp
//  libtooling
//
//  Created by lsakka on 8/18/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#include <stdio.h>
#include "llvmdep.h"
#include "logger.h"

bool hasFuseAnnotation(FunctionDecl * funDecl){

    return funDecl->hasAttr<AnnotateAttr>() && (funDecl->getAttr<AnnotateAttr>()->getAnnotation().str().compare("fuse")==0)
    ;
}


bool hasTreeNodeAnnotation(clang::CXXRecordDecl * recordDecl){
    
  return  recordDecl->hasAttr<AnnotateAttr>() && recordDecl->getAttr<AnnotateAttr>()->getAnnotation().str().compare("TreeNode")==0
    ;
}

bool hasUniqueChildAnnotation(clang::FieldDecl * fieldDecl){
    
    return  fieldDecl->hasAttr<AnnotateAttr>() && fieldDecl->getAttr<AnnotateAttr>()->getAnnotation().str().compare("UniqueChild")==0
    ;
}



bool hasStrictAccessAnnotation(clang::Decl * decl){
    
    return  decl->hasAttr<AnnotateAttr>() && decl->getAttr<AnnotateAttr>()->getAnnotation().startswith("strict_access");
    ;
}

vector< StrictAccessInfo> getStrictAccessInfo(clang::Decl * decl){
    StringRef ref=    decl->getAttr<AnnotateAttr>()->getAnnotation();
   
    vector<StrictAccessInfo> strictAccessInfoVector;
    
    pair<StringRef, StringRef> splittedPair = ref.split('|');
    while (true){
        
        StringRef reftmp=splittedPair.first.split('(').second;
        StringRef par1=reftmp.split(',').first;
        
        
        reftmp=reftmp.split(',').second;
        StringRef par2=reftmp.split(',').first;
        reftmp=reftmp.split(',').second;
        StringRef par3=reftmp.split(')').first;
        
        StrictAccessInfo strictAccessInfoElement;
        
        if(par2.compare("'r'")==0)
            strictAccessInfoElement.isReadOnly=true;
        else if(par2.compare("'w'")==0)
            strictAccessInfoElement.isReadOnly=false;
        else{
            Logger::getStaticLogger().logError("strict access 2nd parameter can only be 'r' or 'w' .. will be conisidere 'w'");
            strictAccessInfoElement.isReadOnly=false;
        }
        
        strictAccessInfoElement.id=stoi(par1.str());
        if(par3.compare("'local'")==0){
            strictAccessInfoElement.isLocal=true;
            strictAccessInfoElement.isGlobal= false;
            
        }
        else if(par3.compare("'global'")==0){
            strictAccessInfoElement.isGlobal= true;
            strictAccessInfoElement.isLocal=false;
            
        }else{
            Logger::getStaticLogger().logError("strict access 3rd parameter can only be 'local' or 'global' .. will be conisidere 'global'");
            strictAccessInfoElement.isGlobal= true;
            strictAccessInfoElement.isLocal=false;
        }
        
        strictAccessInfoVector.push_back(strictAccessInfoElement);
        if(splittedPair.second.str().compare("")==0 ){
            break;
        }else{
            splittedPair=splittedPair.second.split('|');
        }
    }
    return strictAccessInfoVector;
}