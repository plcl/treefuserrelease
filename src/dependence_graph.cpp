//
//  DependeceGraph.cpp
//  libtooling
//
//  Created by lsakka on 5/3/16.
//  Copyright © 2016 lsakka. All rights reserved.
//
#include<stack>
#include "dependence_graph.h"
bool DG_Node::isRootNode(){
    if(this->isMerged){
        //make sure that not of the merged nodes has pred
        for( set<DG_Node * >::iterator it=mergeInfo->mergedNodes.begin();
            it!=mergeInfo->mergedNodes.end(); it++){
            
            if((*it)->predList.size()!=0)
                return false;
        }
        return true;
    }
    else{
        return this->predList.size()==0;
    }
    
}

//this can be converted to static method
DG_Node * DDG::createNode(pair<StatmentInfo *, int> value){
    
    DG_Node * node=new DG_Node(value.first,value.second );
    this->nodes.push_back(node);
    return node;
}

void DDG::merge(DG_Node *  n1 , DG_Node * n2){
    //if(n1->isMerged || n2->isMerged)
    //Logger::getStaticLogger().logWarn("merging nodes that allready bellong to merged nodes will merge both set");
    
    
    //1
    if(n1->isMerged && n2->isMerged){
        
        MergeInfo * tmp=n2->mergeInfo;
        
        for(set<DG_Node *>::iterator it=tmp->mergedNodes.begin();
            it!=tmp->mergedNodes.end(); it++ )
        {
            n1->mergeInfo->mergedNodes.insert(*it);
            (*it)->mergeInfo=n1->mergeInfo;
            
        }
        delete tmp;
        
    }
    
    else if(n1->isMerged && !n2->isMerged){
        n2->mergeInfo=n1->mergeInfo;
        n1->mergeInfo->mergedNodes.insert(n2);
        n2->isMerged=true;
        
    }
    else if(!n1->isMerged && n2->isMerged){
        n1->mergeInfo=n2->mergeInfo;
        n2->mergeInfo->mergedNodes.insert(n1);
        n1->isMerged=true;
        
    }
    else if(!n1->isMerged && !n2->isMerged){
        
        MergeInfo * tmp=new MergeInfo();
        n1->isMerged=true;
        n2->isMerged=true;
        tmp->mergedNodes.insert(n1);
        tmp->mergedNodes.insert(n2);
        n1->mergeInfo=tmp;
        n2->mergeInfo=tmp;
    }
    
    
    
}

void DDG::unmerge(DG_Node * n){
    
    MergeInfo * mergeInfo=n->mergeInfo;
    
    //unmerge current node
    n->isMerged=false;
    n->mergeInfo=NULL;
    mergeInfo->mergedNodes.erase(n);
    
    //special case if unmerging resulted in single node
    if(mergeInfo->mergedNodes.size()==1){
        (* mergeInfo->mergedNodes.begin())->isMerged=false;
        (* mergeInfo->mergedNodes.begin())->mergeInfo=NULL ;
        delete mergeInfo;
    }
    
    
}

void DDG::dump(){
    
    Logger::getStaticLogger().logDebug("dumping the dependence graph");
    Logger::getStaticLogger().logDebug("dumping nodes");
    
    for(int i=0; i<nodes.size(); i++){
        
        
        Logger::getStaticLogger().logDebug("traversalId:" +
                                           to_string(nodes[i]->traversalId)+
                                           ",stmtId:"+ to_string(nodes[i]->StatmentInfo->idInTraversals)+
                                           ",address:"+
                                           to_string((long long)(nodes[i]->StatmentInfo)) +",body:"+string(nodes[i]->StatmentInfo->stmt->getStmtClassName()) );
        
        for(map< DG_Node * , DepInfo> ::iterator it=  nodes[i]->succList.begin();
            it!=nodes[i]->succList.end(); it++){
            
            Logger::getStaticLogger().logDebug("dep To :["  +
                                               to_string(it->first->traversalId)+ "|" +
                                               to_string(it->first->StatmentInfo->idInTraversals)+"|"+
                                               to_string(( long long)(it->first->StatmentInfo) )+
                                               "], Type:["+
                                               to_string(it->second.control_dep)+"|"+
                                               to_string(it->second.global_dep )+ "|" +
                                               to_string(it->second.local_dep) + "|" +
                                               to_string(it->second.ontree_dep)+"|"+
                                               to_string(it->second.ontree_dep_fusable)+
                                               "]") ;
            
            
        }
        
    }
    
}

void DDG::dumpToPrint(){
    
    Logger::getStaticLogger().logDebug("dumping the dependence graph");
    Logger::getStaticLogger().logDebug("dumping nodes");
    
    for(int i=0; i<nodes.size(); i++){
        
        for(map< DG_Node * , DepInfo> ::iterator it=  nodes[i]->succList.begin();
            it!=nodes[i]->succList.end(); it++){
            
            
            if(nodes[i]->traversalId!=it->first->traversalId){
                Logger::getStaticLogger().log("\"t:"+to_string(nodes[i]->traversalId)+",s:"+to_string(nodes[i]->StatmentInfo->idInTraversals)+"\""+
                                              " -> "+
                                              "\"t:"+ to_string(it->first->traversalId)+",s:"+to_string(it->first->StatmentInfo->idInTraversals)+"\";");
                
            }
            
        }
        
    }
    for(int i=0; i<nodes.size(); i++){
        
        if(nodes[i]->StatmentInfo->isCallStmt){
            Logger::getStaticLogger().log("\"t:"+to_string(nodes[i]->traversalId)+",s:"+to_string(nodes[i]->StatmentInfo->idInTraversals)+"\""+";[color=\"0.305 0.625 1.000\"]\n");
        }
        
        
        
        
    }
    
}

void DDG::dumpMergeInfo(){
    
    map<MergeInfo * , bool> visited;
    for(int i=0; i<nodes.size(); i++){
        if(nodes[i]->isMerged && visited[nodes[i]->mergeInfo]==false){
            
            visited[nodes[i]->mergeInfo]=true;
            Logger::getStaticLogger().logDebug("merge info :\nmerged child :"+(*nodes[i]->mergeInfo->mergedNodes.begin())->StatmentInfo->calledChild->getNameAsString()+" participtaing nodes:");
            
            for(set<DG_Node * >::iterator it=nodes[i]->mergeInfo->mergedNodes.begin(); it!=nodes[i]->mergeInfo->mergedNodes.end(); it++){
                
                Logger::getStaticLogger().logDebug("node :["  +
                                                   to_string((*it)->traversalId)+ "|" +
                                                   to_string((*it)->StatmentInfo->idInTraversals)+"|"+
                                                   to_string(( long long)((*it)->StatmentInfo) ));
                
            }
            
        }
        else if (nodes[i]->StatmentInfo->isCallStmt && nodes[i]->isMerged==false){
            Logger::getStaticLogger().logDebug("unmerged call node to child"
                                               +nodes[i]->StatmentInfo->calledChild->getNameAsString()+":["  +
                                               to_string(nodes[i]->traversalId)+ "|" +
                                               to_string(nodes[i]->StatmentInfo->idInTraversals)+"|"+
                                               to_string(( long long)(nodes[i]->StatmentInfo) ));
        }
    }
    
}

void DDG::addDep(DepType debType, DG_Node * src , DG_Node * dst){
    
    
    assert(src!=dst);
    
    
    if(debType==global_dep){
        src->succList[dst].global_dep=true;
        dst->predList[src].global_dep=true;
    }
    else if(debType==local_dep){
        src->succList[dst].local_dep=true;
        dst->predList[src].local_dep=true;
        
        
    }
    else if(debType==ontree_dep){
        src->succList[dst].ontree_dep=true;
        dst->predList[src].ontree_dep=true;
        
        
    }
    else if(debType==ontree_dep_fusable){
        src->succList[dst].ontree_dep_fusable=true;
        dst->predList[src].ontree_dep_fusable=true;
        
        
    }
    
    else if(debType==control_dep){
        src->succList[dst].control_dep=true;
        dst->predList[src].control_dep=true;
        
        
    }
    
}


//check that there is no non fusable dep between nodes with in same merge unit
//and that no two of them belongs to the same traversal
bool DDG::hasWrongFuse( MergeInfo * mergeInfo){
    
    
    set<int> existedTraversalIds;
    clang::FieldDecl * commonCalledChild=(*mergeInfo->mergedNodes.begin())->StatmentInfo->calledChild;
    
    //check that all have the same called child
    for(set<DG_Node *>::iterator it=mergeInfo->mergedNodes.begin();
        it!=mergeInfo->mergedNodes.end() ;it++)
    {
        if((*it)->StatmentInfo->calledChild!=commonCalledChild)
            return true;
        
        if(existedTraversalIds.find((*it)->traversalId)!=existedTraversalIds.end()){
            return true;
        }
        else
            existedTraversalIds.insert((*it)->traversalId);
        
    }
    
    return false;
    
}


//this will check the following
bool DDG::hasWrongFuse(){
    
    map<MergeInfo * , bool> visited;
    for(int i=0; i<nodes.size(); i++){
        
        if(nodes[i]->isMerged && visited[nodes[i]->mergeInfo]==false)
        {
            visited[nodes[i]->mergeInfo]=true;
            if(hasWrongFuse(nodes[i]->mergeInfo))
                return true;
        }
    }
    return false;
    
}

bool DDG::hasIllegalMerge(){
    return hasCycle() || hasWrongFuse();
    
    
}

bool DDG::hasCycleRec(DG_Node * node, map<DG_Node *, int> & visited,stack<DG_Node * > &cyclePath){
    
    if(visited[node]==2)
        return false ;
    
    if(visited[node]==1){
        cyclePath.push(node );
        return true;
    }
    
    cyclePath.push(node);
    if(node->isMerged){
        
        MergeInfo * mergeInfo=node->mergeInfo;
        //convert all visited to grey
        for(set<DG_Node * >::iterator it=mergeInfo->mergedNodes.begin();
            it!=mergeInfo->mergedNodes.end(); it++){
            
            assert(visited[*it]==0);
            visited[*it]=1;
            
        }
        
        //visit all successors
        for(set<DG_Node * >::iterator it=mergeInfo->mergedNodes.begin();
            it!=mergeInfo->mergedNodes.end(); it++){
            
            
            for(map<DG_Node *,DepInfo>::iterator it2=(*it)->succList.begin();
                it2!=(*it)->succList.end(); it2++){
                
                //the node is part of the merged nodes nodes
                if(mergeInfo->isInMergedNodes( it2->first))
                    continue;
                
                else{
                    if(hasCycleRec(it2->first,visited,cyclePath)==true)
                        return true;
                    
                }
            }
            
        }
        //mark all as black
        for(set<DG_Node * >::iterator it=mergeInfo->mergedNodes.begin();
            it!=mergeInfo->mergedNodes.end(); it++){
            
            assert(visited[*it]==1);
            visited[*it]=2;
            
        }
        cyclePath.pop();
        return false ;
    }else{
        visited[node]=1;
        
        //  visit successors
        for(map<DG_Node *,DepInfo>::iterator it=node->succList.begin();
            it!=node->succList.end(); it++){
            
            //all dependcies are considered here so no need to check dependence type
            if(hasCycleRec(it->first,visited,cyclePath)==true)
                return true;
        }
        visited[node]=2;
        cyclePath.pop();
        return false;
    }
}



void printCyclePath( stack<DG_Node*> path){
    
    Logger::getStaticLogger().logDebug("printing cycle info ");
    while (!path.empty()){
        DG_Node* tmp=path.top();
        path.pop();

        if(tmp->isMerged){
            Logger::getStaticLogger().logDebug("-merge node participating nodes  :");

            
            for(set<DG_Node * >::iterator it=tmp->mergeInfo->mergedNodes.begin(); it!=tmp->mergeInfo->mergedNodes.end(); it++){
                Logger::getStaticLogger().logDebug("\tnode :["  +
                                                   to_string((*it)->traversalId)+ "|" +
                                                   to_string((*it)->StatmentInfo->idInTraversals)+"|"+
                                                   to_string(( long long)((*it)->StatmentInfo) ));
                
            }
            
        }else{
            Logger::getStaticLogger().logDebug("-single node :["  +
                                               to_string((tmp)->traversalId)+ "|" +
                                               to_string((tmp)->StatmentInfo->idInTraversals)+"|"+
                                               to_string(( long long)((tmp)->StatmentInfo) ));
            Logger::getStaticLogger().logDebug(tmp->StatmentInfo->stmt->getStmtClassName());

        }
        
    }
}
bool DDG::hasCycle(){
    //at each node that does not have predessors start a DFS
    map<DG_Node *, int> visited ;
    stack<DG_Node*> cyclePath;
    for(int i=0 ; i<nodes.size(); i++){
        
        assert(visited[nodes[i]]!=1);
        if(  visited[nodes[i]]==0){
            
            if( hasCycleRec(nodes[i] ,visited,cyclePath) ){
                
                //printCyclePath(cyclePath);
                return true;
            }
        }
    }
    return false;
}
