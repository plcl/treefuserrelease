//
//  function_finder.cpp
//  libtooling
//
//  Created by lsakka on 1/15/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#include "function_finder.h"
#include "logger.h"
#include "access_path.h"



bool FunctionFinder::VisitFunctionDecl(FunctionDecl * funDeclaration){
    
    
    //must be complete definition
   // funDeclaration->dump();
    if(funDeclaration->hasBody()==false)
        return true;
    
    //must have fuste attribute to be considered
    if(! hasFuseAnnotation(funDeclaration))
        return true;
    
    FunctionAnalyser *  tmp=new FunctionAnalyser(funDeclaration);
    if(!tmp->isValidFuse()){
        Logger::getStaticLogger().logInfo("function :"+funDeclaration->getNameAsString()+", is not valid fuse methods");
    
        
    }else{
        Logger::getStaticLogger().logInfo("function :"+funDeclaration->getNameAsString()+", is  valid fuse methods");

    }
    
    
    this->functionsInformation[funDeclaration]=(tmp);
    //go to next
    return true;
}

void FunctionFinder::findFunctions(ASTContext * context){
    this->TraverseDecl(context->getTranslationUnitDecl());
}