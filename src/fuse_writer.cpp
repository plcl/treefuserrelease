//
//  treefuse-writer.cpp
//  libtooling
//s
//  Created by lsakka on 7/14/16.
//  Copyright Â© 2016 lsakka. All rights reserved.
//

#include "treefuse_writer.h"
#define __OPT_BITMASK

#define FUSE_CAP 2
#define diff_CAP 4


string toBinString(unsigned int in){
    string out;
    while(in!=0){
        int x= in%2;
        out=to_string(x)+out;
        in=in/2;
    }
    out="0b"+out;
    return out;
}
int TreeFuseWriter::counter=0;

TreeFuseWriter::TreeFuseWriter( vector<clang::CallExpr *> & traversalsCAllExprList_,clang::FunctionDecl * enclosingFunDecls_, ASTContext  * context_, Rewriter & rewriter_, vector<DG_Node* > & statmentsTopologicalOrders_ ): rewriter(rewriter_) ,statmentsTopologicalOrder(statmentsTopologicalOrders_),traversalsCAllExprList(traversalsCAllExprList_)
{
    
    this->enclosingFunDecl=enclosingFunDecls_;
    this-> context=context_;
    
    
}

int TreeFuseWriter::countNumberOfParticipatingTraversals(vector<bool>&  participatingTraversals){
    
    int count =0;
    for(int i=0; i<participatingTraversals.size(); i++){
        if(participatingTraversals[i]==true){
            count++;
        }
    }
    
    return count;
}

int TreeFuseWriter::findTraversalIdOfFirstCall(vector<bool>&  participatingTraversals){
    for(int i=0; i<participatingTraversals.size(); i++){
        if(participatingTraversals[i]==true){
            
            return i;
        }
    }
    
    return -1;
}
bool TreeFuseWriter::isGenerated(vector<bool> & participatingTraversals){
    
    string name=createName(participatingTraversals);
    
    return this->availableFuseCombonations.find(name)!=availableFuseCombonations.end();
}

string TreeFuseWriter::createName(vector<bool>&  participatingTraversals){
    string ret="_fuse_"+to_string(fuseId)+"_";
    
    for(int i=0; i<participatingTraversals.size(); i++){
        if(participatingTraversals[i]==true)
            ret+=+"F"+to_string(i+1);
    }
    return ret;
}


pair<int , int> TreeFuseWriter::isSingleActiveTraversal(vector<bool>&  participatingTraversals){
    
    
    pair<int, int> ret;
    
    
    int count =0;
    int tId=-1;
    for(int i=0; i<participatingTraversals.size(); i++){
        if(participatingTraversals[i]==true){
            count++;
            tId=i;
        }
    }
    if(count==1)
        ret.first=SINGLE_CALL;
    else if(count==0)
        ret.first=NO_CALL;
    else
        ret.first=MULTI_CALL;
    
    ret.second=tId;
    
    return ret;
}


#ifdef __OPT_BITMASK
void TreeFuseWriter::setBlockSubPart(string & decls, string & blockPart,vector<bool> &participatingTraversals,int blockId_ , vector< vector<DG_Node * > > stmtList){
    
    StatmentPrinter printer;
    
    for(int j=0; j<participatingTraversals.size(); j++){
        
        if(participatingTraversals[j]==false)
            continue;
        
        string funcId=to_string(j+1);
        string blockId=to_string(blockId_);
        
        string next_label="_label_B"+blockId+"F"+funcId+"_Exit";
        string tmp="";
        
        for(int k=0; k<stmtList[j].size();k++){
            
            if(stmtList[j][k]->StatmentInfo->stmt->getStmtClass()==clang::Stmt::DeclStmtClass){
                
                decls+="\t"+printer.printStmt(stmtList[j][k]->StatmentInfo->stmt,context->getSourceManager(),traversalsDeclList[j]->getParamDecl(0),next_label,j+1 )+";\n";
                
            }else{
                tmp+=printer.printStmt(stmtList[j][k]->StatmentInfo->stmt,context->getSourceManager(),traversalsDeclList[j]->getParamDecl(0),next_label,j+1 );
                
            }
        }
        if(tmp.compare("")!=0){
            blockPart+="if (truncate_flags &"+toBinString((1<<j))+") {\n";
            blockPart+=tmp;
            blockPart+="}\n"+next_label+":\n";
        }
        
    }
    
}

void TreeFuseWriter::setCallPart( string & callPart,vector<bool> &participatingTraversals,DG_Node * callNode,FusedTraversalWriteBackInfo * writeBackInfo){
    
    StatmentPrinter printer;
    map<int , clang::CallExpr* > tidToCallExpr;
    
    vector<bool> participatingTraversalsForNextCall;
    
    participatingTraversalsForNextCall.resize(traversalsDeclList.size());
    
    fill(participatingTraversalsForNextCall.begin(),participatingTraversalsForNextCall.end() , false);
    
    if(callNode->isMerged){
        
        for(set<DG_Node*>::iterator it=callNode->mergeInfo->mergedNodes.begin();
            it!=callNode->mergeInfo->mergedNodes.end(); it++){
            
            if(participatingTraversals[(*it)->traversalId]==true)
            {
                tidToCallExpr[(*it)->traversalId]=dyn_cast<clang::CallExpr>((*it)->StatmentInfo->stmt);
                participatingTraversalsForNextCall[(*it)->traversalId]=true;
                
            }
            
        }
        
    }else{
        if(participatingTraversals[callNode->traversalId]==true)
        {
            tidToCallExpr[callNode->traversalId]=dyn_cast<clang::CallExpr >(callNode->StatmentInfo->stmt);
            
            participatingTraversalsForNextCall[callNode->traversalId]=true;
            
        }
        
    }
    int active_calls_count=countNumberOfParticipatingTraversals(participatingTraversalsForNextCall);
    
    
    
    
    if(active_calls_count==NO_CALL){
        callPart="";
        return;
    }
    
    
    unsigned int cond=0;
    
    string callCond="if ( (truncate_flags & ";
    for(int k=0; k<participatingTraversalsForNextCall.size();k++){
        if(participatingTraversalsForNextCall[k]==false)
            continue;
        
        cond|=(1<<k);
    }
    callCond+=toBinString(cond)+") )";
    if(active_calls_count==SINGLE_CALL){
        
        int traversalId=findTraversalIdOfFirstCall(participatingTraversalsForNextCall);
        string nextCallName=traversalsDeclList[traversalId]->getNameAsString();
        callPart+=callCond+"{\n\t";
        callPart+=nextCallName+"("+"_r->"+callNode->StatmentInfo->calledChild->getNameAsString();
        clang::CallExpr * callExpr (tidToCallExpr[traversalId]);
        
        string nextCallParams;
        for(int h=1; h<callExpr->getNumArgs(); h++){
            
            nextCallParams+=", "+printer.printStmt(callExpr->getArg(h), context->getSourceManager(),
                                                   traversalsDeclList[traversalId]->getParamDecl(0),
                                                   "dummy", traversalId+1);
            
        }
        callPart +=nextCallParams;
        callPart+=");";
        callPart+="\n}";
        return;
    }
    
    
    string nextCallName;
    bool callingSame = false;
    if(active_calls_count>1)
    {
        int curr_active_calls_count = countNumberOfParticipatingTraversals(participatingTraversals);
        
        if (curr_active_calls_count > FUSE_CAP ){
            //  if( (curr_active_calls_count  -  active_calls_count) > diff_CAP ){
            nextCallName=createName(participatingTraversalsForNextCall);
            if(curr_active_calls_count==2)
                assert((curr_active_calls_count==active_calls_count) && "why!");
            if(this->isGenerated(participatingTraversalsForNextCall) == false){
                this->generateWriteBackInfo(participatingTraversalsForNextCall,tidToCallExpr);
                
            }
        }else{
            nextCallName = writeBackInfo->name;
            callingSame = true;
            
        }
    }
    
    
    callPart+=callCond+"{\n\t";
    callPart+=nextCallName+"("+"_r->"+callNode->StatmentInfo->calledChild->getNameAsString();
    
    
    if (callingSame){
        
        string nextCallParams;
        for(int k=0; k<participatingTraversals.size();k++){
            
            if(participatingTraversals[k]==false)
                continue;
            
            
            clang::CallExpr * callExpr =participatingTraversalsForNextCall[k]==true?  tidToCallExpr[k]:writeBackInfo->particiaptingTraversal_and_Calls[k];
            
            for(int h=1; h<callExpr->getNumArgs(); h++){
                
                nextCallParams+=", "+printer.printStmt(callExpr->getArg(h), context->getSourceManager(), traversalsDeclList[k]->getParamDecl(0), "dummy", k+1);
                
            }
            
            
        }
        
        nextCallParams+=",truncate_flags & "+toBinString(cond);
        callPart +=nextCallParams;
        callPart+=");";
        callPart+="\n}";
        return;
        
    }else{
        string nextCallParams;
        for(int k=0; k<participatingTraversalsForNextCall.size();k++){
            
            if(participatingTraversalsForNextCall[k]==false)
                continue;
            
            
            clang::CallExpr * callExpr=(tidToCallExpr[k]);
            
            for(int h=1; h<callExpr->getNumArgs(); h++){
                
                nextCallParams+=", "+printer.printStmt(callExpr->getArg(h), context->getSourceManager(), traversalsDeclList[k]->getParamDecl(0), "dummy", k+1);
                
            }
            
            
        }
        
        nextCallParams+=",truncate_flags & "+toBinString(cond);
        callPart +=nextCallParams;
        callPart+=");";
        callPart+="\n}";
        return;
    }
    
}

void TreeFuseWriter::generateWriteBackInfo(vector<bool> & participatingTraversals, map<int, clang::CallExpr *> _participatingTraversals_and_Calls){
    
    
    StatmentPrinter printer;
    
    //generate the name of the new function
    string idName=createName(participatingTraversals);
    
    //check if already generated and assert on it
    if(this->isGenerated(participatingTraversals))
        assert(false);
    
    
    //create writeback info
    FusedTraversalWriteBackInfo *  writeBackInfo=new FusedTraversalWriteBackInfo;
    this->availableFuseCombonations[idName]=writeBackInfo;
    
    // build declatation signetrue (forward declaration)
    
    //2-build the new function call and add it.
    writeBackInfo->particiaptingTraversal_and_Calls= _participatingTraversals_and_Calls;
    writeBackInfo->name=idName;
    writeBackInfo->forwardDecl ="void "+idName+"(";
    
    writeBackInfo->forwardDecl +=string(this->traversalsDeclList[0]->getParamDecl(0)->getType().getAsString())+" _r";
    
    
    //append the arguments of each method and rename locals  by adding _fx_ only participating traversals
    for(int i=0;i<this->traversalsDeclList.size(); i++){
        
        if(participatingTraversals[i]==false)
            continue;
        
        for(int j=1; j<traversalsDeclList[i]->parameters().size(); j++){
            writeBackInfo->forwardDecl +=","+string(traversalsDeclList[i]->parameters()[j]->getType().getAsString())+" _f"+to_string(i+1)+"_"+traversalsDeclList[i]->parameters()[j]->getDeclName().getAsString();
            
        }
    }
    
    //apend the doFx for each function ( not needed)!!
    /*
     for(int i=0;i<traversalsDeclList.size(); i++){
     if(participatingTraversals[i]==false)
     continue;
     
     writeBackInfo->forwardDecl +=",bool doF"+to_string(i+1);
     
     }*/
    writeBackInfo->forwardDecl +=",unsigned int truncate_flags)";
    
    
    //build the body of the traversal
    
    //store all top level declarations here (seen across blocks , calls only occure on top level (relative to method body !!)
    string decls;
    
    vector< vector<DG_Node * > > stamentsOderedByTId;
    stamentsOderedByTId.resize(traversalsDeclList.size());
    
    int blockId_=0;
    
    for (int i=0; i<statmentsTopologicalOrder.size(); i++) {
        
        if(statmentsTopologicalOrder[i]->StatmentInfo->isCallStmt){
            blockId_++;
            //the block part
            writeBackInfo->body+="//block "+to_string(blockId_)+"\n";
            
            
            string blockSubPart="";
            this->setBlockSubPart(decls, blockSubPart, participatingTraversals, blockId_,stamentsOderedByTId );
            writeBackInfo->body+=blockSubPart;
            
            
            //  call part
            string callPart="";
            this->setCallPart(callPart, participatingTraversals, statmentsTopologicalOrder[i],writeBackInfo);
            //callect call expression (only for participating traversals)
            writeBackInfo->body+=callPart;
            
            stamentsOderedByTId.clear();
            stamentsOderedByTId.resize(traversalsDeclList.size());
            
        }  else{
            if(participatingTraversals[statmentsTopologicalOrder[i]->traversalId]==false)
                continue;
            else{
                stamentsOderedByTId[statmentsTopologicalOrder[i]->traversalId].push_back(statmentsTopologicalOrder[i]);
            }
            
        }
    }
    
    //last block ( no read is needed)
    blockId_++;
    writeBackInfo->body+="//block "+to_string(blockId_)+"\n";
    
    string blockSubPart="";
    this->setBlockSubPart(decls, blockSubPart, participatingTraversals, blockId_,stamentsOderedByTId );
    writeBackInfo->body+=blockSubPart;
    
    
    
    string callPart="";
    callPart+="return ;\n";
    //callect call expression (only for participating traversals)
    writeBackInfo->body+=callPart;
    writeBackInfo->body=decls+  writeBackInfo->body;
    
    string fullFun="//****** this fused method is generated by PLCL\n "+writeBackInfo->forwardDecl+"{\n"+"//first level declarations\n"+decls+"\n//body\n"+writeBackInfo->body+"\n}\n\n";
    
    
}
void TreeFuseWriter::writeBackFusedVersion(){
    //generate decl list
    traversalsDeclList.clear();
    for(int i=0; i<traversalsCAllExprList.size(); i++){
        traversalsDeclList.push_back(traversalsCAllExprList[i]->getCalleeDecl()->getAsFunction());
        
    }
    //set fuse counter
    TreeFuseWriter::counter++;
    this->fuseId=counter;
    
    //1-comment all old calls
    for(int i=0; i<traversalsCAllExprList.size(); i++){
        rewriter.InsertText(traversalsCAllExprList[i]->getExprLoc(), "//");
    }
    //build the wholse set of new traversals
    
    vector<bool> participatingTraversalsStart;
    
    map<int, clang::CallExpr *> tidToCallExpr;
    for(int i=0; i<this->traversalsCAllExprList.size(); i++){
        tidToCallExpr[i]=traversalsCAllExprList[i];
    }
    participatingTraversalsStart.resize(traversalsCAllExprList.size());
    fill(participatingTraversalsStart.begin(), participatingTraversalsStart.end(), true);
    
    this->generateWriteBackInfo(participatingTraversalsStart,tidToCallExpr);
    
    // add forward declarations
    for(map<string,FusedTraversalWriteBackInfo *>::iterator it=availableFuseCombonations.begin(); it!=availableFuseCombonations.end(); it++){
        rewriter.InsertText(this->enclosingFunDecl->getLocStart(), (it->second->forwardDecl+";\n"));
    }
    
    //add all function definitions
    for(map<string,FusedTraversalWriteBackInfo *>::iterator it=availableFuseCombonations.begin(); it!=availableFuseCombonations.end(); it++){
        rewriter.InsertText(this->enclosingFunDecl->getLocStart(), (it->second->forwardDecl+"\n{\n"+it->second->body+"\n};\n"));
    }
    
    //add the new call
    
    StatmentPrinter printer;
    //2-build the new function call and add it.
    string newCall=createName(participatingTraversalsStart)+
    "("+
    printer.stmtTostr(traversalsCAllExprList[0]->getArg(0), context->getSourceManager());
    
    //3-append arguments of all methods in the same order and build defualt params
    for(int i=0; i<traversalsCAllExprList.size(); i++){
        
        for(int j=1; j<traversalsCAllExprList[i]->getNumArgs();j++){
            
            newCall+=","+printer.printStmt(traversalsCAllExprList[i]->getArg(j), context->getSourceManager());
        }
    }
    //add doFunX for each traversals
    unsigned int x=0;
    for(int i=0; i<traversalsCAllExprList.size(); i++){
        x|=(1<<i);
    }
    newCall+=","+toBinString(x) +");";
    rewriter.InsertTextAfter
    (Lexer::findLocationAfterToken(traversalsCAllExprList[traversalsCAllExprList.size()-1]->getLocEnd(), tok::TokenKind::semi, context->getSourceManager(), context->getLangOpts(), true), "\n\t//added by fuse transformer \n\t"+newCall+"\n");
    
    
}


#else/*
void TreeFuseWriter::setBlockSubPart(string & decls, string & blockPart,vector<bool> &participatingTraversals,int blockId_ , vector< vector<DG_Node * > > stmtList){

StatmentPrinter printer;

for(int j=0; j<participatingTraversals.size(); j++){

if(participatingTraversals[j]==false)
continue;

string funcId=to_string(j+1);
string blockId=to_string(blockId_);

string next_label="_label_B"+blockId+"F"+funcId+"_Exit";
string tmp="";

for(int k=0; k<stmtList[j].size();k++){

if(stmtList[j][k]->StatmentInfo->stmt->getStmtClass()==clang::Stmt::DeclStmtClass){

decls+="\t"+printer.printStmt(stmtList[j][k]->StatmentInfo->stmt,context->getSourceManager(),traversalsDeclList[j]->getParamDecl(0),next_label,j+1 )+";\n";

}else{
tmp+=printer.printStmt(stmtList[j][k]->StatmentInfo->stmt,context->getSourceManager(),traversalsDeclList[j]->getParamDecl(0),next_label,j+1 );

}
}
if(tmp.compare("")!=0){
blockPart+="if (doF"+funcId+") {\n";
blockPart+=tmp;
blockPart+="}\n"+next_label+":\n";
}

}

}

void TreeFuseWriter::setCallPart( string & callPart,vector<bool> &participatingTraversals,DG_Node * callNode){

StatmentPrinter printer;
map<int , clang::CallExpr* > tidToCallExpr;

vector<bool> participatingTraversalsForNextCall;

participatingTraversalsForNextCall.resize(traversalsDeclList.size());

fill(participatingTraversalsForNextCall.begin(),participatingTraversalsForNextCall.end() , false);

if(callNode->isMerged){

for(set<DG_Node*>::iterator it=callNode->mergeInfo->mergedNodes.begin();
it!=callNode->mergeInfo->mergedNodes.end(); it++){

if(participatingTraversals[(*it)->traversalId]==true)
{
clang::Stmt * tmp=(*it)->StatmentInfo->stmt;
tidToCallExpr[(*it)->traversalId]=  dyn_cast <clang::CallExpr > (tmp);

participatingTraversalsForNextCall[(*it)->traversalId]=true;

}

}

}else{
if(participatingTraversals[callNode->traversalId]==true)
{
tidToCallExpr[callNode->traversalId]=callNode;

participatingTraversalsForNextCall[callNode->traversalId]=true;

}

}
pair<int, int> isSingleTraversal=isSingleActiveTraversal(participatingTraversalsForNextCall);
int isSingleStatus=isSingleTraversal.first;

if(isSingleStatus==NO_CALL){
callPart="";
return;
}
string nextCallParams;
//build params list for the called traversal


string callCond="if ( (false ";
for(int k=0; k<participatingTraversalsForNextCall.size();k++){
if(participatingTraversalsForNextCall[k]==false)
continue;


callCond+="|| doF"+to_string(k+1);

clang::CallExpr * callExpr=
dyn_cast<clang::CallExpr>(tidToCallExpr[k]->StatmentInfo->stmt);
for(int h=1; h<callExpr->getNumArgs(); h++){

nextCallParams+=", "+printer.printStmt(callExpr->getArg(h), context->getSourceManager(), traversalsDeclList[k]->getParamDecl(0), "dummy", k+1);

}

}
//add truncating flags only if several traversals might run together
if(isSingleStatus==MULTI_CALL){
for(int k=0; k<participatingTraversalsForNextCall.size(); k++){
if(participatingTraversalsForNextCall[k]==false)
continue;

nextCallParams+=string(",") + "doF"+to_string(k+1);

}
}

callCond+=")==true){\n\t";
callPart+=callCond;



string nextCallName;
if(isSingleStatus==SINGLE_CALL)
nextCallName=traversalsDeclList[isSingleTraversal.second]->getNameAsString();


if(isSingleStatus==MULTI_CALL)
{
nextCallName=createName(participatingTraversalsForNextCall);

if(this->isGenerated(participatingTraversalsForNextCall)==false){
this->generateWriteBackInfo(participatingTraversalsForNextCall);
}
}
callPart+=nextCallName+"("+"_r->"+callNode->StatmentInfo->calledChild->getNameAsString();

if(nextCallParams.size())
callPart +=nextCallParams;

callPart+=");";

callPart+="\n}";




}

void TreeFuseWriter::generateWriteBackInfo(vector<bool> & participatingTraversals){


StatmentPrinter printer;

//generate the name of the new function
string idName=createName(participatingTraversals);

//check if already generated and assert on it
if(this->isGenerated(participatingTraversals))
assert(false);


//create writeback info
FusedTraversalWriteBackInfo *  writeBackInfo=new FusedTraversalWriteBackInfo;
this->availableFuseCombonations[idName]=writeBackInfo;


// build declatation signetrue (forward declaration)

//2-build the new function call and add it.
writeBackInfo->name=idName;
writeBackInfo->forwardDecl ="void "+idName+"(";

writeBackInfo->forwardDecl +=string(this->traversalsDeclList[0]->getParamDecl(0)->getType().getAsString())+" _r";


//append the arguments of each method and rename locals  by adding _fx_ only participating traversals
for(int i=0;i<this->traversalsDeclList.size(); i++){

if(participatingTraversals[i]==false)
continue;

for(int j=1; j<traversalsDeclList[i]->parameters().size(); j++){
writeBackInfo->forwardDecl +=","+string(traversalsDeclList[i]->parameters()[j]->getType().getAsString())+" _f"+to_string(i+1)+"_"+traversalsDeclList[i]->parameters()[j]->getDeclName().getAsString();

}
}

//apend the doFx for each function
for(int i=0;i<traversalsDeclList.size(); i++){
if(participatingTraversals[i]==false)
continue;

writeBackInfo->forwardDecl +=",bool doF"+to_string(i+1);

}
writeBackInfo->forwardDecl +=")";


//build the body of the traversal

//store all top level declarations here (seen across blocks , calls only occure on top level (relative to method body !!)
string decls;

vector< vector<DG_Node * > > stamentsOderedByTId;
stamentsOderedByTId.resize(traversalsDeclList.size());

int blockId_=0;

for (int i=0; i<statmentsTopologicalOrder.size(); i++) {

if(statmentsTopologicalOrder[i]->StatmentInfo->isCallStmt){
blockId_++;
//the block part
writeBackInfo->body+="//block "+to_string(blockId_)+"\n";




string blockSubPart="";
this->setBlockSubPart(decls, blockSubPart, participatingTraversals, blockId_,stamentsOderedByTId );
writeBackInfo->body+=blockSubPart;



//  call part
string callPart="";
this->setCallPart(callPart, participatingTraversals, statmentsTopologicalOrder[i]);
//callect call expression (only for participating traversals)
writeBackInfo->body+=callPart;

stamentsOderedByTId.clear();
stamentsOderedByTId.resize(traversalsDeclList.size());

}  else{
if(participatingTraversals[statmentsTopologicalOrder[i]->traversalId]==false)
continue;
else{
stamentsOderedByTId[statmentsTopologicalOrder[i]->traversalId].push_back(statmentsTopologicalOrder[i]);
}

}
}

//last block ( no read is needed)
blockId_++;
writeBackInfo->body+="//block "+to_string(blockId_)+"\n";



string blockSubPart="";
this->setBlockSubPart(decls, blockSubPart, participatingTraversals, blockId_,stamentsOderedByTId );
writeBackInfo->body+=blockSubPart;



string callPart="";
callPart+="return ;\n";
//callect call expression (only for participating traversals)
writeBackInfo->body+=callPart;
writeBackInfo->body=decls+  writeBackInfo->body;

string fullFun="//****** this fused method is generated by PLCL\n "+writeBackInfo->forwardDecl+"{\n"+"//first level declarations\n"+decls+"\n//body\n"+writeBackInfo->body+"\n}\n\n";

}

void TreeFuseWriter::writeBackFusedVersion(){

//generate decl list
traversalsDeclList.clear();
for(int i=0; i<traversalsCAllExprList.size(); i++){
traversalsDeclList.push_back(traversalsCAllExprList[i]->getCalleeDecl()->getAsFunction());

}
//set fuse counter
TreeFuseWriter::counter++;
this->fuseId=counter;

//1-comment all old calls
for(int i=0; i<traversalsCAllExprList.size(); i++){
rewriter.InsertText(traversalsCAllExprList[i]->getExprLoc(), "//");
}
//build the wholse set of new traversals

vector<bool> participatingTraversalsStart;
participatingTraversalsStart.resize(traversalsCAllExprList.size());
fill(participatingTraversalsStart.begin(), participatingTraversalsStart.end(), true);

this->generateWriteBackInfo(participatingTraversalsStart);

// add forward declarations
for(map<string,FusedTraversalWriteBackInfo *>::iterator it=availableFuseCombonations.begin(); it!=availableFuseCombonations.end(); it++){
rewriter.InsertText(this->enclosingFunDecl->getLocStart(), (it->second->forwardDecl+";\n"));
}

//add all function definitions
for(map<string,FusedTraversalWriteBackInfo *>::iterator it=availableFuseCombonations.begin(); it!=availableFuseCombonations.end(); it++){
rewriter.InsertText(this->enclosingFunDecl->getLocStart(), (it->second->forwardDecl+"\n{\n"+it->second->body+"\n};\n"));
}

//add the new call

StatmentPrinter printer;
//2-build the new function call and add it.
string newCall=createName(participatingTraversalsStart)+
"("+
printer.stmtTostr(traversalsCAllExprList[0]->getArg(0), context->getSourceManager());

//3-append arguments of all methods in the same order and build defualt params
for(int i=0; i<traversalsCAllExprList.size(); i++){

for(int j=1; j<traversalsCAllExprList[i]->getNumArgs();j++){

newCall+=","+printer.printStmt(traversalsCAllExprList[i]->getArg(j), context->getSourceManager());
}
}
//add doFunX for each traversals
for(int i=0; i<traversalsCAllExprList.size(); i++){
newCall+=",true";
}
newCall+=");";
rewriter.InsertTextAfter
(Lexer::findLocationAfterToken(traversalsCAllExprList[traversalsCAllExprList.size()-1]->getLocEnd(), tok::TokenKind::semi, context->getSourceManager(), context->getLangOpts(), true), "\n\t//added by fuse transformer \n\t"+newCall+"\n");


}
*/
#endif

void StatmentPrinter::print_handleStmt(clang::Stmt * stmt, SourceManager &sm){
    stmt=stmt->IgnoreImplicit();
    
    switch (stmt->getStmtClass()) {
        case clang::Stmt::CompoundStmtClass:
            
            for(clang::Stmt::child_iterator it= stmt->child_begin();it!=stmt->child_end(); it++){
                print_handleStmt(*it,sm);
            }
            
            break;
        case clang::Stmt::StmtClass::CallExprClass:
            //we will print the decl and then the of it
        {
            clang::CallExpr * callExpr=dyn_cast<clang::CallExpr>(stmt);
            ret += callExpr->getDirectCallee()->getNameAsString()+"(";
            for(int i=0; i<callExpr->getNumArgs(); i++  ){
                
                print_handleStmt(callExpr->getArg(i), sm);
                if(i!=callExpr->getNumArgs()-1)
                    ret+=",";
            }
            ret+=")";
            if(insideExp==0)
                ret+=";";
            //append the arguments
            // assert(false);
        }
            break;
            
        case clang::Stmt::StmtClass::BinaryOperatorClass:
            //print the lhs ,
        {
            insideExp++;
            clang::BinaryOperator * binOp=dyn_cast<clang::BinaryOperator> (stmt);
            
            if(binOp->isAssignmentOp())
                ret+="\t";
            print_handleStmt(binOp->getLHS(),sm);
            
            //print op
            ret+=binOp->getOpcodeStr();
            
            //print lhs
            print_handleStmt(binOp->getRHS(),sm);
            if(binOp->isAssignmentOp())
                ret+=";\n";
            insideExp--;
            
        }
            break;
            
            
        case clang::Stmt::StmtClass::DeclRefExprClass:
        {
            
            clang::DeclRefExpr * declRefExpr=dyn_cast<clang::DeclRefExpr>(stmt);
            if(declRefExpr->getDecl()==this->rootDecl)
                ret+="_r";
            else if(!declRefExpr->getDecl()->isDefinedOutsideFunctionOrMethod())
                ret+="_f"+to_string(this->funId)+"_"+declRefExpr->getDecl()->getNameAsString();
            else
                ret+=stmtTostr(stmt,sm);
        }
            break;
        case clang::Stmt::StmtClass::ImplicitCastExprClass:
        {
            ret+="|>";
            clang::ImplicitCastExpr * impCastExpr=dyn_cast<clang::ImplicitCastExpr> (stmt);
            
            print_handleStmt(impCastExpr->getSubExpr(),sm);
        }
            break;
        case clang::Stmt::StmtClass::MemberExprClass:
            
        {
            
            clang::MemberExpr * memberExpr=dyn_cast<clang::MemberExpr> (stmt);
            
            
            print_handleStmt(*memberExpr->child_begin(), sm);
            if(memberExpr->isArrow())
                ret+="->"+memberExpr->getMemberDecl()->getNameAsString();
            else{
                ret+="."+memberExpr->getMemberDecl()->getNameAsString();
                
            }
            
        }
            break;
            
        case clang::Stmt::StmtClass::IfStmtClass:
        {
            
            clang::IfStmt * S=dyn_cast<clang::IfStmt> (stmt) ;
            //check the condition part first
            if(S->getCond()!=NULL ){
                insideExp++;
                ret+="\t if (";
                clang::Expr * cond=S->getCond()->IgnoreImplicit();
                print_handleStmt(cond,sm);
                ret+=")";
                insideExp--;
                
            }
            //check then part
            if(S->getThen()!=NULL)
            {
                
                clang::Stmt * then=S->getThen()->IgnoreImplicit();
                ret+="{\n";
                print_handleStmt(then  , sm);
                ret+="\t}";
            }else{
                ret+="{}\n";
            }
            
            //check else part
            if(S->getElse()!=NULL)
            {
                clang::Stmt * _else=S->getElse()->IgnoreImplicit();
                ret+="else{\n\t";
                print_handleStmt(_else  , sm);
                ret+="\n\t}";
            }else
            {
                ret+="\n";
                
            }
        }
            break;
        case clang::Stmt::StmtClass::ReturnStmtClass:
#ifdef __OPT_BITMASK
            ret+="\t truncate_flags&="+toBinString(~ (unsigned int)(1<<(funId-1))) +"; goto "+nextLabel+" ;\n";
#else
            ret+="\tdoF"+to_string(funId)+" = false ; goto "+nextLabel+" ;\n";
#endif
            break;
        case clang::Stmt::Stmt::NullStmtClass:
            ret+="\t\t;\n";
            
            break;
        case clang::Stmt::StmtClass::DeclStmtClass:
        {
            // stmt->dump();
            clang::DeclStmt * declStmt=dyn_cast<clang::DeclStmt>(stmt);
            
            for(clang::DeclStmt::decl_iterator it=declStmt->decl_begin(); it!=declStmt->decl_end(); it++){
                
                clang::Decl *  decl=*it;
                clang::VarDecl *  varDecl=dyn_cast<clang::VarDecl>(decl);
                assert(varDecl!=NULL);
                
                ret+="\t"+varDecl->getType().getAsString()+" "+
                "_f"+to_string(this->funId)+"_"+varDecl->getNameAsString()+" ";
                if(varDecl->getInit()!=NULL){
                    ret+= "=";
                    print_handleStmt (varDecl->getInit(),sm);
                    
                    ret+= ";\n";
                    
                }
                
                else
                    ret+=";\n";
            }
            break;
            
        }
            
            
        case clang::Stmt::StmtClass::ParenExprClass:
        {
            clang::ParenExpr * parenExpr=dyn_cast<clang::ParenExpr>(stmt);
            ret+="(";
            print_handleStmt(parenExpr->getSubExpr(),sm);
            ret+=")";
        }
            break;
            
        case clang::Stmt::StmtClass::CXXMemberCallExprClass:
        {
            
            clang::CXXMemberCallExpr * callExpr=dyn_cast<clang::CXXMemberCallExpr>(stmt);
            print_handleStmt(*((callExpr)->child_begin())->child_begin(), sm);
            
            ret+="->"+callExpr->getCalleeDecl()->getAsFunction()->getNameAsString()+"(";
            for(int i=0; i<callExpr->getNumArgs(); i++){
                print_handleStmt(callExpr->getArg(i), sm);
                if(i!=callExpr->getNumArgs()-1)
                    ret+=",";
            }
            ret+=")";
            if(insideExp==0)
                ret+=";";
            
            
        }
            break;
            
            
        default:
            ret+=stmtTostr(stmt,sm);
            
    }
    return ;
}

/*no longer used (with out narrowing)*/

/*
 std::string stmtTostr(class clang::Stmt *d, SourceManager &sm) {
 // (T, U) => "T,,"
 string text = Lexer::getSourceText(CharSourceRange::getTokenRange(d->getSourceRange()), sm, LangOptions(), 0);
 if (text.at(text.size()-1) == ',')
 return Lexer::getSourceText(CharSourceRange::getCharRange(d->getSourceRange()), sm, LangOptions(), 0);
 return text;
 }
 std::string declTostr(clang::Decl *d, SourceManager &sm) {
 // (T, U) => "T,,"
 string text = Lexer::getSourceText(CharSourceRange::getTokenRange(d->getSourceRange()), sm, LangOptions(), 0);
 if (text.at(text.size()-1) == ',')
 return Lexer::getSourceText(CharSourceRange::getCharRange(d->getSourceRange()), sm, LangOptions(), 0);
 return text;
 }
 */


void writeTransformation_withOutNarrowing(vector<clang::CallExpr *> & canFuseList,vector<DG_Node* > &topOrder,ASTContext  * context_,Rewriter & rewriter,clang::FunctionDecl * enclosingFunDecl){
    
    static int counter=0;
    
    
    StatmentPrinter printer;
    //generate the name of the new function
    
    string idName="_fuse_"+to_string(counter++);
    for(int i=0; i<canFuseList.size(); i++){
        idName+=+"_"+canFuseList[i]->getCalleeDecl()->getAsFunction()->getNameAsString();
    }
    
    //1-comment all old calls
    for(int i=0; i<canFuseList.size(); i++){
        rewriter.InsertText(canFuseList[i]->getExprLoc(), "//");
    }
    
    //2-build the new function call and add it.
    string newCall=idName+
    "("+
    printer.stmtTostr(canFuseList[0]->getArg(0), context_->getSourceManager());
    
    //3-append arguments of all methods in the same order and build defualt params
    vector<string> defaultParams;
    defaultParams.resize(canFuseList.size());
    for(int i=0; i<canFuseList.size(); i++){
        
        for(int j=1; j<canFuseList[i]->getNumArgs();j++){
            
            if(canFuseList[i]->getArg(j)->getType()->isBuiltinType())
                defaultParams[i]+=",0";
            else
                defaultParams[i]+=","+canFuseList[i]->getArg(j)->getType().getAsString()+"()";
            
            newCall+=","+printer.stmtTostr(canFuseList[i]->getArg(j), context_->getSourceManager());
        }
    }
    //add doFunX for each traversals
    for(int i=0; i<canFuseList.size(); i++){
        newCall+=",true";
    }
    newCall+=");";
    rewriter.InsertTextAfter
    (Lexer::findLocationAfterToken(canFuseList[canFuseList.size()-1]->getLocEnd(), tok::TokenKind::semi, context_->getSourceManager(), context_->getLangOpts(), true), "\n\t//added by fuse transformer \n\t"+newCall+"\n");
    
    // rewriter.overwriteChangedFiles();
    
    //step 2 : write the declaration of the new fused method
    
    //1.generate decl list
    vector<clang::FunctionDecl * > declList;
    for(int i=0; i<canFuseList.size(); i++){
        declList.push_back(canFuseList[i]->getCalleeDecl()->getAsFunction());
        
    }
    
    //generate function signatrue
    string signeture="void __attribute__(( fuse ))  "+
    idName+"("+
    string(declList[0]->getParamDecl(0)->getType().getAsString())+" _r";
    
    //append the arguments of each method and rename of them by adding _fx_ to each
    for(int i=0;i<declList.size(); i++){
        
        for(int j=1; j<declList[i]->parameters().size(); j++){
            signeture+=","+string(declList[i]->parameters()[j]->getType().getAsString())+" _f"+to_string(i+1)+"_"+
            declList[i]->parameters()[j]->getDeclName().getAsString();
            
        }
    }
    
    //apend the doFx for each function
    for(int i=0;i<declList.size(); i++){
        signeture+=",bool doF"+to_string(i+1);
        
    }
    signeture+=")";
    //Now build the body of the function
    
    string decls; //store all top level declarations here (seen across blocks , calls only occure on top level (relative to method body !!)
    
    string body; //the body next to top level decl
    
    string retPart;
    
    retPart="if ( ";
    for(int i=0; i<declList.size(); i++){
        retPart+="doF"+to_string(i+1) +"==false";
        if(i!=declList.size()-1)
            retPart+=" && ";
    }
    retPart+=" ){\n return ;\n} ";
    
    
    
    //divide statments in the toplogical order into blocks "statments  between two function calls nodes
    
    vector< vector<DG_Node * > > stamentsOderedByTId;
    stamentsOderedByTId.resize(canFuseList.size());
    int blockId_=0;
    
    for (int i=0; i<topOrder.size(); i++) {
        
        if(topOrder[i]->StatmentInfo->isCallStmt){
            //dump stamentsOderedByTId
            //the block part
            blockId_++;
            string blockPart="//block "+to_string(blockId_)+"\n";
            //the call part
            string callPart;
            string nextCallParams;
            
            for(int j=0; j<stamentsOderedByTId.size(); j++){
                
                string funcId=to_string(j+1);
                string blockId=to_string(blockId_);
                string next_label="";
                if(j==declList.size()-1)
                    next_label+="_label_C"+to_string(blockId_);
                else
                    next_label+="_label_B"+blockId+"F"+to_string(j+2);
                
                blockPart+="\n_label_B"+blockId+"F"+funcId+":\n";
                blockPart+="if (doF"+funcId+") {\n";
                for(int k=0; k<stamentsOderedByTId[j].size();k++){
                    
                    if(stamentsOderedByTId[j][k]->StatmentInfo->stmt->getStmtClass()==clang::Stmt::DeclStmtClass){
                        
                        decls+="\t"+printer.printStmt(stamentsOderedByTId[j][k]->StatmentInfo->stmt,context_->getSourceManager(),declList[j]->getParamDecl(0),next_label,j+1 )+";\n";
                        
                    }else{
                        blockPart+=printer.printStmt(stamentsOderedByTId[j][k]->StatmentInfo->stmt,context_->getSourceManager(),declList[j]->getParamDecl(0),next_label,j+1 );
                        
                    }
                    
                    
                }
                blockPart+="}\n";
            }
            body+=blockPart;
            
            //  dump the function call
            callPart="\n_label_C"+to_string(blockId_)+":\n";
            
            //  callPart+=retPart+"\n\n";
            
            //put the new call
            
            map<int , DG_Node* > tidToCallExpr;
            //build next call params
            if(topOrder[i]->isMerged){
                
                for(set<DG_Node*>::iterator it=topOrder[i]->mergeInfo->mergedNodes.begin();
                    it!=topOrder[i]->mergeInfo->mergedNodes.end(); it++){
                    
                    tidToCallExpr[(*it)->traversalId]=(*it);
                    
                }
                
            }else{
                tidToCallExpr[topOrder[i]->traversalId]=topOrder[i];
                
            }
            
            string callCond="if ( (false ";
            for(int k=0; k<canFuseList.size();k++){
                if(tidToCallExpr.find(k)==tidToCallExpr.end()){
                    //if not participating
                    nextCallParams+=defaultParams[k];
                }else{
                    callCond+="|| doF"+to_string(k+1);
                    
                    clang::CallExpr * callExpr=
                    dyn_cast<clang::CallExpr>(tidToCallExpr[k]->StatmentInfo->stmt);
                    for(int h=1; h<callExpr->getNumArgs(); h++){
                        
                        nextCallParams+=", "+printer.printStmt(callExpr->getArg(h), context_->getSourceManager(), declList[k]->getParamDecl(0), "dummy", k+1);
                        
                    }
                }
                
            }
            callCond+=")==true){\n\t";
            //add doFx for each traversal, pass it as is except if traversal is not on the merged nodes then pass it false
            for(int k=0; k<declList.size(); k++){
                string doCallNext ="doF"+to_string(k+1);
                
                if(tidToCallExpr.find(k)==tidToCallExpr.end() ){
                    doCallNext="false";
                }
                nextCallParams+=","+doCallNext;
            }
            
            
            callPart+=callCond;
            callPart+=idName+"("+"_r->"+topOrder[i]->StatmentInfo->calledChild->getNameAsString();
            
            if(nextCallParams.size())
                callPart +=nextCallParams+");";
            
            callPart+="\n}";
            body+=callPart;
            
            stamentsOderedByTId.clear();
            stamentsOderedByTId.resize(canFuseList.size());
        }
        else{
            stamentsOderedByTId[topOrder[i]->traversalId].push_back(topOrder[i]);
            
        }
        
    }
    //add the final block
    blockId_++;
    string blockPart="//block "+to_string(blockId_)+"\n";
    //the call part
    string callPart;
    string nextCallParams;
    
    for(int j=0; j<stamentsOderedByTId.size(); j++){
        
        string funcId=to_string(j+1);
        string blockId=to_string(blockId_);
        string next_label="";
        if(j==declList.size()-1)
            next_label+="_label_C"+to_string(blockId_);
        else
            next_label+="_label_B"+blockId+"F"+to_string(j+2);
        
        blockPart+="\n_label_B"+blockId+"F"+funcId+":\n";
        blockPart+="if (doF"+funcId+") {\n";
        for(int k=0; k<stamentsOderedByTId[j].size();k++){
            
            if(stamentsOderedByTId[j][k]->StatmentInfo->stmt->getStmtClass()==clang::Stmt::DeclStmtClass){
                decls+="\t"+printer.printStmt(stamentsOderedByTId[j][k]->StatmentInfo->stmt,context_->getSourceManager(),declList[j]->getParamDecl(0),next_label,j+1 )+";\n";
                
            }else{
                blockPart+=printer.printStmt(stamentsOderedByTId[j][k]->StatmentInfo->stmt,context_->getSourceManager(),declList[j]->getParamDecl(0),next_label,j+1 );
                
            }
            
            
        }
        blockPart+="}\n";
    }
    body+=blockPart;
    callPart="\n_label_C"+to_string(blockId_)+":\n";
    callPart+="return ;\n";
    body+=callPart;
    
    string fullFun="//****** this fused method is generated by PLCL\n "+signeture+"{\n"+"//first level declarations\n"+decls+"\n//body\n"+body+"\n}\n\n";
    
    
    rewriter.InsertText(enclosingFunDecl->getLocStart(), fullFun);
    //  cout<<fullFun;
    
    
    return;
}