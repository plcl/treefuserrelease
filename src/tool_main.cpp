
//------------------------------------------------------------------------------
#include"logger.h"
#include <sstream>
#include <string>
#include <iostream>
#include "function_analyser.h"
#include "llvmdep.h"
#include "function_finder.h"
#include "record_analyser.h"
#include "fuse_transformation.h"

#include <assert.h>
#include <unistd.h>
static llvm::cl::OptionCategory MyToolCategory("my-tool options");
int main(int argc, const char **argv) {
    //create the tool ,parse source code and build ASTs
    clang::tooling::CommonOptionsParser OptionsParser(argc, argv, MyToolCategory);
    clang::tooling::ClangTool Tool(     OptionsParser.getCompilations(),OptionsParser.getSourcePathList());
    
    std::vector< std::unique_ptr< ASTUnit >> ASTList;
    Tool.buildASTs(ASTList);
    if(Tool.run(newFrontendActionFactory<clang::SyntaxOnlyAction>().get())==1){
        Logger::getStaticLogger().logError("source code has error ...");
        return 1 ;
    }
    

#ifdef TIMING    
     uint64_t        start;
     uint64_t        end;
     uint64_t        elapsed;
     uint64_t        elapsedNano;
     static mach_timebase_info_data_t    sTimebaseInfo;
    cout<<"start timing"<<endl;
     start = mach_absolute_time();
#endif    
    Logger::getStaticLogger().logInfo("*****perform tree_node, unique_child semantic checks and analyse records ");
    RecordsAnalyser recordAnalyser;
    for(int i=0; i<ASTList.size(); i++){
        
        recordAnalyser.analyseRecordDecls(&ASTList[i].get()->getASTContext());
    }
    
    
    Logger::getStaticLogger().logInfo("*****Identify  and analyse funcations with valid fuse attr ");
    FunctionFinder functionsInfo;
    
    for(int i=0; i<ASTList.size(); i++){
        
        functionsInfo.findFunctions(&ASTList[i].get()->getASTContext());
        
    }
    
    Logger::getStaticLogger().logInfo("*****build dependence graph for each sequence of fuse traversals and operating on the same node");
    for(int i=0; i<ASTList.size(); i++){
        CanFuseCandidateSearchTraversal candidatesFinder(&ASTList[i].get()->getASTContext(),&functionsInfo);
        candidatesFinder.TraverseDecl( ASTList[i].get()->getASTContext().getTranslationUnitDecl());
        candidatesFinder.rewriter.overwriteChangedFiles();
        
    }

#ifdef TIMING    
     end = mach_absolute_time();
     elapsed = end - start;
     if ( sTimebaseInfo.denom == 0 ) {
     (void) mach_timebase_info(&sTimebaseInfo);
     }
     elapsedNano = elapsed * sTimebaseInfo.numer / sTimebaseInfo.denom;
     cout<<"TreeFuser Time:"<<elapsed<<endl;
     cout<<"TreeFuser Time:"<<elapsedNano<<endl;
#endif    
    return 1;// Tool.run(newFrontendActionFactory(&findFunctions).get());
}
