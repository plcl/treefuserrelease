//
//  DependenceAnalyser.cpp
//  libtooling
//
//  Created by lsakka on 5/3/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#include "dependence_analyser.h"
#define ENABLECODEMOTION true

DDG * DependenceAnalyser::createDependnceGraph(vector<FunctionAnalyser * > traversals){
    map< int , map<StatmentInfo* ,DG_Node *>  > traversel_stmtToDGNode;
    
    DDG * depGraph =new DDG();
    for(int i=0; i<traversals.size();  i++){
        
        //create a corresponding graph node for each stmt
        for(int j=0; j<traversals[i]->statments.size(); j++){
            pair<StatmentInfo * , int> pair=make_pair(traversals[i]->statments[j],i);
            traversel_stmtToDGNode[i][pair.first]= depGraph->createNode(pair);	
        }
        
    }
    
    for(int i=0; i<traversals.size(); i++){
        addIntraTraversalDependecies(depGraph, traversals[i],traversel_stmtToDGNode[i]);
    }
    for(int i=0 ; i<traversals.size(); i++){
        for(int j=i+1 ; j<traversals.size(); j++)
            addInterTraversalDependecies(depGraph, traversals[i],traversals[j],traversel_stmtToDGNode[i],traversel_stmtToDGNode[j]);
        
    }
    
    return depGraph;
}


//two stmt will conflicts if access same value, and if ontree have the same tree access parts,
//local deps will be ignored except for intra dependence analysis,as the code will be
//duplicated with local variables names changed

bool DependenceAnalyser::haveSameValuePart(AccessPath * ap1, AccessPath * ap2 ){
    
    
    //special case
    if(ap1->isStrictAccessCall && ap2->isStrictAccessCall){
       
        if(ap1->attr.id==ap2->attr.id){
         
            if(ap1->isGlobal()){
                if( ap1->attr.id==8){
                    //cout<<  ap1->attr.id<<endl;
                    assert(false);
                }
              //  cout<<"two gloval strict accesses conflics\n";
            }
            return true;
        }
        else
            return false;
        
    }
    else if(ap1->isStrictAccessCall || ap2->isStrictAccessCall){
        return false;
    }
    //1-if oneof them is postfix of the other then there is conflict there
    
    //ther both refer to node ( no value) this function will assume true for this case
    if(ap1->hasValuePart()==false && ap2->hasValuePart()==false)
        return true;
    
    if(ap1->hasValuePart()==false && ap2->hasValuePart()==true)
        return false;
    
    if(ap1->hasValuePart()==true && ap2->hasValuePart()==false)
        return false;
    
    //both has Value part if one of smaller is postfix of the longer then there is problem
    AccessPath * shorter,*  longer;
    if(ap1->getValuePathSize()>=ap2->getValuePathSize())
    {
        shorter=ap2;
        longer=ap1;
    }else{
        shorter=ap1;
        longer=ap2;
    }
    
    //check that the shorter is postfix of the longer
    for(int i=shorter->getValueStartIndex(), j=longer->getValueStartIndex();
        i<(shorter->getValueStartIndex()+shorter->getValuePathSize()); i++, j++){
        if(shorter->getDeclAtIndex(i)!=longer->getDeclAtIndex(j))
            return false;
    }
    return true;
    
}



void  DependenceAnalyser::addCallToStmtDeps_helper(_def_AccessPathIterator ap2,_def_AccessPathIterator ap1,   FunctionAnalyser *  f1,DDG * depGraph,DG_Node * s2Node, DG_Node *  c1Node){
    
    
    
    StatmentInfo * stmt1ASTNode =s2Node->StatmentInfo;
    StatmentInfo * callStmtASTNode=c1Node->StatmentInfo;
    
    // C.{child in F1}*.{ap1 in F1}= ap2 iff ontree
    // ap1=ap2 if offtree
    
    if((*ap1)->isGlobal() && !(*ap2)->isGlobal())
        return;
    if((*ap1)->isOnTree() && !(*ap2)->isOnTree())
        return;
    //ignore locals (each itteration has its own local scope)
    if((*ap1)->isLocal() || (*ap2)->isLocal())
        return;
    
    if (this->haveSameValuePart(*ap1, *ap2)==false)
        return;
    
    if((*ap1)->isOffTree() ){
        depGraph->addDep(global_dep,c1Node, s2Node);
        return;
        
    }
    
    if((*ap2)->getTreeAccessPathSize() >=(*ap1)->getTreeAccessPathSize())
        return;
    
    //ap1 must start with the caled child for
    if( (*ap1)->splittedAccessPath[1].second != callStmtASTNode->getCalledChild())
        return;
    
    bool flag=true;
    //ap2 must be suffix of ap1-{C}.
    int i1, i2;
    for( i1=(*ap1)->getTreeAccessPathEndIndex() , i2=(*ap2)->getTreeAccessPathEndIndex();
        i2!=0; i2--, i1--){
        
        if((*ap1)->splittedAccessPath[i1].second!=(*ap2)->splittedAccessPath[i2].second)
        {
            flag=false; break;
        }
    }
    if(flag==false)
        return;
    
    else
        // remaing part of (ap1 - {C})-{ap2} must be in F2 childs
        
        assert(i1>=1);
    
    flag=true;
    while(i1>=1){
        
        if(!f1->isInCalledChildList( dyn_cast<clang::FieldDecl>( (*ap1)->splittedAccessPath[i1].second ))){
            flag=false;
            break;
        }
        i1--;
    }
    
    if(flag)
        depGraph->addDep(ontree_dep,c1Node, s2Node);
    
    
    return;
    
}

void  DependenceAnalyser::addStmtToCallDeps_helper(_def_AccessPathIterator ap1,_def_AccessPathIterator ap2,   FunctionAnalyser *  f2,DDG * depGraph,DG_Node * s1Node,DG_Node * c2Node){
    
    
    StatmentInfo * stmt1ASTNode =s1Node->StatmentInfo;
    StatmentInfo * callStmtASTNode=c2Node->StatmentInfo;
    
    // ap1= C.{child in F2}*.{ap2 in F2}.V and one is write at lease
    
    if((*ap1)->isGlobal() && !(*ap2)->isGlobal())
        return;
    if((*ap1)->isOnTree() && !(*ap2)->isOnTree())
        return;
    //ignore locals (each itteration has its own local scope)
    if((*ap1)->isLocal() || (*ap2)->isLocal())
        return;
    
    if (this->haveSameValuePart(*ap1, *ap2)==false)
        return;
    
    
    if((*ap1)->isOffTree() ){
        depGraph->addDep(global_dep,s1Node, c2Node);
        return;
        
    }
    
    if((*ap2)->getTreeAccessPathSize() >= (*ap1)->getTreeAccessPathSize())
        return;
    
    //ap1 must start with the caled child for
    if( (*ap1)->splittedAccessPath[1].second != callStmtASTNode->getCalledChild())
        return;
    
    bool flag=true;
    //ap2 must be suffix of ap1-{C}.
    int i1, i2;
    for( i1=(*ap1)->getTreeAccessPathEndIndex() , i2=(*ap2)->getTreeAccessPathEndIndex();
        i2!=0; i2--, i1--){
        
        if((*ap1)->splittedAccessPath[i1].second!=(*ap2)->splittedAccessPath[i2].second)
        {
            flag=false; break;
        }
    }
    if(flag==false)
        return;
    
    else
        // remaing part of (ap1 - {C})-{ap2} must be in F2 childs
        
        assert(i1>=1);
    
    flag=true;
    while(i1>=1){
        
        if(!f2->isInCalledChildList( dyn_cast<clang::FieldDecl>( (*ap1)->splittedAccessPath[i1].second ))){
            flag=false;
            break;
        }
        i1--;
    }
    
    if(flag)
        depGraph->addDep(ontree_dep,s1Node, c2Node);
    
    
    return;
    
}

//check if ap1=={ C in F}*.ap2
bool  ap1_eq_FAndAp2(_def_AccessPathIterator ap1,_def_AccessPathIterator ap2,   FunctionAnalyser *  f){
    
    
    if((*ap2)->getTreeAccessPathSize() >(*ap1)->getTreeAccessPathSize())
        return false;
    
    
    //ap2 must be suffix of ap1
    int i1, i2;
    for( i1=(*ap1)->getTreeAccessPathEndIndex() , i2=(*ap2)->getTreeAccessPathEndIndex();
        i2!=0; i2--, i1--){
        
        if((*ap1)->splittedAccessPath[i1].second!=(*ap2)->splittedAccessPath[i2].second)
            return false;
    }
    // remaing part of (ap1 -ap2} must be in F2 childs
    
    while(i1>=1){
        
        if(!f->isInCalledChildList( dyn_cast<clang::FieldDecl>( (*ap1)->splittedAccessPath[i1].second ))){
            return false;
        }
        i1--;
    }
    
    return true;
    
}

void DependenceAnalyser::addStmtToCallDeps(DDG * depGraph, DG_Node * s1Node, DG_Node * c2Node){
    
    StatmentInfo * stmt1ASTNode =s1Node->StatmentInfo;
    
    FunctionAnalyser * f2=c2Node->StatmentInfo->getEnclosingFunction();
    
    
    //read(S1) vs writes in f(C).
    for( _def_AccessPathIterator ap1=stmt1ASTNode->accessPaths.getReadSet().begin();
        ap1!=stmt1ASTNode->accessPaths.getReadSet().end();
        ap1++ ){
        {
            //for each ap2 in f2 in write area
            for(int i=0; i<f2->statments.size(); i++ )
            {
                
                for(_def_AccessPathIterator ap2=f2->statments[i]->accessPaths.getWriteSet().begin();
                    ap2!=f2->statments[i]->accessPaths.getWriteSet().end();
                    ap2++)
                {
                    
                    addStmtToCallDeps_helper(ap1,ap2,f2,depGraph,s1Node,c2Node);
                    
                }
                
            }
            
        }
    }
    
    //write(S1) vs reads/writes in f(C).
    for( _def_AccessPathIterator ap1=stmt1ASTNode->accessPaths.getWriteSet().begin();
        ap1!=stmt1ASTNode->accessPaths.getWriteSet().end();
        ap1++ ){
        {
            //for each ap2 in f2 in write area
            for(int i=0; i<f2->statments.size(); i++ )
            {
                
                for(_def_AccessPathIterator ap2=f2->statments[i]->accessPaths.getReadSet().begin();
                    ap2!=f2->statments[i]->accessPaths.getReadSet().end();
                    ap2++)
                {
                    
                    addStmtToCallDeps_helper(ap1,ap2,f2,depGraph,s1Node,c2Node);
                    
                }
                
            }
            //for each ap2 in f2 in write area
            for(int i=0; i<f2->statments.size(); i++ )
            {
                
                for(_def_AccessPathIterator ap2=f2->statments[i]->accessPaths.getWriteSet().begin();
                    ap2!=f2->statments[i]->accessPaths.getWriteSet().end();
                    ap2++)
                {
                    
                    addStmtToCallDeps_helper(ap1,ap2,f2,depGraph,s1Node,c2Node);
                    
                }
                
            }
            
        }
    }
    
}

void DependenceAnalyser::addCallToStmtDeps(DDG * depGraph, DG_Node * c1Node, DG_Node * s2Node){
    
    StatmentInfo * stmt2ASTNode =s2Node->StatmentInfo;
    FunctionAnalyser * f1=c1Node->StatmentInfo->getEnclosingFunction();
    

    
    //read(S2) vs writes in f(C).
    for( _def_AccessPathIterator ap2=stmt2ASTNode->accessPaths.getReadSet().begin();
        ap2!=stmt2ASTNode->accessPaths.getReadSet().end();
        ap2++ ){
        {
            //for each ap2 in f2 in write area
            for(int i=0; i<f1->statments.size(); i++ )
            {
                
                for(_def_AccessPathIterator ap1=f1->statments[i]->accessPaths.getWriteSet().begin();
                    ap1!=f1->statments[i]->accessPaths.getWriteSet().end();
                    ap1++)
                {
                    
                    addCallToStmtDeps_helper(ap1,ap2,f1,depGraph,s2Node,c1Node);
                    
                }
                
            }
            
        }
    }
    
    //write(S1) vs reads in f(C).
    for( _def_AccessPathIterator ap2=stmt2ASTNode->accessPaths.getWriteSet().begin();
        ap2!=stmt2ASTNode->accessPaths.getWriteSet().end();
        ap2++ ){
        {
            //for each ap2 in f2 in write area
            for(int i=0; i<f1->statments.size(); i++ )
            {
                
                for(_def_AccessPathIterator ap1=f1->statments[i]->accessPaths.getReadSet().begin();
                    ap1!=f1->statments[i]->accessPaths.getReadSet().end();
                    ap1++)
                {
                    
                    addCallToStmtDeps_helper(ap1,ap2,f1,depGraph,s2Node,c1Node);
                    
                }
                
                //for each ap2 in f2 in write area
                for(int i=0; i<f1->statments.size(); i++ )
                {
                    
                    for(_def_AccessPathIterator ap1=f1->statments[i]->accessPaths.getWriteSet().begin();
                        ap1!=f1->statments[i]->accessPaths.getWriteSet().end();
                        ap1++)
                    {
                        
                        addCallToStmtDeps_helper(ap1,ap2,f1,depGraph,s2Node,c1Node);
                        
                    }
                    
                }
                
            }
            
        }
    }
    
    
    
}

void DependenceAnalyser:: addCallToCallDeps(DDG * depGraph, DG_Node * c1Node, DG_Node * c2Node,bool intra){
    
    
    
    for(int i=0; i<c1Node->StatmentInfo->getEnclosingFunction()->statments.size(); i++){
        StatmentInfo * stmt1=c1Node->StatmentInfo->getEnclosingFunction()->statments[i];
        
        for(int j=0 ; j<c2Node->StatmentInfo->getEnclosingFunction()->statments.size();j++)
        {
            StatmentInfo * stmt2=c2Node->StatmentInfo->getEnclosingFunction()->statments[j];
            
            //W vs W/R
            for( _def_AccessPathIterator ap1=stmt1->accessPaths.getWriteSet().begin();
                ap1!=stmt1->accessPaths.getWriteSet().end();
                ap1++ ){
                {
                    
                    for(_def_AccessPathIterator ap2=stmt2->accessPaths.getWriteSet().begin();
                        ap2!=stmt2->accessPaths.getWriteSet().end();
                        ap2++)
                    {
                        
                        if((*ap1)->isGlobal() && !(*ap2)->isGlobal())
                            continue;
                        if((*ap1)->isOnTree() && !(*ap2)->isOnTree())
                            continue;
                        //ignore locals (each itteration has its own local scope)
                        if((*ap1)->isLocal() || (*ap2)->isLocal())
                            continue;
                        
                        if (this->haveSameValuePart(*ap1, *ap2)==false)
                            continue;
                        
                        
                        if((*ap1)->isOffTree() ){
                            depGraph->addDep(global_dep,c1Node, c2Node);
                            continue;
                            
                        }else{//on Tree
                            if(c1Node->StatmentInfo->getCalledChild()==c2Node->StatmentInfo->getCalledChild()){
                                // c1Node->StatmentInfo->stmt->dump();
                                //  c2Node->StatmentInfo->stmt->dump();
                                
                                bool isDep=false;
                                
                                if(ap1_eq_FAndAp2(ap1, ap2, c2Node->StatmentInfo->getEnclosingFunction()))
                                    isDep=true;
                                
                                // {..}*.ap1=ap2
                                if(isDep == false && ap1_eq_FAndAp2(ap2, ap1, c1Node->StatmentInfo->getEnclosingFunction()))
                                    isDep=true;
                                
                                
                                
                                if(isDep){
                                    if(intra==false)
                                        depGraph->addDep(ontree_dep_fusable, c1Node, c2Node);
                                    else
                                        depGraph->addDep(ontree_dep  , c1Node, c2Node);
                                }
                                
                                
                            }
                        }
                        
                        
                    }
                    
                    for(_def_AccessPathIterator ap2=stmt2->accessPaths.getReadSet().begin();
                        ap2!=stmt2->accessPaths.getReadSet().end();
                        ap2++)
                    {
                        
                        if((*ap1)->isGlobal() && !(*ap2)->isGlobal())
                            continue;
                        if((*ap1)->isOnTree() && !(*ap2)->isOnTree())
                            continue;
                        //ignore locals (each itteration has its own local scope)
                        if((*ap1)->isLocal() || (*ap2)->isLocal())
                            continue;
                        
                        if (this->haveSameValuePart(*ap1, *ap2)==false)
                            continue;
                        
                        
                        if((*ap1)->isOffTree() ){
                            depGraph->addDep(global_dep,c1Node, c2Node);
                            continue;
                            
                        }else{//on Tree
                            if(c1Node->StatmentInfo->getCalledChild()==c2Node->StatmentInfo->getCalledChild()){
                                
                                bool isDep=false;
                                
                                if(ap1_eq_FAndAp2(ap1, ap2, c2Node->StatmentInfo->getEnclosingFunction()))
                                    isDep=true;
                                
                                // {..}*.ap1=ap2
                                if(isDep==false && ap1_eq_FAndAp2(ap2, ap1, c1Node->StatmentInfo->getEnclosingFunction()))
                                    isDep=true;
                                
                                
                                
                                if(isDep){
                                    if(intra==false)
                                        depGraph->addDep(ontree_dep_fusable, c1Node, c2Node);
                                    else
                                        depGraph->addDep(ontree_dep  , c1Node, c2Node);
                                }
                                
                                
                            }
                        }
                        
                        
                    }
                    
                }
            }
            
            //R vs W
            for( _def_AccessPathIterator ap1=stmt1->accessPaths.getReadSet().begin();
                ap1!=stmt1->accessPaths.getReadSet().end();
                ap1++ ){
                {
                    
                    for(_def_AccessPathIterator ap2=stmt2->accessPaths.getWriteSet().begin();
                        ap2!=stmt2->accessPaths.getWriteSet().end();
                        ap2++)
                    {
                        
                        if((*ap1)->isGlobal() && !(*ap2)->isGlobal())
                            continue;
                        if((*ap1)->isOnTree() && !(*ap2)->isOnTree())
                            continue;
                        //ignore locals (each itteration has its own local scope)
                        if((*ap1)->isLocal() || (*ap2)->isLocal())
                            continue;
                        
                        if (this->haveSameValuePart(*ap1, *ap2)==false)
                            continue;
                        
                        
                        if((*ap1)->isOffTree() ){
                            depGraph->addDep(global_dep,c1Node, c2Node);
                            continue;
                            
                        }else{//on Tree
                            if(c1Node->StatmentInfo->getCalledChild()==c2Node->StatmentInfo->getCalledChild()){
                                
                                bool isDep=false;
                                
                                if(ap1_eq_FAndAp2(ap1, ap2, c2Node->StatmentInfo->getEnclosingFunction()))
                                    isDep=true;
                                
                                // {..}*.ap1=ap2
                                if(isDep==false && ap1_eq_FAndAp2(ap2, ap1, c1Node->StatmentInfo->getEnclosingFunction()))
                                    isDep=true;
                                
                                
                                
                                if(isDep){
                                    if(intra==false)
                                        depGraph->addDep(ontree_dep_fusable, c1Node, c2Node);
                                    else
                                        depGraph->addDep(ontree_dep  , c1Node, c2Node);
                                }
                                
                                
                            }
                        }
                        
                        
                    }
                    
                    
                }
            }
            
            
        }
        
        
    }
    
    
    
    //offTree Cases
    //ap1 in F1=ap2 in F2 then conflict exists
}



void DependenceAnalyser::addStmtToStmtDependencies(DDG * depGraph, DG_Node * s1Node, DG_Node * s2Node,const _def_AccessPathSet & s1ApSet,const  _def_AccessPathSet &  s2ApSet , bool intra){
    
    
    //check for
    for( _def_AccessPathIterator it1=s1ApSet.begin();it1!=s1ApSet.end(); it1++){
        
        for(_def_AccessPathIterator it2=s2ApSet.begin(); it2!=s2ApSet.end();it2++){
            if((*it1)->isGlobal() && !(*it2)->isGlobal())
                continue;
            if((*it1)->isOnTree() && !(*it2)->isOnTree())
                continue;
            //ignore local if not intra
            if(!intra &  ((*it1)->isLocal() || (*it2)->isLocal()))
                continue;
            if(intra && (*it1)->isLocal() && !(*it2)->isLocal())
                continue;
            
            if(haveSameValuePart(*it1,*it2)==false)
                continue;
            
            if((*it1)->isOffTree()){
                depGraph->addDep(global_dep,s1Node, s2Node);
                continue;
            }
            //in case of stmt to stmt both of TAP "tree access part"  must be the same
            if((*it1)->isOnTree()){
                
                if((*it1)->getTreeAccessPathSize()!=(*it2)->getTreeAccessPathSize())
                    continue;
                
                for(int i=1 ; i<=(*it1)->getTreeAccessPathEndIndex(); i++){
                    
                    if((*it1)->splittedAccessPath[i].second!=(*it2)->splittedAccessPath[i].second)
                        continue;
                }
                
                depGraph->addDep(ontree_dep,s1Node, s2Node);
                
            }
            
        }
    }
    
}


void DependenceAnalyser::addIntraTraversalDependecies(DDG * depGraph , FunctionAnalyser * traversal , map<StatmentInfo* , DG_Node * > & stmtToGNode){
    
    bool hasAnyWrite=false;//OR read of pointer
    for(int k=0; k<traversal->statments.size() ; k++){
        
        if(traversal->statments[k]->accessPaths.getWriteSet().size()!=0){
            hasAnyWrite=true;
            break;
        }
    }
   
    
    for(int i=0; i<traversal->statments.size(); i++){
        StatmentInfo *  stmt1=traversal->statments[i];
        assert(stmt1->idInTraversals==i);
        
        for(int j=i+1; j<traversal->statments.size(); j++){
            
            StatmentInfo*   stmt2=traversal->statments[j];
            
            if( ENABLECODEMOTION ==false){
                depGraph->addDep(control_dep, stmtToGNode[stmt1], stmtToGNode[stmt2]);
            }
            
            assert(stmt2->idInTraversals==j);
            
            bool stmt2hasPointerRead=false;//OR read of pointer
            
            //for each read access path
            for(_def_AccessPathIterator it=stmt2->accessPaths.getReadSet().begin();
                it!=stmt2->accessPaths.getReadSet().end();it++){
                
                if((*it)->isOnTree()){
                    stmt2hasPointerRead=true;
                    break;
                }
            }
            
            
            if(stmt1->hasReturn && (stmt2hasPointerRead))
                depGraph->addDep(control_dep, stmtToGNode[stmt1], stmtToGNode[stmt2]);

            
            
            
            if(stmt1->hasReturn && (stmt2->isCallStmt==false)  && stmt2->accessPaths.getWriteSet().size()!=0)
                depGraph->addDep(control_dep, stmtToGNode[stmt1], stmtToGNode[stmt2]);
            
            if(stmt2->hasReturn && (stmt1->isCallStmt==false)  && stmt1->accessPaths.getWriteSet().size()!=0)
                depGraph->addDep(control_dep, stmtToGNode[stmt1], stmtToGNode[stmt2]);
            
            //these can be more restrict if we say that if the call stmt
            if(stmt1->hasReturn && (stmt2->isCallStmt==true)  && hasAnyWrite)
                depGraph->addDep(control_dep, stmtToGNode[stmt1], stmtToGNode[stmt2]);
            
            if(stmt2->hasReturn && (stmt1->isCallStmt==true)  && hasAnyWrite)
                depGraph->addDep(control_dep, stmtToGNode[stmt1], stmtToGNode[stmt2]);
            
            
            //stmt To stmt dependecies
            
            addStmtToStmtDependencies(depGraph,stmtToGNode[stmt1], stmtToGNode[stmt2], stmt1->accessPaths.getReadSet(),stmt2->accessPaths.getWriteSet(),true);
            
            
            addStmtToStmtDependencies(depGraph,stmtToGNode[stmt1], stmtToGNode[stmt2], stmt1->accessPaths.getWriteSet(),stmt2->accessPaths.getReadSet(),true);
            
            
            addStmtToStmtDependencies(depGraph,stmtToGNode[stmt1], stmtToGNode[stmt2], stmt1->accessPaths.getWriteSet(),stmt2->accessPaths.getWriteSet(),true);
            
            //stmt To call
            if(stmt2->isCallStmt)
                addStmtToCallDeps(depGraph, stmtToGNode[stmt1], stmtToGNode[stmt2]);
            
            if(stmt1->isCallStmt)
                addCallToStmtDeps(depGraph, stmtToGNode[stmt1], stmtToGNode[stmt2]);
            
            //call to call
            if(stmt1->isCallStmt && stmt2->isCallStmt)
                addCallToCallDeps(depGraph,stmtToGNode[stmt1], stmtToGNode[stmt2],true);
            
        }
    }
}

void DependenceAnalyser::addInterTraversalDependecies(DDG * depGraph , FunctionAnalyser * traversal1, FunctionAnalyser * traversal2 , map<StatmentInfo* , DG_Node * > & stmtToGNodeT1 , map<StatmentInfo* , DG_Node * > & stmtToGNodeT2 ){
    
    
    for(int i=0; i<traversal1->statments.size(); i++){
        StatmentInfo *  stmt1=traversal1->statments[i];
        
        for(int j=0; j<traversal2->statments.size(); j++){
            
            StatmentInfo*   stmt2=traversal2->statments[j];
         
            
            addStmtToStmtDependencies(depGraph,stmtToGNodeT1[stmt1], stmtToGNodeT2[stmt2], stmt1->accessPaths.getReadSet(),stmt2->accessPaths.getWriteSet(),false);
            
            
            addStmtToStmtDependencies(depGraph,stmtToGNodeT1[stmt1], stmtToGNodeT2[stmt2], stmt1->accessPaths.getWriteSet(),stmt2->accessPaths.getReadSet(),false);
            
            
            addStmtToStmtDependencies(depGraph,stmtToGNodeT1[stmt1], stmtToGNodeT2[stmt2], stmt1->accessPaths.getWriteSet(),stmt2->accessPaths.getWriteSet(),false);
            
            //stmt To call
            if(stmt2->isCallStmt)
                addStmtToCallDeps(depGraph,stmtToGNodeT1[stmt1], stmtToGNodeT2[stmt2]);
            
            if(stmt1->isCallStmt)
                addCallToStmtDeps(depGraph ,stmtToGNodeT1[stmt1], stmtToGNodeT2[stmt2]);
            
            //call to stmt
            if(stmt1->isCallStmt && stmt2->isCallStmt)
                addCallToCallDeps(depGraph,stmtToGNodeT1[stmt1], stmtToGNodeT2[stmt2],false);
            
        }
    }
}



