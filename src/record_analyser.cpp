//
//  RecordsAnalyser.cpp
//  libtooling
//
//  Created by lsakka on 1/5/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#include "record_analyser.h"
#include "logger.h"

map<clang::ASTContext * , map<const clang::RecordDecl *,RecordInfo *  > > RecordsAnalyser::recordsInfoStore=map<clang::ASTContext * , map<const clang::RecordDecl *,RecordInfo *  > > ();


const set<clang::FieldDecl *>&  RecordsAnalyser::getChildAccessDecls(const clang::RecordDecl * recordDecl){
    
    ASTContext * context=& recordDecl->getASTContext();
    assert( RecordsAnalyser::recordsInfoStore[context][recordDecl]->_isTreeNode==1);
    
    return RecordsAnalyser::recordsInfoStore[context][recordDecl]->getChildAccessDecls();
    
}

const RecordInfo & RecordsAnalyser:: getRecordInfo(const clang::RecordDecl * recordDecl){
    
    ASTContext * context=& recordDecl->getASTContext();
    return *RecordsAnalyser::recordsInfoStore[context][recordDecl];
    
}

//if all members are scalers and members of members..
bool RecordsAnalyser::isCompleteScaler( clang::ValueDecl  * const  s){
    
    if(s->getType()->isBuiltinType())
        return true;
    
    else if(s->getType()->isClassType()){
        clang::CXXRecordDecl * tmp=s->getType()->getAsCXXRecordDecl();
        if(RecordsAnalyser::recordsInfoStore[&(s->getASTContext())][tmp]!=NULL && RecordsAnalyser::recordsInfoStore[&(s->getASTContext())][tmp]->isCompleteScaler!=-1
           )
            return RecordsAnalyser::recordsInfoStore[&(s->getASTContext())][tmp]->isCompleteScaler;
        else{
            clang::RecordDecl::field_iterator it= tmp->field_begin();
            it= tmp->field_begin();
            while(it!=tmp->field_end()){
                if(!isCompleteScaler(*it)){
                    return RecordsAnalyser::recordsInfoStore[&(s->getASTContext())][tmp]->isCompleteScaler=false;
                }
                it++;
            }
            
        }
        return RecordsAnalyser::recordsInfoStore[&(s->getASTContext())][tmp]->isCompleteScaler=true;
    }
    else
        return false;
    
}

//if is bulit in or not object (not pointer or reference) return true
bool RecordsAnalyser::isScaler( clang::ValueDecl  * const decl){
    
    if(decl->getType()->isBuiltinType())
        return true;
    
    else if(decl->getType()->isClassType()|| decl->getType()->isStructureType()){
        return true;
    }
    else
        return false;
    
}

bool RecordsAnalyser::VisitCXXRecordDecl(clang::CXXRecordDecl * s){
    
    ASTContext * context=& s->getASTContext();
    
    
    RecordInfo * recordInfo=new RecordInfo();
    if (RecordsAnalyser::recordsInfoStore[context].find(s) != RecordsAnalyser::recordsInfoStore[context].end() ){
        Logger::getStaticLogger().logWarn("RecordsAnalyser::VisitCXXRecordDecl : record allready analysed");
        return true;
    }else{
        RecordsAnalyser::recordsInfoStore[context][s]= recordInfo;
    }
    if(/*s->hasAttr<TreeNodeAttr>()*/ hasTreeNodeAnnotation(s) ){
        
        // Logger::getStaticLogger().logInfo("start checking tree_node semantic for class \""+s->getNameAsString()+"\"");
        
        //check that it have member that is pointer to itslef
        clang::RecordDecl::field_iterator it= s->field_begin();
        while(it!=s->field_end()){
            
            if(hasUniqueChildAnnotation(*it)){
                if(!(*it)->getType()->isPointerType()){
                    Logger::getStaticLogger().logError("unique_child attr must be poniter and recursive , unique_child attr is dropped");
                    (*it)->dropAttr<AnnotateAttr>();
                }
                else if(!( (*it)->getType()->getPointeeCXXRecordDecl()==s )){//TODO: check if this is correct always (is it safe)!
                    
                    bool flag=false;
                    for(clang::CXXRecordDecl::base_class_iterator it2 =s->bases_begin(); it2!=s->bases_end(); it2++)
                    {
                        
                        if((*it)->getType()->getPointeeCXXRecordDecl()==it2->getType()->getAsCXXRecordDecl())
                            flag=true;
                        
                    }
                    if(!flag){
                        Logger::getStaticLogger().logError("unique_child is not refering to the container type!, unique_child attr is dropped");
                        (*it)->dropAttr<AnnotateAttr>();
                    }
                }
                else{
                    recordInfo->childAccessDecls.insert((*it));
                }
            }
            it++;
        }
        /* commentd 10 June
         if( recordInfo->childAccessDecls.size()  ==0){
         Logger::getStaticLogger().logError("class \""+s->getNameAsString()+"\" does not have field with unique_child attr, tree_node attr is dropped");
         s->dropAttr<TreeNodeAttr>();
         recordInfo->_isTreeNode=false;
         return true;
         }*/
        
        //all fields must be complete scalers ( why!)
        /*
         bool  isCompleteScaler=true;
         it= s->field_begin();
         while(it!=s->field_end()){
         if((*it)->hasAttr<UniqueChildAttr>()==false){
         if(RecordsAnalyser::isCompleteScaler(*it)==false){
         isCompleteScaler=false;
         break;
         }
         
         }
         it++;
         }
         if(isCompleteScaler==false){
         Logger::getStaticLogger().logError("tree_node object non-scaler member not allowed "+(*it)->getType().getAsString()+" tree_node attribute is dropped");
         s->dropAttr<TreeNodeAttr>();
         recordInfo->_isTreeNode=0;
         }else{*/
        recordInfo->_isTreeNode=1;
        Logger::getStaticLogger().logInfo("class \""+s->getNameAsString()+"\"is a tree_node");
        //}
        
    }
    else{
        recordInfo->_isTreeNode=false;
    }
    return true;
}

void RecordsAnalyser::analyseRecordDecls(ASTContext  * context_){
    
    this->TraverseDecl(context_->getTranslationUnitDecl());
    
}