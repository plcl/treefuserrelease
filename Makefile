ifndef VERBOSE
		QUIET:=@
endif
CXX = g++
LLVM_CONFIG?=llvm-config
LDFLAGS+=$(shell $(LLVM_CONFIG) --cppflags --ldflags)
LDFLAGS+= -ldl -ltinfo -lpthread
COMMON_FLAGS=-Wall -Wextra -fno-rtti -Wno-sign-compare
CXXFLAGS+=$(COMMON_FLAGS) $(shell $(LLVM_CONFIG) --cxxflags)
CPPFLAGS+=$(shell $(LLVM_CONFIG) --cppflags) -fpermissive -I./header
CLANGLIBS = \
	-Wl,--start-group \
	-lclang \
	-lclangAST \
	-lclangFrontendTool \
	-lclangFrontend \
	-lclangDriver \
	-lclangSerialization \
	-lclangCodeGen \
	-lclangParse \
	-lclangSema \
	-lclangStaticAnalyzerCheckers \
	-lclangStaticAnalyzerCore \
	-lclangAnalysis \
	-lclangIndex \
	-lclangRewrite \
	-lclangARCMigrate \
	-lclangEdit \
	-lclangAST \
	-lclangLex \
	-lclangBasic \
	-lclangFormat \
	-lclangToolingCore \
	-lclangIndex \
	-lclangEdit \
	-lclangRewriteFrontend \
	-lclangEdit \
	-lclangASTMatchers \
	-lclangRewrite \
	-lclangRewriteFrontend \
	-lclangStaticAnalyzerFrontend \
	-lclangStaticAnalyzerCheckers \
	-lclangStaticAnalyzerCore \
	-lclangSerialization \
	-lclangToolingCore \
	-lclangTooling \
	-Wl,--end-group
	
LLVMLIBS=$(shell $(LLVM_CONFIG) --libs --system-libs)

SOURCEDIR    = src
BUILDDIR     = build

SOURCE      = $(wildcard ./src/*.cpp)
HEADER      = $(wildcard ./header/*.h)
OBJECT1     =$(SOURCE:.cpp=.o)
OBJECT      = $(subst src,build, $(OBJECT1))
EXECUTABLE = treefuser

all: default

default: directory $(OBJECT)
	@echo Linking Project
	$(QUIET)$(CXX) -o $(EXECUTABLE) $(CXXFLAGS) $(OBJECT) $(CLANGLIBS) $(LLVMLIBS) $(LDFLAGS)

directory:
	mkdir -p $(BUILDDIR) 
	@echo done from here
	@echo $(SOURCE) 
	@echo $(OBJECT) 

$(BUILDDIR)/%.o : $(SOURCEDIR)/%.cpp $(HEADER)
	@echo $@
	@echo Compiling $*.cpp
	$(QUIET)$(CXX) -o $@ -c $(CPPFLAGS) $(CXXFLAGS) $<

clean:
	$(QUIET)rm -rf $(EXECUTABLE) $(BUILDDIR)

