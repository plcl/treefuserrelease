#include "main.h"
int num = 0;



int main(int argc, char** argv)
{
    if (argc < 2) {
        cout << "Usage: ./run <FILE>" << endl;
        return -1;
    }
    
    string filename(argv[1]);
    ifstream file(filename);
    string str((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
    str.erase(remove_if(str.begin(), str.end(), ::isspace), str.end());
    str.erase(remove_if(str.begin(), str.end(), ::iscntrl), str.end());
#ifndef FUSE_ONLY
    
     try {
     XMLPlatformUtils::Initialize();
     }
     catch (const XMLException & toCatch) {
     char* message = XMLString::transcode(toCatch.getMessage());
     cout << "Error during initialization! :" << endl;
     cout << message << endl;
     XMLString::release(&message);
     return 1;
     }
     
     XercesDOMParser* parser = new XercesDOMParser();
     parser->setValidationScheme(XercesDOMParser::Val_Always);
     optional
     parser->setDoNamespaces(true); 
     
     ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
     parser->setErrorHandler(errHandler);
     
     
     MemBufInputSource buffin((const XMLByte*)str.c_str(), str.size(), "str");
     
     try {
     parser->parse(buffin);
     }
     catch (const XMLException& toCatch) {
     char* message = XMLString::transcode(toCatch.getMessage());
     cout << "Exception message is: " << endl;
     cout << message << endl;
     XMLString::release(&message);
     return -1;
     }
     catch (const DOMException& toCatch) {
     char* message = XMLString::transcode(toCatch.msg);
     cout << "Exception message is: " << endl;
     cout << message << endl;
     XMLString::release(&message);
     return -1;
     }
     catch (...) {
     cout << "Unexpected Exception " << endl;
     return -1;
     }
     
     DOMDocument* doc = parser->getDocument(); 
     DOMNode* root    = doc->getDocumentElement();
     Layout Tree Building
     Box* tree = buildLayoutTree(root);
#endif
    Box* tree ;
    
    //Succnum Calculation
    traverseCountSuccessors(tree);
    
    //Horizontal Size Update
    traverseHorizontalSizeUpdate(tree);
    
    //Position Update
    traversePositionUpdate(tree);
    
    //Vertical Size Update
    traverseVerticalSizeUpdate(tree);
    
    //Position Update
    traversePositionUpdate(tree);
    
    //Color Update
    traverseColorUpdate(tree, 1, 3);
   
    int succnum = tree->succnum;
    
    //Printing Total Nodes Traversed
    cout << "Number of Nodes " << num << endl;
    
#ifndef FUSE_ONLY
    destroyLayoutTree(tree);
    
    delete parser;
    delete errHandler;
#endif
    
    return 0;
}
