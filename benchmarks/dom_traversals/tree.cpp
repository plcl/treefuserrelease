#include "tree.h"
//Globals
string nodelbox("nodel");
string noderbox("noder");
string nodetitle("title");
string nodeheight("height");
string nodewidth("width");
string nodecolor("color");

Box::Box()
{
		this->title     = "";
		this->height    = 0;
		this->width     = 0;
		this->color     = "";
		this->succnum   = 0;
		this->x         = 0;
		this->y         = 0;
		this->alignment = false;
		this->left      = NULL;
		this->right     = NULL;
}
#ifndef FUSE_ONLY
Box* buildLayoutTree(DOMNode* node)
{
		if (node == NULL) return NULL;

		Box* n = new Box();

		DOMNodeList* children = node->getChildNodes();
		for (int i = 0; i < children->getLength(); ++i) {
				if (DOMNode::ELEMENT_NODE == children->item(i)->getNodeType()){
						string name(XMLString::transcode(children->item(i)->getNodeName()));
						if (nodelbox == name){
								n->left = buildLayoutTree(children->item(i));
					    }
						else if (noderbox == name){
								n->right = buildLayoutTree(children->item(i));
					    }
						else if (nodetitle == name){
								string str(XMLString::transcode(dynamic_cast<DOMText*>(children->item(i)->getFirstChild())->getWholeText()));
								n->title = str;
					    }
						else if (nodeheight == name){
								string str(XMLString::transcode(dynamic_cast<DOMText*>(children->item(i)->getFirstChild())->getWholeText()));
								n->height = atoi(str.c_str());
					    }
						else if (nodewidth == name){
								string str(XMLString::transcode(dynamic_cast<DOMText*>(children->item(i)->getFirstChild())->getWholeText()));
								n->width = atoi(str.c_str());
					    }
						else if (nodecolor == name){
								string str(XMLString::transcode(dynamic_cast<DOMText*>(children->item(i)->getFirstChild())->getWholeText()));
								n->color = str;
					    }
				}
		}
		
				
		return n;
}

void destroyLayoutTree(Box* node)
{
		if (node == NULL) return;
		destroyLayoutTree(node->left);
		destroyLayoutTree(node->right);
		free(node);
}
#endif
