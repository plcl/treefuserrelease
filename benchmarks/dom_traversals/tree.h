#ifndef TREE_H_
#define TREE_H_

#include <cstdlib>
#include <string>
#include <iostream>
#include "common.h"

#ifndef FUSE_ONLY

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
using namespace xercesc;

#endif


using namespace std;


class  TF_TreeStructure Box
{
		public:
				Box();
				~Box(){};
				string title;
				int height;
				int width;
				int color;
				int succnum;
				int x;
				int y;
				bool alignment;
                TF_TreeRecursiveField Box* left;
                TF_TreeRecursiveField Box* right;
};
#ifndef FUSE_ONLY
Box*  buildLayoutTree(DOMNode* node);  //building layout tree
void  destroyLayoutTree(Box* node);    //freeing layout treeojioi-0
#endif
#endif
 
