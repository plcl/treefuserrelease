#ifndef TRAVERSAL_H_
#define TRAVERSAL_H_

#include "tree.h"
#include "common.h"



void  traversePrint(Box* node);
TF_Traversal void  traverseCountSuccessors(Box* node);
TF_Traversal void traverseHorizontalSizeUpdate(Box* node);
TF_Traversal void  traverseVerticalSizeUpdate(Box* node);
TF_Traversal void traversePositionUpdate(Box* node);
TF_Traversal void traverseColorUpdate(Box* node, int low, int high);


void traversePrint(Box* node)
{
    if (node == NULL) return;
    
    //num++;
    cout << "Title      :" << node->title   << endl;
    cout << "Heigtht    :" << node->height  << endl;
    cout << "Width      :" << node->width   << endl;
    cout << "Color      :" << node->color   << endl;
    cout << "SuccNum    :" << node->succnum << endl;
    cout << "X Position :" << node->x       << endl;
    cout << "Y Position :" << node->y       << endl;
    cout << endl;
    traversePrint(node->left);
    traversePrint(node->right);
}

void TF_Traversal traverseCountSuccessors(Box* node)
{
    if (node == NULL) return;
    
    //num =num+1;
    
    traverseCountSuccessors(node->left);
    traverseCountSuccessors(node->right);
    int l=0;
    int r=0;
    
    if(node->left!=NULL)
        l = node->left->succnum;
    
    if(node->right!=NULL)
        r = node->left->succnum;
    
    
    
    if(node->left != NULL && node->right != NULL) node->succnum = l + r + 2;
    else if(node->left != NULL && node->right == NULL) node->succnum = l + 1;
    else if(node->left == NULL && node->right != NULL) node->succnum = r + 1;
    else node->succnum = 0;
    
    return;
}

void TF_Traversal traverseHorizontalSizeUpdate(Box* node)
{
    if (node == NULL) return;
    
    //num=num+1;
    
    traverseHorizontalSizeUpdate(node->left);
    traverseHorizontalSizeUpdate(node->right);
    
    if(node->left != NULL && node->right != NULL) {
        if(node->left->height > node->right->height){
            node->height =node->left->height ;
        }else{
            node->height =node->right->height ;
            
        }
        node->width  = node->left->width + node->right->width;
    }
    else if(node->left != NULL && node->right == NULL) {
        node->height = node->left->height;
        node->width  = node->left->width;
    }
    else if(node->left == NULL && node->right != NULL) {
        node->height = node->right->height;
        node->width  = node->right->width;
    }
    node->alignment = false;
    
    return;
}

void TF_Traversal  traverseVerticalSizeUpdate(Box* node)
{
    if (node == NULL) return;
    
    //num=num+1;
    
    traverseVerticalSizeUpdate(node->left);
    traverseVerticalSizeUpdate(node->right);
    
    if(node->left != NULL && node->right != NULL) {
        node->height  = node->left->height + node->right->height;
        if(node->left->width > node->right->width){
        
            node->width = node->left->width;
        }else{
            node->width = node->right->width;

        }
    }
    else if(node->left != NULL && node->right == NULL) {
        node->height = node->left->height;
        node->width  = node->left->width;
    }
    else if(node->left == NULL && node->right != NULL) {
        node->height = node->right->height;
        node->width  = node->right->width;
    }
    node->alignment = true;
    
    return;
}

void TF_Traversal  traversePositionUpdate(Box* node)
{
    if(node == NULL) return;
    
    //num=num+1;
    
    if (node->alignment==false) {
        if(node->left != NULL && node->right != NULL) {
            node->left->x  = node->x;
            node->left->y  = node->y;
            node->right->x = node->x + node->left->width;
            node->right->y = node->y;
            
        }
        else if(node->left != NULL && node->right == NULL) {
            node->left->x  = node->x;
            node->left->y  = node->y;
        }
        else if(node->left == NULL && node->right != NULL) {
            node->right->x  = node->x;
            node->right->y  = node->y;
        }
    }
    else {
        if(node->left != NULL && node->right != NULL) {
            node->left->x  = node->x;
            node->left->y  = node->y;
            node->right->x = node->x;
            node->right->y = node->y + node->left->height;
        }
        else if(node->left != NULL && node->right == NULL) {
            node->left->x  = node->x;
            node->left->y  = node->y;
        }
        else if(node->left == NULL && node->right != NULL) {
            node->right->x  = node->x;
            node->right->y  = node->y;
        }
    }
    
    traversePositionUpdate(node->left);
    traversePositionUpdate(node->right);
    
    return;
}

#define RED 3
void TF_Traversal  traverseColorUpdate(Box* node, int low, int high)
{
    if (node == NULL) return;
    else if (node->succnum < low) return;
    
    //num=num+1;
    
  //  string red("red");
    if (node->succnum > low && node-> succnum < high) node->color = RED;
    
    traverseColorUpdate(node->left, low, high);
    traverseColorUpdate(node->right, low, high);
    
    return;
}




#endif
