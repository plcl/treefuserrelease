#ifndef MAIN_H_
#define MAIN_H_
#define FUSE_ONLY

#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <algorithm>


#ifndef FUSE_ONLY

#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#endif


#include "tree.h"
#include "traversal.h"
#include "common.h"

using namespace std;
//using namespace xercesc;

#endif
