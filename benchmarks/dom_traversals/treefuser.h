//
//  treefuser.h
//  libtooling
//
//  Created by lsakka on 7/5/17.
//  Copyright © 2017 lsakka. All rights reserved.
//

#ifndef treefuser_h
#define treefuser_h

#define TF_TreeStructure        __attribute__(( annotate("TreeNode")))
#define TF_TreeRecursiveField   __attribute__(( annotate("UniqueChild")))
#define TF_Traversal            __attribute__((annotate("fuse")))

#define TF_AbsrtactedAccess(AccessList)   __attribute__(( annotate( "strict_access" #AccessList )))

#endif /* treefuser_h */
