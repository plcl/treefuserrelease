%{
    #include "astnode.h"
    #include "scanner.h"
%}

KEY_PROGRAM  "PROGRAM"
KEY_BEGIN    "BEGIN"
KEY_END      "END"
KEY_IF       "IF"
KEY_ELSE     "ELSE"
KEY_FI       "FI"
KEY_WHILE    "WHILE"
KEY_ELIHE    "ELIHW"
KEY_INT      "INT"

OP_ASSIGN  ":="
OP_ADD     "+"
OP_SUB     "-"
OP_MUL     "*"
OP_DIV     "/"
OP_EQ      "="
OP_NEQ     "!="
OP_LE      "<"
OP_GR      ">"
OP_LEQ     "<="
OP_GEQ     ">="
OP_LPAREN  "("
OP_RPAREN  ")"
OP_SEMICOL ";"
OP_COMMA   ","

DIGIT			[0-9]
LETTER			[A-Za-z]
COMMENT			--.*\n
INTLITERAL		{DIGIT}+
IDENTIFIER 		{LETTER}({LETTER}|{DIGIT})*
WHITESPACE      [ \n\t\r\n]+

%%
{KEY_PROGRAM}  { return TOKEN_KEY_PROGRAM  ; }
{KEY_BEGIN}    { return TOKEN_KEY_BEGIN    ; }
{KEY_END}      { return TOKEN_KEY_END      ; }
{KEY_IF}       { return TOKEN_KEY_IF       ; }
{KEY_ELSE}     { return TOKEN_KEY_ELSE     ; }
{KEY_FI}       { return TOKEN_KEY_FI       ; }
{KEY_WHILE}    { return TOKEN_KEY_WHILE    ; }
{KEY_ELIHE}    { return TOKEN_KEY_ELIHW    ; }
{KEY_INT}      { return TOKEN_KEY_INT      ; }

{OP_ASSIGN}  { return TOKEN_OP_ASSIGN  ; }
{OP_ADD}     { return TOKEN_OP_ADD     ; }
{OP_SUB}     { return TOKEN_OP_SUB     ; }
{OP_MUL}     { return TOKEN_OP_MUL     ; }
{OP_DIV}     { return TOKEN_OP_DIV     ; }
{OP_EQ}      { return TOKEN_OP_EQ      ; }
{OP_NEQ}     { return TOKEN_OP_NEQ     ; }
{OP_LE}      { return TOKEN_OP_LE      ; }
{OP_GR}      { return TOKEN_OP_GR      ; }
{OP_LEQ}     { return TOKEN_OP_LEQ     ; }
{OP_GEQ}     { return TOKEN_OP_GEQ     ; }
{OP_LPAREN}  { return TOKEN_OP_LPAREN  ; }
{OP_RPAREN}  { return TOKEN_OP_RPAREN  ; }
{OP_SEMICOL} { return TOKEN_OP_SEMICOL ; }
{OP_COMMA}   { return TOKEN_OP_COMMA   ; }

{IDENTIFIER}                         { return TOKEN_IDENTIFIER    ; }
{INTLITERAL}                         { return TOKEN_INTLITERAL    ; }
{COMMENT}                            { ; }
{WHITESPACE}                         { ; }

. 									 { return TOKEN_ILLEGAL;}
%%
int yywrap()
{
    return 1;
}
