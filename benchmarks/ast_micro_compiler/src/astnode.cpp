#include "astnode.h"



std::string nodeTypesToStr[]={"","ASSIGN", "BIN", "VARREF", "IF","CONSTANT", "SEQ", "WHILE","FUNCDEF","VARDEF"};
int counterAll=0;
bool doOptimizeAgain=true;
bool isFirtIteration=true;


bool typeError;
bool consistencyError;
bool multiDef_v;
bool multiDef_f;
bool undefinedRef;

int counter=0;
int counterFused=0;
int counterFusedAll=0;

map < string , map<string, int> > symbolTable_simple;
string curr_function;
int def_counter;

AstNodeBase::AstNodeBase(int nodeType_, int valueType_, int binOperator_, int id_,  string idName_,int intValue_, int boolValue_){
    nodeType=nodeType_;
    valueType=valueType_;
    binOperator=binOperator_;
    valueComputed=false;
    id=id_;
    idName=idName_;
    intValue=intValue_;
    boolValue=boolValue_;
    counter=0;
    valueComputed=false;
    
    this->whileCond=NULL ;
    this->whileBody=NULL;
    this->ifCond=NULL ;
    this->ifThenBody=NULL;
    this->ifElseBody=NULL;
    this->assignId=NULL ;
    this-> assignExpr=NULL;
    this->binLeft=NULL ;
    this->binRight=NULL;
    this->seqLeft=NULL ;
    this->seqRight=NULL;
    this->funBody=NULL ;
    this->nextFuncDef=NULL;
    
}


AstNodeBase::AstNodeBase(int nodeType_, int valueType_, int binOperator_, int id_,  string idName_,int intValue_, int boolValue_,string funcName_){
    nodeType=nodeType_;
    valueType=valueType_;
    binOperator=binOperator_;
    valueComputed=false;
    id=id_;
    idName=idName_;
    intValue=intValue_;
    boolValue=boolValue_;
    funcName =funcName_;
    counter=0;
    valueComputed=false;
    
    
    this->whileCond=NULL ;
    this->whileBody=NULL;
    this->ifCond=NULL ;
    this->ifThenBody=NULL;
    this->ifElseBody=NULL;
    this->assignId=NULL ;
    this-> assignExpr=NULL;
    this->binLeft=NULL ;
    this->binRight=NULL;
    this->seqLeft=NULL ;
    this->seqRight=NULL;
    this->funBody=NULL ;
    this->nextFuncDef=NULL;
    
    
    
}



AstNodeBase * createSeqNode(){
    return new AstNodeBase(SEQ, -1, -1, -1 ,"", -1, -1);
}

AstNodeBase * createIFNode(){
    return new AstNodeBase(IF, -1, -1, -1, "", -1,-1);
}

AstNodeBase * createAssignNode(){
    return new AstNodeBase(ASSIGN,-1, -1, -1, "", -1,-1);
}

AstNodeBase * createBinNode(int binOperator){
    return new AstNodeBase(BIN, -1, binOperator,-1, "", -1,-1);
}

AstNodeBase * createConstNode(int val){
    return new AstNodeBase(CONSTANT, INT_TYPE, -1, -1, "", val, false);
}

AstNodeBase * createConstBoolNode(bool val){
    return new AstNodeBase(CONSTANT, BOOL_TYPE, -1, -1, "", val, false);
}

AstNodeBase * createVarRefNode(int id, string name , int type){
    return new AstNodeBase(VARREF, type, -1, id, name,-1, -1);
}

AstNodeBase * createWhileNode(){
    return new AstNodeBase(WHILE,-1, -1, -1, "", -1, -1);
}

AstNodeBase * createFunDefNode(string funName){
    return new AstNodeBase(FUNCDEF,-1, -1, -1, "", -1, -1,funName);
}

AstNodeBase * createVarDefNode(int id , string name, int type){
    return new AstNodeBase(VARDEF, type, -1, id, name,-1, -1);
}
__attribute__((annotate("strict_access(8,'r','global')"))) void increaseCount(AstNodeBase * n){
    if(n!=NULL)
        counter++;
    counterAll++;
    
}
