#include "compiler.h"
#include "parser.h"
#include <iostream>
#include <stdbool.h>
#include <assert.h>
#include <unistd.h>
#include <chrono>
#include <stdbool.h>
using namespace std;
#define __USE_C99_MATH
#define _Bool bool
//#define PERF
//#define ONLY_AST_BUILDER


#ifndef ONLY_AST_BUILDER

#include "integer_propagation.h"
#include "check_set_types.h"
#include "constant_folding.h"
#include "consistency_check.h"
#include "available_expr.h"
#include "func_names_check.h"
#include "var_def_checks.h"
int countNodes(AstNodeBase *  n){
    if(n==NULL)
        return 0;
    
    int sum=1;
    sum += countNodes(n->assignId);
    sum += countNodes(n->assignExpr);
    
    sum += countNodes(n->ifCond);
    sum += countNodes(n->ifThenBody);
    sum += countNodes(n->ifElseBody);
    
    sum += countNodes(n->whileCond);
    sum += countNodes(n->whileBody);
    
    sum += countNodes(n->binLeft);
    sum += countNodes(n->binRight);
    
    sum += countNodes(n->seqLeft);
    sum += countNodes(n->seqRight);
    
    sum += countNodes(n->funBody);
    sum += countNodes(n->nextFuncDef);
    return sum;
}

#endif


#ifdef PERF

#include <papi.h>
#define NUM 8
string instance("Original");
int ret;
int events[] = {PAPI_TOT_CYC, PAPI_TOT_INS, PAPI_L2_DCA, PAPI_L2_DCM, PAPI_L2_ICA, PAPI_L2_ICM, PAPI_L3_TCA, PAPI_L3_TCM};
string defs[] = {"Cycle Count", "Instruction Count", "L2 Data Access Count", "L2 Data Miss Count", "Instruction Access Count", "L2 Instruction Miss Count", "L3 Total Access Count", "L3 Total Miss Count"};
long long values[NUM];
long long rcyc0, rcyc1, rusec0, rusec1;
long long vcyc0, vcyc1, vusec0, vusec1;

void init_papi(){
    ret = PAPI_library_init(PAPI_VER_CURRENT);
    if (ret != PAPI_VER_CURRENT){
        cerr << "PAPI Init Error" << endl;
        exit(1);
    }
    for(int i=0; i<NUM; ++i){
        ret = PAPI_query_event(events[i]);
        if(ret != PAPI_OK){
            cerr << "PAPI Event " << i << " does not exist" << endl;
            // handleError(ret);
        }
    }
    
}
void start_counters( ) {
    //Performance Counters Start
    rcyc0  = PAPI_get_real_cyc();
    rusec0 = PAPI_get_real_usec();
    
    vcyc0  = PAPI_get_virt_cyc();
    vusec0 = PAPI_get_virt_usec();
    
    ret = PAPI_start_counters(events, NUM);
    if (ret != PAPI_OK){
        cerr << "PAPI Error starting counters" << endl;
        
    }
}
void read_counters( ) {
    //Performance Counters Read
    rcyc1  = PAPI_get_real_cyc();
    rusec1 = PAPI_get_real_usec();
    
    vcyc1  = PAPI_get_virt_cyc();
    vusec1 = PAPI_get_virt_usec();
    
    ret = PAPI_stop_counters(values, NUM);
    if (ret != PAPI_OK){
        if ( ret == PAPI_ESYS) {
            cout<< "error inside PAPI call" << endl;
        }else if (ret == PAPI_EINVAL){
            cout<< "error with arguments" <<endl;
        }
        
        cerr << "PAPI Error reading counters" << endl;
        //  handleError(ret);
        
    }
}
#endif

AstNodeBase * root;
int numNodes;
int treeSize;
auto t1 = std::chrono::high_resolution_clock::now();
auto t2 = std::chrono::high_resolution_clock::now();

void print_counters(string test_name){
    cout << test_name;
    cout << "," << numNodes;
    cout << "," << treeSize;
#ifdef PERF
    
    
    for(int i=0; i<NUM; ++i){
        cout << "," << values[i] ;
    }
    int wall_clock_time = (rusec1-rusec0)/1000.0 ;
    cout << "," << wall_clock_time;
#else
    double time_spent  = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count();
    cout << "," << time_spent;
#endif
    cout<<endl;
    
}
void run_ast_opt_passses(AstNodeBase * n){
    
#ifdef PERF
    start_counters( );
#endif
    
    /* t1 = std::chrono::high_resolution_clock::now();
     astConsistencyCheck(n);
     checkFunctionNames(n);
     checkVarDefs_AssignIds(n);
     checkAndSetTypes(n);     
     t2 = std::chrono::high_resolution_clock::now();
    */

#ifdef PERF
    read_counters( ) ;
#endif  
    if( (consistencyError || typeError || multiDef_f || multiDef_v || undefinedRef  )){
        cout<< "error in the AST or input program" << endl;
        cout<< consistencyError<<","<<typeError<<","<<multiDef_v<<","<<multiDef_v<<","<<undefinedRef<<endl;
    }else{
#ifdef PERF
        start_counters( );
#endif
        t1 = std::chrono::high_resolution_clock::now();
        constantFolding(n);
        integerConstantPropagation(n,true);
        findIdsUsedByExprsAndBuildExprAsStr(n);
        findExpsKillSets(n);
        findExpsGenSets(n);
        findPostAvailableExpr(n);        
        t2 = std::chrono::high_resolution_clock::now();
        
#ifdef PERF
        read_counters( ) ;
#endif
        print_counters("[#nodes, tree size, runtime]");
        
    }
}

void printAst(AstNodeBase *  n){
    if(n==NULL)
        return;
    cout<<"node type is :"<< nodeTypesToStr[n->nodeType]<<n->nodeType<<endl;
    printAst(n->assignId);
    printAst(n->assignExpr);
    
    printAst(n->ifCond);
    printAst(n->ifThenBody);
    printAst(n->ifElseBody);
    
    printAst(n->whileCond);
    printAst(n->whileBody);
    
    printAst(n->binLeft);
    printAst(n->binRight);
    
    printAst(n->seqLeft);
    printAst(n->seqRight);
    
    printAst(n->funBody);
    printAst(n->nextFuncDef);
    
}

int main(int argc, char **argv){
    cout<<"compiler just started"<<endl ;
    cout<<"size of one node is:" <<  sizeof(AstNodeBase) << endl;
    yyin = fopen(argv[1], "r");
    
    int parse = yyparse();
    
    if(parse)
        cout << "Not accepted" << endl;
    else
        cout << "Accepted" << endl;
    
#ifdef PERF
    init_papi();
#endif
    
#ifndef ONLY_AST_BUILDER
    AstNodeBase * n=root;
    numNodes = countNodes(root);
    treeSize= numNodes*sizeof(AstNodeBase) ;
   
    run_ast_opt_passses(n);
    
    
    
#endif
    return 0;
}


