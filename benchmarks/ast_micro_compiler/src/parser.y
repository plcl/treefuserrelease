%{
    #include <string.h>
    #include <stdio.h>
    #include <map>
    #include <vector>
    #include <utility>
    #include <sstream>
    #include "parser.h"
    #include "astnode.h"
    #define YYMAXDEPTH 100000
    #define YYINITDEPTH 1000000
    
    vector<string> idList;
    map<string, int> symboltable;
    extern AstNodeBase* root;
    using namespace std;
    int varcount = 0;
%}
%start program

%union {char* sval; int ival; AstNodeBase* astval;}

%token TOKEN_KEY_PROGRAM
%token TOKEN_KEY_BEGIN
%token TOKEN_KEY_END
%token TOKEN_KEY_IF
%token TOKEN_KEY_ELSE
%token TOKEN_KEY_FI
%token TOKEN_KEY_WHILE
%token TOKEN_KEY_ELIHW
%token TOKEN_KEY_INT

%token TOKEN_OP_ASSIGN
%token TOKEN_OP_ADD
%token TOKEN_OP_SUB
%token TOKEN_OP_MUL
%token TOKEN_OP_DIV
%token TOKEN_OP_EQ
%token TOKEN_OP_NEQ
%token TOKEN_OP_LE
%token TOKEN_OP_GR
%token TOKEN_OP_LEQ
%token TOKEN_OP_GEQ
%token TOKEN_OP_LPAREN
%token TOKEN_OP_RPAREN
%token TOKEN_OP_SEMICOL
%token TOKEN_OP_COMMA

%token TOKEN_IDENTIFIER
%token TOKEN_INTLITERAL

%token TOKEN_ILLEGAL

%type <sval> id
%type <ival> mulop addop
%type <astval> stmt_list stmt base_stmt if_stmt while_stmt assign_stmt assign_expr expr expr_prefix factor factor_prefix postfix_expr
%%
/* Program */
program           :  fun_defs {root=$<astval>1;}
;
fun_defs          : fun_def{symboltable.clear();}fun_defs
{   $<astval>$= $<astval>1;
    $<astval>$->nextFuncDef = $<astval>3;
}
| fun_def
{ $<astval>$= $<astval>1;
    $<astval>$->nextFuncDef=NULL;
}
;

fun_def           : TOKEN_KEY_BEGIN id  fun_body  TOKEN_KEY_END
{
    
    $<astval>$= createFunDefNode ($<sval>2);
    
    $<astval>$->funBody=$<astval>3;
    
}
;
id                : TOKEN_IDENTIFIER{$<sval>$ = strdup(yytext);}
;
fun_body          :  stmt_list
{
    $<astval>$ = $<astval>1;
}
;
//decl		      : var_decl decl
//                  | empty
//				  ;

/* Variable Declaration */
var_decl          : var_type id_list TOKEN_OP_SEMICOL
{
    for(int i = idList.size()-1; i >= 0; i--){
        symboltable[idList[i]] = varcount++;
        
    }
    
    AstNodeBase * tmp =createSeqNode();
    $<astval>$= tmp;
    for(int i = idList.size()-1; i >= 0; i--){
        tmp->seqLeft = createVarDefNode(symboltable[idList[i]],idList[i],1 );
        if(i != 0){
            tmp->seqRight   = createSeqNode();
            tmp = tmp->seqRight;
        }
    }
    
    
}
;
var_type	      : TOKEN_KEY_INT
;
id_list           : id{idList.clear();} id_tail{idList.push_back($<sval>1); }
;
id_tail           : TOKEN_OP_COMMA id id_tail{idList.push_back($<sval>2);}
| empty
;

/* Statement List */
stmt_list         : stmt stmt_list{$<astval>$ = createSeqNode(); $<astval>$->seqLeft = $<astval>1; $<astval>$->seqRight = $<astval>2; }
| empty{$<astval>$ = createSeqNode();}
;
stmt              : base_stmt{$<astval>$ = $<astval>1;}
| if_stmt{$<astval>$ = $<astval>1; }
| while_stmt{$<astval>$ = $<astval>1;}
;
base_stmt         : assign_stmt{$<astval>$ = $<astval>1;}
| var_decl {$<astval>$ = $<astval>1;}
;

/* Basic Statements */
assign_stmt       : assign_expr TOKEN_OP_SEMICOL{$<astval>$ = $<astval>1;}
;

assign_expr       : id TOKEN_OP_ASSIGN expr{$<astval>$ = createAssignNode(); $<astval>$->assignId = createVarRefNode(symboltable[$<sval>1], $<sval>1, INT_TYPE); $<astval>$->assignExpr = $<astval>3; }
;

/* Expressions */
expr              : expr_prefix factor
{
    AstNodeBase* left;
    if($<astval>1 != NULL){
        left = $<astval>1;
        left->binRight = $<astval>2;
    }
    else{
        left = $<astval>2;
    }
    $<astval>$ = left;
}
;
expr_prefix       : expr_prefix factor addop
{
    AstNodeBase* left;
    if($<astval>1 != NULL){
        left=$<astval>1;
        left->binRight = $<astval>2;
    }
    else{
        left=$<astval>2;
    }
    $<astval>$ = createBinNode($<ival>3);
    $<astval>$->binLeft = left;
}
| empty{$<astval>$ = NULL;}
;
factor            : factor_prefix postfix_expr
{
    AstNodeBase* left;
    if($<astval>1 != NULL){
        left = $<astval>1;
        left->binRight = $<astval>2;
    }
    else{
        left = $<astval>2;
    }
    $<astval>$ = left;
}
;
factor_prefix     : factor_prefix postfix_expr mulop
{
    AstNodeBase* left;
    if($<astval>1 != NULL){
        left=$<astval>1;
        left->binRight = $<astval>2;
    }
    else{
        left=$<astval>2;
    }
    $<astval>$ = createBinNode($<ival>3);
    $<astval>$->binLeft = left;
}
| empty{$<astval>$ = NULL;}
;
postfix_expr      : TOKEN_OP_LPAREN expr TOKEN_OP_RPAREN{$<astval>$ = $<astval>2;}
| id{$<astval>$ = createVarRefNode(symboltable[$<sval>1], $<sval>1, INT_TYPE);}
| TOKEN_INTLITERAL{$<astval>$ = createConstNode(atoi(strdup(yytext)));}
;
addop             : TOKEN_OP_ADD{$<ival>$ = ADD;}
| TOKEN_OP_SUB{$<ival>$ = SUB;}
;
mulop             : TOKEN_OP_MUL{$<ival>$ = MUL;}
| TOKEN_OP_DIV{$<ival>$ = DIV;}
;

/* Complex Statements and Condition */
if_stmt           : TOKEN_KEY_IF TOKEN_OP_LPAREN cond TOKEN_OP_RPAREN stmt_list else_part TOKEN_KEY_FI
{$<astval>$ = createIFNode(); $<astval>$->ifCond = $<astval>3; $<astval>$->ifThenBody = $<astval>5; $<astval>$->ifElseBody = $<astval>6;}
;
else_part         : TOKEN_KEY_ELSE stmt_list{$<astval>$ = $<astval>2;}
| empty{$<astval>$ = createSeqNode();}
cond              : expr compop expr{$<astval>$ = createBinNode($<ival>2); $<astval>$->binLeft = $<astval>1; $<astval>$->binRight = $<astval>3;}
compop            : TOKEN_OP_GR  {$<ival>$ = GR;}
| TOKEN_OP_LE  {$<ival>$ = LE;}
| TOKEN_OP_EQ  {$<ival>$ = EQ;}
| TOKEN_OP_NEQ {$<ival>$ = NEQ;}
| TOKEN_OP_LEQ {$<ival>$ = LEQ;}
| TOKEN_OP_GEQ {$<ival>$ = GEQ;}
;
/* While statements */
while_stmt        : TOKEN_KEY_WHILE TOKEN_OP_LPAREN cond TOKEN_OP_RPAREN stmt_list TOKEN_KEY_ELIHW
{$<astval>$ = createWhileNode(); $<astval>$->whileCond = $<astval>3; $<astval>$->whileBody = $<astval>5;}
;
/* Empty  Token*/
empty 			  :
;
%%

void yyerror(char* s)
{
    printf( "error: %s\n", s);
}