//
//  integerPropagation2.h
//  libtooling
//
//  Created by lsakka on 6/27/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#ifndef integerPropagation2_h
#define integerPropagation2_h

#include <map>
#include "astnode.h"

void AstNodeBase::addToConstantMap_R5(int id_ , int val){
    idToConstVal[id_]=val;
}

void AstNodeBase::clearConstantMap_R5(){
    idToConstVal.clear();
}

bool AstNodeBase::isInConstanMap_R5( int id_ ){
    return (idToConstVal.find(id_)!=idToConstVal.end());
}

int AstNodeBase::readFromConstantMap_R5( int id_ ){
    
    //assert(idToValStoreInt[n].find(id_)!=idToValStoreInt[n].end());
    return idToConstVal[id_];
}

void AstNodeBase::setConstantMapToIntersectionOf_R5( AstNodeBase * nsrc1,AstNodeBase * nsrc2 ){
    
    idToConstVal.clear();
    
    for (map<int,int>::iterator it= nsrc1->idToConstVal.begin(); it!= nsrc1->idToConstVal.end(); it++){
        if(nsrc2->isInConstanMap_R5(it->first) ){
            if(nsrc1->idToConstVal[it->first]== nsrc2->idToConstVal[it->first]){
                idToConstVal[it->first]=it->second;
            }
        }
    }
}

void AstNodeBase::copyConstantMapTo_R5(AstNodeBase * ndest){
    
    ndest->idToConstVal.clear();
    
    for (map<int,int>::iterator it= idToConstVal.begin(); it!= idToConstVal.end(); it++){
        
        ndest->idToConstVal[it->first]=it->second;
        
    }
    
}

TF_Traversal void integerConstantPropagation(AstNodeBase * n,bool doReplacement){
    COUNT_FUSED_inner
    if(true){
        increaseCount( n);
        //base cases
        
        
        if( n==NULL || consistencyError||typeError||multiDef_v
           ||multiDef_f ||undefinedRef )
            return ;
        
    }
    //VARDEF
    /** nothing to do */
    
    //FUNCDEF
    integerConstantPropagation(n->funBody, true);
    integerConstantPropagation(n->nextFuncDef, true);
    
    //CONST
    if( n->nodeType==CONSTANT)
        return;
    
    //ID
    if(n->nodeType==VARREF && doReplacement){
        if(n->isInConstanMap_R5(n->id) ){
            n->nodeType=CONSTANT;
            n->intValue=n->readFromConstantMap_R5(n->id);
            n->valueComputed=true;
        }
        return;
    }
    
    //ASSIGN
    if(n->nodeType==ASSIGN){
        n->copyConstantMapTo_R5(n->assignExpr);
        n->assignExpr->writeConstantMap_R5();
    }
    
    integerConstantPropagation(n->assignExpr,true);
    
    if(n->nodeType==ASSIGN ){
        if(n->assignExpr->nodeType== CONSTANT ){
            // OUT = IN+GEN-KILL
            if(n->assignExpr->valueType==INT_TYPE){
                n->addToConstantMap_R5( n->assignId->id, n->assignExpr->intValue);
            }
        }
    }
    
    //SEQ
    if(n->nodeType==SEQ){
        if(n->seqLeft==NULL && n->seqRight==NULL)
            return;
    }
    if(n->nodeType==SEQ){
        if(n->seqLeft!=NULL){
            n->copyConstantMapTo_R5(n->seqLeft);
            n->seqLeft->writeConstantMap_R5();
        }
    }
    
    integerConstantPropagation(n->seqLeft,true);
    
    if(n->nodeType==SEQ){
        if(n->seqRight!=NULL){
            n->seqLeft->copyConstantMapTo_R5( n->seqRight);
            n->seqRight->writeConstantMap_R5();
        }else{
            n->seqLeft->copyConstantMapTo_R5( n);
            n->writeConstantMap_R5();
            return;
        }
    }
    integerConstantPropagation(n->seqRight,true);
    
    
    //IF
    if(n->nodeType==IF){
        n->copyConstantMapTo_R5(n->ifCond);
        n->copyConstantMapTo_R5(n->ifThenBody);
        n->copyConstantMapTo_R5(n->ifElseBody);
        n->ifCond->writeConstantMap_R5();
        n->ifThenBody->writeConstantMap_R5();
        n->ifElseBody->writeConstantMap_R5();
        
    }
    integerConstantPropagation(n->ifCond,true);
    integerConstantPropagation(n->ifThenBody,true);
    integerConstantPropagation(n->ifElseBody,true);
    
    if(n->nodeType==IF){
        n->setConstantMapToIntersectionOf_R5(n->ifThenBody, n->ifElseBody);
        n->ifThenBody->readConstantMap_R5();
        n->ifElseBody->readConstantMap_R5();
    }
    
    //BIN
    
    if(n->nodeType==BIN){
        n->copyConstantMapTo_R5(n->binLeft);
        n->copyConstantMapTo_R5(n->binRight);
        n->binLeft->writeConstantMap_R5();
        n->binRight->writeConstantMap_R5();
        
    }
    integerConstantPropagation(n->binLeft,true);
    integerConstantPropagation(n->binRight,true);
    
    //WHILE
    
    integerConstantPropagation(n->whileBody,false);
    
    if(n->nodeType==WHILE){
        n->whileCond->setConstantMapToIntersectionOf_R5(n, n->whileBody);
        n->whileBody->readConstantMap_R5();
        n->readConstantMap_R5();
        
        //copy the intersection to the root node and to the while body also
        n->whileCond->copyConstantMapTo_R5(n);
        n->writeConstantMap_R5();
        
        n->whileCond->copyConstantMapTo_R5(n->whileBody);
        n->whileBody->writeConstantMap_R5();
        
    }
    
    integerConstantPropagation(n->whileBody,true);
    integerConstantPropagation(n->whileCond,true);
    return;
    
}

#endif /* integerPropagation2_h */
