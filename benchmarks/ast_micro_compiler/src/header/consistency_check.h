//
//  syntaxCheck2.h
//  libtooling
//
//  Created by lsakka on 6/27/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#ifndef syntaxCheck2_h
#define syntaxCheck2_h


#include "astnode.h"
using  namespace std;

TF_Traversal void astConsistencyCheck(AstNodeBase * n){
    COUNT_FUSED_inner
    if(true){
    increaseCount(n);
    if( n==NULL || IS_FIRST_IT)
        return ;
    }
    //CONST
    
    //Variable Reference 
    
 
    //VARDEF

    
    //FUNCDEF
    
    if(n->nodeType==FUNCDEF ){
        if(     n->funBody == NULL ){
            consistencyError=true;
            return;
        }
        if(  n->nextFuncDef!= NULL &&  n->nextFuncDef->nodeType!=FUNCDEF){
            consistencyError=true;
            return;
        }
    }
    
    
    astConsistencyCheck(n->funBody);
    astConsistencyCheck(n->nextFuncDef);
    
    if(n->nodeType==ASSIGN ){
        if(     n->assignId==NULL ||  n->assignExpr==NULL){
            consistencyError=true;

            return;
        }
        if(  n->assignId->nodeType!=VARREF ||(n->assignExpr->nodeType!= VARREF && n->assignExpr->nodeType!= CONSTANT &&  n->assignExpr->nodeType!= BIN)){

            consistencyError=true;
            return;
        }
    }
    astConsistencyCheck(n->assignExpr);
    
    //SEQ
    if(n->nodeType==SEQ){
        if(n->seqLeft==NULL && n->seqRight!=NULL){

            consistencyError= true;
            return;
        }
        if(n->seqLeft!=NULL && n->seqLeft->nodeType!= VARDEF && n->seqLeft->nodeType!= WHILE && n->seqLeft->nodeType!= SEQ &&  n->seqLeft->nodeType!= IF && n->seqLeft->nodeType!= ASSIGN){

            consistencyError=true;
            return;
        }
        
        if(n->seqRight!=NULL &&  n->seqLeft->nodeType!= VARDEF && n->seqRight->nodeType!= WHILE && n->seqRight->nodeType!= SEQ &&  n->seqRight->nodeType!= IF && n->seqRight->nodeType!= ASSIGN){

            consistencyError=true;
            return;
        }
        
    }
    astConsistencyCheck(n->seqLeft);
    astConsistencyCheck(n->seqRight);
    
    //IF
    
    if(n->nodeType==IF){
        if(n->ifCond==NULL ||n->ifThenBody==NULL|| n->ifElseBody==NULL){

            consistencyError= true;
            return;
        }
        if(n->ifCond->nodeType!= VARREF && n->ifCond->nodeType!= CONSTANT &&  n->ifCond->nodeType!= BIN){

            consistencyError=true;
            return;
        }
        if(n->ifThenBody->nodeType!= WHILE && n->ifThenBody->nodeType!= SEQ &&  n->ifThenBody->nodeType!= IF && n->ifThenBody->nodeType!= ASSIGN){

            consistencyError=true;
            return;
        }
        if(n->ifElseBody->nodeType!= WHILE && n->ifElseBody->nodeType!= SEQ &&  n->ifElseBody->nodeType!= IF && n->ifElseBody->nodeType!= ASSIGN){

            consistencyError=true;
            return;
        }
        
    }
    astConsistencyCheck(n->ifCond);
    astConsistencyCheck(n->ifThenBody);
    astConsistencyCheck(n->ifElseBody);
    
    
    //BIN
    if(n->nodeType==BIN){
        if(n->binLeft==NULL ||n->binRight==NULL){
            consistencyError= true;
            return;
        }
        if(n->binLeft->nodeType!= VARREF && n->binLeft->nodeType!= CONSTANT &&  n->binLeft->nodeType!= BIN){
            consistencyError=true;
            return;
        }
        if(n->binRight->nodeType!= VARREF && n->binRight->nodeType!= CONSTANT &&  n->binRight->nodeType!= BIN){
            consistencyError=true;
            return;
        }
        
    }
    astConsistencyCheck(n->binLeft);
    astConsistencyCheck(n->binRight);
    
    
    //WHILE
    if(n->nodeType==WHILE){
        if(n->whileCond==NULL ||n->whileBody==NULL){
            consistencyError= true;
            return;
        }
        
        if(n->whileCond->nodeType!= VARREF && n->whileCond->nodeType!= CONSTANT &&
           n->whileCond->nodeType!= BIN){
            consistencyError=true;
            return;
        }
        if(n->whileBody->nodeType!= WHILE && n->whileBody->nodeType!= SEQ &&  n->whileBody->nodeType!= IF && n->whileBody->nodeType!= ASSIGN){
            consistencyError=true;
            return;
        }
    }
    astConsistencyCheck(n->whileCond);
    astConsistencyCheck(n->whileBody);
    
    
}



#endif /* syntaxCheck2_h */
