//
//  checkFunNames.h
//  libtooling
//
//  Created by lsakka on 3/14/17.
//  Copyright © 2017 lsakka. All rights reserved.
//

#ifndef checkFunNames_h
#define checkFunNames_h



#include "astnode.h"
#include <set>
#include <stdio.h>

using  namespace std;


TF_AbsrtactedAccess((7,'r','global'))
bool function_name_exist( AstNodeBase * funDef){
   
    if(symbolTable_simple.find(funDef->funcName)!= symbolTable_simple.end()){
        return true;
    }
    else{
        return false;
    }
}

TF_AbsrtactedAccess((7,'w','global'))
void add_function_to_symboltable( AstNodeBase * funDef){

    symbolTable_simple[funDef->funcName] = map<string, int>();
   
}

TF_Traversal void checkFunctionNames(AstNodeBase * n){
    COUNT_FUSED_inner
    if(true){
        increaseCount(n);
        if( n==NULL || IS_FIRST_IT)
            return ;
    }
    
    if(n->nodeType != FUNCDEF ){
        return;
    }
    
    if (function_name_exist(n) == true){
        multiDef_f = true;
        return;
    }
    add_function_to_symboltable(n);
    checkFunctionNames(n->nextFuncDef);
    
}


#endif /* checkFunNames_h */
