//
//  availableExpr.h
//  libtooling
//
//  Created by lsakka on 6/27/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#ifndef availableExpr_h
#define availableExpr_h


#include<map>
#include<set>
#include<string>
#include<sstream>
#include "astnode.h"

string _to_string(int in_){
    stringstream ss;
    ss  >> in_;
    return ss.str();
}
string _to_string(bool in_){
    stringstream ss;
    ss  >> in_;
    return ss.str();
}
/******************************findIdsUsedByExprsAndBuildExprAsStr******************************/

void AstNodeBase::addUsedIdsTo_R2( AstNodeBase * dst){
    for(set<int>::iterator it=this->idsUsedInExp.begin();it!=this->idsUsedInExp.end(); it++){
        dst->idsUsedInExp.insert(*it);
        
    }
    
}

void AstNodeBase::buildStringExprConst_R2(int value_type ,int intval, bool boolVal){
    
    if(value_type==BOOL_TYPE)
        this->exprAsStr=_to_string(boolVal);
    
    if(value_type==INT_TYPE)
        
        this->exprAsStr=_to_string(intval);
    
}

void AstNodeBase::buildStringExprID_R2(int id_ ){
    
    this->exprAsStr="id_"+_to_string(id_)+"_";
    
}

void AstNodeBase::buildStringExprBIN_R2(int binaryOperator ){
    this->exprAsStr=binLeft->exprAsStr+"["+_to_string(binaryOperator)+"]"+binRight->exprAsStr;
}

TF_Traversal void findIdsUsedByExprsAndBuildExprAsStr(AstNodeBase * n ){
    
    COUNT_FUSED_inner
    
    if(true){
        increaseCount(n);
        if(n==NULL || IS_DO_OPT_AGAIN||  consistencyError||typeError||multiDef_v||multiDef_f ||undefinedRef)
            return;
    }
    //CONST
    if(n->nodeType==CONSTANT){
        n->buildStringExprConst_R2(n->valueType,n->intValue,n->boolValue);
    }
    
    //ID
    if(n->nodeType==VARREF){
        n->addIdToUSedIds_R2(n->id);
        n->buildStringExprID_R2(n->id);
    }
    
    //VARDEF
    /** nothing to do */
    
    //FUNCDEF
    findIdsUsedByExprsAndBuildExprAsStr(n->funBody);
    findIdsUsedByExprsAndBuildExprAsStr(n->nextFuncDef);


    //ASSIGN
    findIdsUsedByExprsAndBuildExprAsStr(n->assignExpr);
    
    //SEQ
    findIdsUsedByExprsAndBuildExprAsStr(n->seqLeft);
    findIdsUsedByExprsAndBuildExprAsStr(n->seqRight);
    
    
    //IF
    findIdsUsedByExprsAndBuildExprAsStr(n->ifCond);
    findIdsUsedByExprsAndBuildExprAsStr(n->ifThenBody);
    findIdsUsedByExprsAndBuildExprAsStr(n->ifElseBody);
    
    //BIN
    findIdsUsedByExprsAndBuildExprAsStr(n->binLeft);
    findIdsUsedByExprsAndBuildExprAsStr(n->binRight);
    
    if(n->nodeType==BIN){
        n->binLeft->addUsedIdsTo_R2(n);
        n->writeUsedIds_R2();
    }
    
    if(n->nodeType==BIN){
        n->binRight->addUsedIdsTo_R2(n);
        n->writeUsedIds_R2();
    }
    
    if(n->nodeType==BIN){
        n->buildStringExprBIN_R2(n->binOperator);
        n->binLeft->readUsedIds_R2();
        n->binRight->readUsedIds_R2();
    }
    
    //WHILE
    findIdsUsedByExprsAndBuildExprAsStr(n->whileCond);
    findIdsUsedByExprsAndBuildExprAsStr(n->whileBody);
    
}

/********************************findExpsKillSets*****************************/

void AstNodeBase::addToKillSet_R3(int id_){
    
    this->killSet.insert(id_);
}

void AstNodeBase::appendKillSetToKillSetOfNode_R3( AstNodeBase * dst){
    
    
    for( set<int>::iterator it=this->killSet.begin() ; it!=this->killSet.end(); it++){
        dst->killSet.insert(*it);
    }
}

//kill sets only built for stmt nodes (If /SEQ/ASSIGN/ and WHILE) not for expr nodes(ID/CONST/BIN)
TF_Traversal void findExpsKillSets(AstNodeBase * n){
    COUNT_FUSED_inner
    
    if(true){
        increaseCount(n);
        if(n==NULL || IS_DO_OPT_AGAIN|| consistencyError||typeError||multiDef_v||multiDef_f ||undefinedRef)
            return;
    }
    
    //VARDEF
    /** nothing to do */
    
    //FUNCDEF
    findExpsKillSets(n->funBody);
    findExpsKillSets(n->nextFuncDef);

    //ASSIGN
    if(n->nodeType==ASSIGN){
        n->addToKillSet_R3(n->assignId->id);
    }
    
    //SEQ
    findExpsKillSets(n->seqLeft);
    findExpsKillSets(n->seqRight);
    if(n->nodeType==SEQ){
        if(n->seqLeft!=NULL){
            n->seqLeft->appendKillSetToKillSetOfNode_R3(n);
            n->writeKillSet_R3();
        }
    }
    if(n->nodeType==SEQ){
        if(n->seqRight!=NULL){
            n->seqRight->appendKillSetToKillSetOfNode_R3(n);
            n->writeKillSet_R3();
        }
    }
    //IF
    findExpsKillSets(n->ifElseBody);
    findExpsKillSets(n->ifThenBody);
    
    if(n->nodeType==IF){
        n->ifThenBody->appendKillSetToKillSetOfNode_R3(n);
        n->writeKillSet_R3();
    }
    
    if(n->nodeType==IF){
        n->ifElseBody->appendKillSetToKillSetOfNode_R3(n);
        n->writeKillSet_R3();
    }
    
    
    //WHILE
    findExpsKillSets(n->whileBody);
    if(n->nodeType==WHILE){
        n->whileBody->appendKillSetToKillSetOfNode_R3(n);
        n->writeKillSet_R3();
        
    }
    //no kill sets need to be propagated there
}

/*******************************findExpsGenSets************************************/

void  AstNodeBase::addToGenSet_R6( int id_ , AstNodeBase * exprNode){
    
    if(id_==(0-1))
        id_=AstNodeBase::tmpCount--;
    
    AvailableExprEntry entry;
    entry.id_=id_;
    entry.exprAsStr=exprNode->exprAsStr;
    entry.idsUsedInExp=exprNode->idsUsedInExp;
    
    if(genSet.find(id_)!=genSet.end())
          assert(false);
    
    this->genSet[id_]=entry;
}

void  AstNodeBase::setToSameGenSetOf_R6(  AstNodeBase * src){
    
    this->genSet=src->genSet;
}

void  AstNodeBase::appendGenSetToGentSetofNode_R6( AstNodeBase * dst){
    
    for(map<int , AvailableExprEntry>::iterator it=this->genSet.begin(); it!=genSet.end(); it++){
        
        if(dst->genSet.find(it->first)!=dst->genSet.end())
            ;// assert(false);
        else
            dst->genSet[it->first]=it->second;
    }
    
}

void  AstNodeBase::removeKillsFromGenSet_R6(AstNodeBase * killSrc){
    
    for(set<int>::iterator it=killSrc->killSet.begin(); it!=killSrc->killSet.end(); it++){
        
        this->genSet.erase(*it);
    }
}

void  AstNodeBase::setGenSetToInerSectionOf_R6( AstNodeBase * src1, AstNodeBase * src2){
    
    this->genSet.clear();
    for(map<int,AvailableExprEntry>::iterator it=src1->genSet.begin(); it!=src1->genSet.end(); it++)
    {
        if(src2->genSet.find(it->first)!=src2->genSet.end()){
            if(src1->genSet[it->first].exprAsStr.compare(src2->genSet[it->first].exprAsStr)==0){
                this->genSet[it->first]=it->second;
            }
        }
        
    }
}

int   AstNodeBase::tmpCount=-2;

//gen sets only built for stmt nodes (If /SEQ/ASSIGN/ and WHILE) not for expr nodes(ID/CONST/BIN)
TF_Traversal void findExpsGenSets(AstNodeBase * n){
    COUNT_FUSED_inner
    if(true){
        increaseCount(n);
        if(n==NULL || IS_DO_OPT_AGAIN|| consistencyError||typeError||multiDef_v||multiDef_f ||undefinedRef)
            return;
    }
    //VARDEF
    /** nothing to do */
    
    //FUNCDEF
    findExpsGenSets(n->funBody);
    findExpsGenSets(n->nextFuncDef);
    
    //CONST
    /** nothing to do */
    
    
    //VARREF
    /** nothing to do */
    
    
    //ASSIGN
    if(n->nodeType==ASSIGN){
        n->addToGenSet_R6(n->assignId->id, n->assignExpr);
        n->assignExpr->readUsedIds_R2();
    }
    
    
    
    findExpsGenSets(n->assignExpr);
    if(n->nodeType==ASSIGN){
        n->assignExpr->appendGenSetToGentSetofNode_R6(n);
        n->writeGenSet_R6();
    }
    
    //SEQ
    findExpsGenSets(n->seqLeft);
    findExpsGenSets(n->seqRight);
  
    if(n->nodeType==SEQ){
        if(n->seqLeft==NULL && n->seqRight==NULL){
        }
        else{
            
            n->setToSameGenSetOf_R6(n->seqLeft);
            n->seqLeft->readGenSet_R6();
            
            if(n->seqRight!=NULL){
                //Gen(seq)=Gen(left)-kills(right)+gen(right)
                n->removeKillsFromGenSet_R6(n->seqRight);
                n->seqRight->readKillSet_R3();
                n->seqRight->appendGenSetToGentSetofNode_R6(n);
                n->writeGenSet_R6();
                
            }
        }
    }
    
    
    //IF
    findExpsGenSets(n->ifElseBody);
    findExpsGenSets(n->ifThenBody);
    
    if(n->nodeType==IF){
        n->setGenSetToInerSectionOf_R6(n->ifElseBody, n->ifThenBody);
        n->ifElseBody->readGenSet_R6();
        n->ifThenBody->readGenSet_R6();
    }
    
    //BIN
    findExpsGenSets(n->binLeft);
    findExpsGenSets(n->binRight);
    //6
    if(n->nodeType==BIN){
        n->binLeft->appendGenSetToGentSetofNode_R6(n);
        n->writeGenSet_R6();
    }
    if(n->nodeType==BIN){
        n->binRight->appendGenSetToGentSetofNode_R6(n);
        n->writeGenSet_R6();
    }
    if(n->nodeType==BIN){
        n->addToGenSet_R6(0-1, n);
        n->writeGenSet_R6();
    }
    
    //WHILE
    
    findExpsGenSets(n->whileBody);
    if(n->nodeType==WHILE){
        //  n->ifThenBody->readGenSet_R6();
    }
    //gen is empty for the while
}


/*******************************findPostAvailableExpr*********************************///
/** post avalaible expression are the set of expression that are avaialable after a specific stmt 
 is executed*///

void   AstNodeBase::updateAvailableExpr_R4(int id_,AstNodeBase * exprNode ){
    if(id_==0)
        return;
    
    AvailableExprEntry entry;
    entry.id_=id_;
    entry.exprAsStr=exprNode->exprAsStr;
    entry.idsUsedInExp=exprNode->idsUsedInExp;
    //if it already exists and the assigment is the same do nothing
    if(availableExpresions.find(id_)!=availableExpresions.end()){
        if(availableExpresions[id_].exprAsStr.compare(entry.exprAsStr)==0)
            return ;
    }
    
    //update the entry
    this->availableExpresions[id_]=entry;
    
    for(map<int , AvailableExprEntry>::iterator it=this->availableExpresions.begin(); it!=this->availableExpresions.end();){
        
        
        if(it->second.idsUsedInExp.find(id_)!=it->second.idsUsedInExp.end()){
            this->availableExpresions.erase(it++);
            
        }else{
            ++it;
        }
    }
}
//this will clear the available expression at the src first

void   AstNodeBase::copyAvailableExprTo_R4(AstNodeBase * dst ){
    dst->availableExpresions.clear();
    dst->availableExpresions=this->availableExpresions;
}

void   AstNodeBase::setToIntersection_R4( AstNodeBase * src1, AstNodeBase * src2){
    
    
    this->availableExpresions.clear();
    
    for(map<int , AvailableExprEntry>::iterator it1=src1->availableExpresions.begin(); it1!=src1->availableExpresions.end();it1++){
        
        
        if(src2->availableExpresions.find(it1->first)!=src2->availableExpresions.end()){
            if(src2->availableExpresions[it1->first].exprAsStr.compare(it1->second.exprAsStr)==0){
                availableExpresions[it1->first]=it1->second;
            }
        }
        
    }
    
}

void   AstNodeBase::addGensToAvaialbeExpr_R4(){
    
    availableExpresions.insert(genSet.begin(),genSet.end());
    
}

void   AstNodeBase::removeKillsFromAvaialbeExpr_R4(){
    
    for(set<int>::iterator it=killSet.begin(); it!=killSet.end(); it++){
        availableExpresions.erase(*it);
    }
}

//OUT(while)=Out(whilebody)=IN(whilebody)=IN(whileconde)=OUT(whilecond)=IN(while)- KILLS(while)+
//(IN(While) INTERSECTION GEN(whilebody)

void   AstNodeBase::computeWhileAvaialbleExpr_R4(){
    
    map<int,AvailableExprEntry> intersection;
    for(map<int , AvailableExprEntry>::iterator it1=this->availableExpresions.begin(); it1!=this->availableExpresions.end();it1++){
        
        
        if(whileBody->genSet.find(it1->first)!=whileBody->genSet.end()){
            if(whileBody->genSet[it1->first].exprAsStr.compare(it1->second.exprAsStr)==0){
                intersection[it1->first]=it1->second;
            }
        }
    }
    
    //remove kills from IN
    this->removeKillsFromAvaialbeExpr_R4();
    //add the intersection
    
    availableExpresions.insert(intersection.begin(), intersection.end());
    
    
}

TF_Traversal void findPostAvailableExpr(AstNodeBase * n ){
    
    COUNT_FUSED_inner
    
    if(true){
        increaseCount(n);
        if(n==NULL || IS_DO_OPT_AGAIN ||  consistencyError||typeError||multiDef_v||multiDef_f ||undefinedRef)
            return ;
    }
    
    //VARDEF
    /** nothing to do */
    
    //FUNCDEF
    findPostAvailableExpr(n->funBody);
    findPostAvailableExpr(n->nextFuncDef);
    
    //CONST
    /** nothing to do */
    
    
    //VARREF
    /** nothing to do */
    
    
    //Pre
    if(n->nodeType==ASSIGN){
        n->copyAvailableExprTo_R4(n->assignExpr);
        n->assignExpr->writeAvaileExpr_R4();
        
    }
    if(n->nodeType==SEQ){
        
        if(n->seqLeft!=NULL){
            n->copyAvailableExprTo_R4(n->seqLeft);
            n->seqLeft->writeAvaileExpr_R4();
        }
    }
    if(n->nodeType==BIN){
        n->copyAvailableExprTo_R4(n->binLeft);
        n->binLeft->writeAvaileExpr_R4();
        
    }
    if(n->nodeType==IF){
        n->copyAvailableExprTo_R4(n->ifCond);
        n->ifCond->writeAvaileExpr_R4();
    }
    if(n->nodeType==IF){
        n->copyAvailableExprTo_R4(n->ifThenBody);
        n->ifThenBody->writeAvaileExpr_R4();
        
    }
    if(n->nodeType==IF){
        n->copyAvailableExprTo_R4(n->ifElseBody);
        n->ifElseBody->writeAvaileExpr_R4();
        
    }
    if(n->nodeType==WHILE){
        n->computeWhileAvaialbleExpr_R4();
        n->whileBody->readGenSet_R6();
        n->readKillSet_R3();
    }
    //28
    if(n->nodeType==WHILE){
        n->copyAvailableExprTo_R4(n->whileBody);
        n->whileBody->writeAvaileExpr_R4();
    }
    if(n->nodeType==WHILE){
        n->copyAvailableExprTo_R4(n->whileCond);
        n->whileCond->writeAvaileExpr_R4();
    }
    
    findPostAvailableExpr(n->assignExpr);
    findPostAvailableExpr(n->seqLeft);
    findPostAvailableExpr(n->binLeft);
    findPostAvailableExpr(n->ifCond);
    findPostAvailableExpr(n->ifThenBody);
    findPostAvailableExpr(n->ifElseBody);
    findPostAvailableExpr(n->whileBody);
    findPostAvailableExpr(n->whileCond);
    //IN
    
    //SEQ
    if(n->nodeType==SEQ){
        if(n->seqLeft!=NULL && n->seqRight!=NULL){
            n->seqLeft->copyAvailableExprTo_R4(n->seqRight);
            n->seqRight->writeAvaileExpr_R4();
        }
    }
    if(n->nodeType==BIN){
        
        n->binLeft->copyAvailableExprTo_R4(n->binRight);
        n->binRight->writeAvaileExpr_R4();
        
    }
    findPostAvailableExpr(n->seqRight);
    findPostAvailableExpr(n->binRight);
    
    //POST
    if(n->nodeType==SEQ){
        //always set available expression the node to the set gen expression of that node.
        n->removeKillsFromAvaialbeExpr_R4();
        n->readKillSet_R3();
    }
    if(n->nodeType==SEQ){
        n->addGensToAvaialbeExpr_R4();
        n->readGenSet_R6();
    }
    
    //Post
    if(n->nodeType==ASSIGN){
        n->removeKillsFromAvaialbeExpr_R4();
        n->readKillSet_R3();
    }
    
    if(n->nodeType==ASSIGN){
        n->addGensToAvaialbeExpr_R4();
        n->readGenSet_R6();
    }
    
    if(n->nodeType==BIN){
        n->addGensToAvaialbeExpr_R4();
        n->readGenSet_R6();
    }
    
    
    
    //11
    if(n->nodeType==IF){
        n->removeKillsFromAvaialbeExpr_R4();
        n->readKillSet_R3();
        
    }
    if(n->nodeType==IF){
        n->addGensToAvaialbeExpr_R4();
        n->ifElseBody->readAvaileExpr_R4();
    }
    
    //WHILE
    
    //OUT(while)=Out(whilebody)=IN(whilebody)=IN(whileconde)=OUT(whilecond)=IN(while)- KILLS(while)+(IN(While) INTERSECTION GEN(whilebody))

    
}

#endif /* availableExpr_h */
