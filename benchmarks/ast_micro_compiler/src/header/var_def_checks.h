//
//  var_def_checks.h
//  libtooling
//
//  Created by lsakka on 3/15/17.
//  Copyright © 2017 lsakka. All rights reserved.
//

#ifndef var_def_checks_h
#define var_def_checks_h



TF_AbsrtactedAccess((7,'w','global'))
void set_curr_symboltable( AstNodeBase * funDef){
    curr_function = funDef->funcName;
    def_counter = 0;

}

TF_AbsrtactedAccess((7,'r','global'))
bool def_exists( AstNodeBase * var){

    if(symbolTable_simple[curr_function].find(var->idName) == symbolTable_simple[curr_function].end()){
        return false;
    }
    else{
        return true;
    }
}

TF_AbsrtactedAccess((7,'w','global'))
int add_def_to_symboltable( AstNodeBase * varDef){

    symbolTable_simple[curr_function][varDef->idName]=def_counter+1;
    def_counter++;
    return def_counter;
}

TF_AbsrtactedAccess((7,'r','global'))

int get_var_ref_id( AstNodeBase * varRef){
    
    return symbolTable_simple[curr_function][varRef->idName];

}


TF_Traversal void checkVarDefs_AssignIds(AstNodeBase * n){
    COUNT_FUSED_inner
    if(true){
        increaseCount(n);
        if( n==NULL || IS_FIRST_IT)
            return ;
    }
    
    //ASSIGN
    checkVarDefs_AssignIds(n->assignId);
    checkVarDefs_AssignIds(n->assignExpr);
    
    //BIN
    checkVarDefs_AssignIds(n->binLeft);
    checkVarDefs_AssignIds(n->binRight);

    // VARREF (ref should exist)
    if(n->nodeType== VARREF ){
        
        if(def_exists(n) == false ){
            undefinedRef = true;
            return;
        }
    }
    if(n->nodeType== VARREF ){
        n->id = get_var_ref_id(n);
    }
    
    //IF
    checkVarDefs_AssignIds(n->ifCond);
    checkVarDefs_AssignIds(n->ifThenBody);
    checkVarDefs_AssignIds(n->ifElseBody);
    
    //CONSTANT
    
    //SEQ
    checkVarDefs_AssignIds(n->seqLeft);
    checkVarDefs_AssignIds(n->seqRight);
    
    //WHILE
    checkVarDefs_AssignIds(n->whileCond);
    checkVarDefs_AssignIds(n->whileBody);
   
    //FUNCDEF
    if(n->nodeType== FUNCDEF){
        set_curr_symboltable(n);
    }
    
    checkVarDefs_AssignIds(n->funBody);
    checkVarDefs_AssignIds(n->nextFuncDef);
    
    //VARDEF
    if(n->nodeType == VARDEF ){
        if(def_exists(n) ==  true){
            multiDef_v= true;
            return;
        }
    }
    if(n->nodeType == VARDEF){
        n->id = add_def_to_symboltable(n);
    }

    
}



#endif /* var_def_checks_h */
