//
//  astTraversals.hpp
//  libtooling
//
//  Created by lsakka on 6/10/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#ifndef astTraversals_hpp
#define astTraversals_hpp

//#define COUNT_FUSED_inner if( n != NULL) counterFused++ ; counterFusedAll++ ;
#define COUNT_FUSED_inner
#define COUNT_FUSED_inner__ if( _r != NULL) counterFused++; counterFusedAll++;


#include<string>
#include<map>
#include<vector>
#include<string>
#include<set>
#include <cassert>
#include "treefuser.h"

#define ASSIGN 1
#define BIN 2
#define VARREF 3
#define IF 4
#define CONSTANT 5
#define SEQ 6
#define WHILE 7
#define FUNCDEF 8
#define VARDEF 9

//define binary operater types
#define ADD 1
#define SUB 2
#define MUL 3
#define DIV 4
#define EQ 5
#define NEQ 6
#define GR 7
#define LE 8
#define GEQ 9
#define LEQ 10


#define AND 300
#define OR 400
//define types
#define INT_TYPE 1
#define BOOL_TYPE 2


#define IS_FIRST_IT false
#define IS_DO_OPT_AGAIN false


using namespace std;

struct AvailableExprEntry{
    int id_;
    string exprAsStr;
    set<int> idsUsedInExp;
    
};

extern std::string nodeTypesToStr[];
extern bool doOptimizeAgain;
extern bool isFirtIteration;

extern bool typeError;
extern bool consistencyError;
extern bool multiDef_v;
extern bool multiDef_f;
extern bool undefinedRef;

extern int counter;
extern int counterAll;

extern int counterFused;
extern int counterFusedAll;

extern map < string , map<string, int> > symbolTable_simple;
extern string curr_function;
extern int def_counter;

class TF_TreeStructure AstNodeBase{
public:
    
    TF_TreeRecursiveField AstNodeBase * whileCond , * whileBody;
    TF_TreeRecursiveField AstNodeBase * ifCond , * ifThenBody,* ifElseBody;
    TF_TreeRecursiveField AstNodeBase * assignId , * assignExpr;
    TF_TreeRecursiveField AstNodeBase * binLeft , * binRight;
    TF_TreeRecursiveField  AstNodeBase * seqLeft , * seqRight;
    TF_TreeRecursiveField AstNodeBase * funBody , * nextFuncDef;
    
    AstNodeBase(int nodeType_, int valueType_, int binOperator_, int id_,  string idName_,int intValue_, int boolValue_);
    
    AstNodeBase(int nodeType_, int valueType_, int binOperator_, int id_,  string idName_,int intValue_, int boolValue_,string funcName_);
    
    int counter;
    static int tmpCount;
    int nodeType;
    int valueType;
    string funcName;
    int binOperator;
    bool valueComputed;
    int id;
    string idName;
    int intValue;
    int boolValue;

    //[Uses] abstract region 2
    set<int> idsUsedInExp;
    string exprAsStr;

    TF_AbsrtactedAccess((2,'r','local'))  void addUsedIdsTo_R2( AstNodeBase * dst);
    TF_AbsrtactedAccess((2,'r','local'))  void readUsedIds_R2(){;}
    TF_AbsrtactedAccess((2,'w','local'))  void writeUsedIds_R2(){;}
    TF_AbsrtactedAccess((2,'w','local')) void addIdToUSedIds_R2(int Id_){
        this->idsUsedInExp.insert(Id_);
    }
    TF_AbsrtactedAccess((2,'w','local'))  void buildStringRep(int leftNodeType, int rightNodeType , int leftNodeValueType, int rightNodeValueType , int curNodeOperator_);
    TF_AbsrtactedAccess((2,'w','local'))  void buildStringExprConst_R2(int value_type ,int intval, bool boolVal);
    TF_AbsrtactedAccess((2,'w','local'))  void buildStringExprID_R2(int id_  );
    TF_AbsrtactedAccess((2,'w','local'))  void buildStringExprBIN_R2(int binaryOperator  );
    
    
    
    
    
    //[availableExpresions] abstract region 4
    map<int , AvailableExprEntry> availableExpresions;

    
    TF_AbsrtactedAccess((4,'w','local'))  void   addGensToAvaialbeExpr_R4();
    TF_AbsrtactedAccess((4,'w','local'))  void   clearAvaialbleExpr_R4(){
        availableExpresions.clear();
    }
    TF_AbsrtactedAccess((4,'w','local'))  void   removeKillsFromAvaialbeExpr_R4();
    TF_AbsrtactedAccess((4,'w','local'))  void   computeWhileAvaialbleExpr_R4();
    TF_AbsrtactedAccess((4,'w','local'))  void updateAvailableExpr_R4(int id_,AstNodeBase * exprNode );
    TF_AbsrtactedAccess((4,'r','local'))  void copyAvailableExprTo_R4( AstNodeBase * dst);
    TF_AbsrtactedAccess((4,'w','local'))  void writeAvaileExpr_R4(){;}
    TF_AbsrtactedAccess((4,'r','local'))  void readAvaileExpr_R4(){;}
    TF_AbsrtactedAccess((4,'w','local'))  void setToIntersection_R4( AstNodeBase * src1, AstNodeBase * src2);
    
    //[constant folding] abstract region 5
    map<int , int> idToConstVal;
    
    TF_AbsrtactedAccess((5,'w','local')) void addToConstantMap_R5( int id_ , int val);
    TF_AbsrtactedAccess((5,'w','local')) void clearConstantMap_R5();
    TF_AbsrtactedAccess((5,'r','local')) bool isInConstanMap_R5(  int id_ );
    TF_AbsrtactedAccess((5,'r','local')) int  readFromConstantMap_R5( int id_ );
    
    
    
    
    
    TF_AbsrtactedAccess((5,'w','local')) void setConstantMapToIntersectionOf_R5( AstNodeBase * nsrc1 ,AstNodeBase * nsrc2 );
    TF_AbsrtactedAccess((5,'r','local')) void copyConstantMapTo_R5(AstNodeBase * ndest);
    TF_AbsrtactedAccess((5,'r','local')) void readConstantMap_R5(){;}
    TF_AbsrtactedAccess((5,'w','local')) void writeConstantMap_R5(){;}
    
    //GenSet abstract region 6
    map<int , AvailableExprEntry> genSet;
   
    TF_AbsrtactedAccess((6,'w','local')) void  addToGenSet_R6( int id_ , AstNodeBase * exprNode);
    TF_AbsrtactedAccess((6,'w','local'))void  setToSameGenSetOf_R6(AstNodeBase * src);
    TF_AbsrtactedAccess((6,'w','local'))void  removeKillsFromGenSet_R6(AstNodeBase * killSrc);
    
    
    TF_AbsrtactedAccess((6,'w','local')) void  setGenSetToInerSectionOf_R6( AstNodeBase * src1, AstNodeBase * src2);
    TF_AbsrtactedAccess((6,'r','local')) void  appendGenSetToGentSetofNode_R6( AstNodeBase * dst);
    
    TF_AbsrtactedAccess((6,'r','local')) void readGenSet_R6(){;}
    TF_AbsrtactedAccess((6,'w','local')) void writeGenSet_R6(){;}
    
    //kill sets access abstract region 3
    set<int> killSet;
    TF_AbsrtactedAccess((3,'r','local'))  void readKillSet_R3(){;}
    TF_AbsrtactedAccess((3,'w','local'))  void writeKillSet_R3(){;}
    TF_AbsrtactedAccess((3,'w','local'))  void addToKillSet_R3(int id_);
    TF_AbsrtactedAccess((3,'w','local'))  void appendKillSetToKillSetOfNode_R3( AstNodeBase * dst);
    
};


//Gloval Strict Access Functions Used to Build function table

TF_AbsrtactedAccess((7,'r','global'))  bool function_name_exist( AstNodeBase * funDef);
TF_AbsrtactedAccess((7,'w','global'))  void add_function_to_symboltable( AstNodeBase * funDef);
TF_AbsrtactedAccess((7,'w','global'))  void set_curr_symboltable( AstNodeBase * funDef);
TF_AbsrtactedAccess((7,'r','global'))  bool def_exists( AstNodeBase * funDef);
TF_AbsrtactedAccess((7,'w','global'))  int  add_def_to_symboltable( AstNodeBase * funDef);
TF_AbsrtactedAccess((7,'r','global'))  int  get_var_ref_id( AstNodeBase * varRef);
TF_AbsrtactedAccess((8,'r','global')) void increaseCount(AstNodeBase * n);


AstNodeBase * createSeqNode();
AstNodeBase * createIFNode();
AstNodeBase * createAssignNode();
AstNodeBase * createBinNode(int binOperator);
AstNodeBase * createConstNode(int val);
AstNodeBase * createConstBoolNode(bool val);
AstNodeBase * createVarRefNode(int id, string name , int type);
AstNodeBase * createVarDefNode(int id, string name , int type);
AstNodeBase * createWhileNode();
AstNodeBase * createFunDefNode(string funName);
#endif /* astTraversals_hpp */
