//
//  checkAndSetTypes2.h
//  libtooling
//
//  Created by lsakka on 6/27/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#ifndef checkAndSetTypes2_h
#define checkAndSetTypes2_h


#include "astnode.h"

TF_Traversal void checkAndSetTypes(AstNodeBase * n){
    COUNT_FUSED_inner
    if(true){
        increaseCount( n);
        if( n==NULL  ||IS_FIRST_IT ||consistencyError )
            return ;
    }
    bool condError=false;
    bool binError=false;
    bool assignError=false;
    
    if(binError||condError||assignError)
        typeError=true;
    
    //VARDEF
    /** nothing to do */
    
    //FUNCDEF
    checkAndSetTypes(n->funBody);
    checkAndSetTypes(n->nextFuncDef);
    
    //CONST
    /** nothing to do */
    
    
    //VARREF
    /** nothing to do */
    
    //ASSIGN
    checkAndSetTypes(n->assignExpr);
    if(n->nodeType==ASSIGN ){
        if(n->assignId->valueType!= n->assignExpr->valueType){
            assignError =true;
        }
    }
    //SEQ
    checkAndSetTypes(n->seqLeft);
    checkAndSetTypes(n->seqRight);
    
    //IF
    checkAndSetTypes(n->ifCond);
    if(n->nodeType==IF ){
        if(n->ifCond->valueType!=BOOL_TYPE){
            condError =true;
        }
    }
    checkAndSetTypes(n->ifThenBody);
    checkAndSetTypes(n->ifElseBody);
    
    //BIN
    checkAndSetTypes(n->binLeft);
    checkAndSetTypes(n->binRight);
    
    if(n->nodeType==BIN){
        if(n->binOperator==ADD ||n->binOperator==SUB){
            if(n->binLeft->valueType!=INT_TYPE || n->binRight->valueType!=INT_TYPE){
                binError =true;
                
            }
            else{
                n->valueType=INT_TYPE;
            }
        }
        if(n->binOperator==OR ||n->binOperator==AND ){
            if(n->binLeft->valueType!=BOOL_TYPE || n->binRight->valueType!=BOOL_TYPE){
                binError =true;
                
            }
            else{
                n->valueType=BOOL_TYPE;
            }
        }
        if(n->binOperator==EQ||n->binOperator==NEQ){
            if(n->binLeft->valueType != n->binRight->valueType){
                binError =true;
                
            }
            else{
                n->valueType=BOOL_TYPE;
            }
        }
        
    }
    
    
    //WHILE
    checkAndSetTypes(n->whileCond);
    if(n->nodeType==WHILE){
        if(n->whileCond->valueType!=BOOL_TYPE){
            condError =true;
        }
    }
    checkAndSetTypes(n->whileBody);
}


#endif /* checkAndSetTypes2_h */
