//
//  constantFolding2.h
//  libtooling
//
//  Created by lsakka on 6/27/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#ifndef constantFolding2_h
#define constantFolding2_h


#include <map>
#include "astnode.h"

TF_Traversal void constantFolding(AstNodeBase * n ){
    COUNT_FUSED_inner
    if(true){
    increaseCount(n);
    if(n==NULL || n->nodeType==CONSTANT || consistencyError||typeError||multiDef_v||multiDef_f ||undefinedRef )
        return ;
    }
    //VARDEF
    /** nothing to do */
    //FUNCDEF
    constantFolding(n->funBody);
    constantFolding(n->nextFuncDef);
    //ASSIGN
    constantFolding(n->assignExpr);
    //SEQ
    constantFolding(n->seqLeft);
    constantFolding(n->seqRight);
    //IF
    constantFolding(n->ifCond);
    constantFolding(n->ifThenBody);
    constantFolding(n->ifElseBody);
    //BIN
    constantFolding(n->binLeft);
    constantFolding(n->binRight);
    if(n->nodeType==BIN){
        
        if(n->binLeft->nodeType==CONSTANT && n->binRight->nodeType==CONSTANT){
            n->nodeType=CONSTANT;
            n->valueComputed=true;
            //  doOptimizeAgain=true;
            if(n->binOperator==ADD)
                n->intValue  = n->binLeft->intValue + n->binRight->intValue;
            
            if(n->binOperator==SUB)
                n->intValue = n->binLeft->intValue-n->binRight->intValue;
            
            if(n->binOperator==AND)
                n->boolValue  = n->binLeft->boolValue && n->binRight->boolValue;
            
            if(n->binOperator==OR)
                n->boolValue  = n->binLeft->boolValue || n->binRight->boolValue;
            
            if(n->binOperator==EQ){
                if(n->binLeft->valueType==BOOL_TYPE)
                    n->boolValue  = n->binLeft->boolValue == n->binRight->boolValue;
                
                if(n->binLeft->valueType==INT_TYPE)
                    n->boolValue  = n->binLeft->intValue == n->binRight->intValue;
                
            }
            if(n->binOperator==NEQ){
                if(n->binLeft->valueType==BOOL_TYPE)
                    n->boolValue  = n->binLeft->boolValue != n->binRight->boolValue;
                
                if(n->binLeft->valueType==INT_TYPE)
                    n->boolValue  = n->binLeft->intValue != n->binRight->intValue;
            }
        }
    }
    //WHILE
    constantFolding(n->whileCond);
    constantFolding(n->whileBody);
        
}


#endif /* constantFolding2_h */
