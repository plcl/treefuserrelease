

#include "treefuser.h"
#include <map>
#include<set>

using namespace std; 

class TF_TreeStructure CompleteBinaryTree{

public:	
	TF_TreeRecursiveField CompleteBinaryTree * left, * right;
	bool isLeaf;
	int value; 
	int max; 
	int min ; 
	float average; 
	int descendent_count;
  
	set< int > data; 
	
	TF_AbsrtactedAccess((3,'1','local')) int checkLocalSet(int key){
	
		if(data.find(key)!=data.end())
			return 1; 
		else
			return 0;
	}
	
}; 



map<CompleteBinaryTree * , int>    nodeToKey;
map<CompleteBinaryTree * , int >   foundMap;



TF_AbsrtactedAccess((1,'r','global')) int getKey( CompleteBinaryTree  * n){
	return nodeToKey[n];	
}

TF_AbsrtactedAccess((2,'w','global')) 
void updateGlobalResult(int res, CompleteBinaryTree * n){

	foundMap[n]=res;
}

TF_Traversal void  searchSets(CompleteBinaryTree * n){

	  searchSets(n->left);
        searchSets(n->right);
	
	int searchKey; 
	searchKey= getKey(n);

	int valueFound;
	valueFound = n->checkLocalSet(searchKey); 	

	updateGlobalResult(valueFound, n); 		
}


TF_Traversal void findMin( CompleteBinaryTree * n){

	if (n->isLeaf) 
		n->min = n->value; 


	if(n->isLeaf)
		return; 

	findMin	(n->left); 
	findMin (n->right); 

	if(n->left->min > n->right->min)	
		n->min=n->left->min; 
	else
		n->min = n->right->min; 
} 

TF_Traversal void findAverage(CompleteBinaryTree* n){

	if (n->isLeaf){ 
		n->average = n->value;  
		n->descendent_count = 0;
		return;
	}

	findAverage (n->left); 
	findAverage (n->right); 

	n->average = n->left->average + n->right->average ; 
	n->descendent_count = n->left->descendent_count+ n->right->descendent_count;
	n->average = n->average/ n->descendent_count;

}

int main(){
	CompleteBinaryTree * root; 
	//assumed the tree is built
	findMin(root);
	findAverage(root);
	searchSets(root);	
}

