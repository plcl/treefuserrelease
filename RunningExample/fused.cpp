

#include "treefuser.h"
#include <map>
#include<set>

using namespace std; 

class TF_TreeStructure CompleteBinaryTree{

public:	
	TF_TreeRecursiveField CompleteBinaryTree * left, * right;
	bool isLeaf;
	int value; 
	int max; 
	int min ; 
	float average; 
	int descendent_count;
  
	set< int > data; 
	
	TF_AbsrtactedAccess((3,'1','local')) int checkLocalSet(int key){
	
		if(data.find(key)!=data.end())
			return 1; 
		else
			return 0;
	}
	
}; 



map<CompleteBinaryTree * , int>    nodeToKey;
map<CompleteBinaryTree * , int >   foundMap;



TF_AbsrtactedAccess((1,'r','global')) int getKey( CompleteBinaryTree  * n){
	return nodeToKey[n];	
}

TF_AbsrtactedAccess((2,'w','global')) 
void updateGlobalResult(int res, CompleteBinaryTree * n){

	foundMap[n]=res;
}

TF_Traversal void  searchSets(CompleteBinaryTree * n){

	  searchSets(n->left);
        searchSets(n->right);
	
	int searchKey; 
	searchKey= getKey(n);

	int valueFound;
	valueFound = n->checkLocalSet(searchKey); 	

	updateGlobalResult(valueFound, n); 		
}


TF_Traversal void findMin( CompleteBinaryTree * n){

	if (n->isLeaf) 
		n->min = n->value; 


	if(n->isLeaf)
		return; 

	findMin	(n->left); 
	findMin (n->right); 

	if(n->left->min > n->right->min)	
		n->min=n->left->min; 
	else
		n->min = n->right->min; 
} 

TF_Traversal void findAverage(CompleteBinaryTree* n){

	if (n->isLeaf){ 
		n->average = n->value;  
		n->descendent_count = 0;
		return;
	}

	findAverage (n->left); 
	findAverage (n->right); 

	n->average = n->left->average + n->right->average ; 
	n->descendent_count = n->left->descendent_count+ n->right->descendent_count;
	n->average = n->average/ n->descendent_count;

}

void _fuse_1_F1F2F3(class CompleteBinaryTree * _r,unsigned int truncate_flags);
void _fuse_1_F1F2F3(class CompleteBinaryTree * _r,unsigned int truncate_flags)
{
		int _f3_searchKey ;
;
		int _f3_valueFound ;
;
//block 1
if (truncate_flags &0b1) {
	 if (_r->isLeaf){
	_r->min=_r->value;
	}
	 if (_r->isLeaf){
	 truncate_flags&=0b11111111111111111111111111111110; goto _label_B1F1_Exit ;
	}
}
_label_B1F1_Exit:
if (truncate_flags &0b10) {
	 if (_r->isLeaf){
	_r->average=_r->value;
	_r->descendent_count=0;
	 truncate_flags&=0b11111111111111111111111111111101; goto _label_B1F2_Exit ;
	}
}
_label_B1F2_Exit:
if ( (truncate_flags & 0b111) ){
	_fuse_1_F1F2F3(_r->left,truncate_flags & 0b111);
}//block 2
if ( (truncate_flags & 0b111) ){
	_fuse_1_F1F2F3(_r->right,truncate_flags & 0b111);
}//block 3
if (truncate_flags &0b1) {
	 if (_r->left->min>_r->right->min){
	_r->min=_r->left->min;
	}else{
		_r->min=_r->right->min;

	}}
_label_B3F1_Exit:
if (truncate_flags &0b10) {
	_r->average=_r->left->average+_r->right->average;
	_r->descendent_count=_r->left->descendent_count+_r->right->descendent_count;
	_r->average=_r->average/_r->descendent_count;
}
_label_B3F2_Exit:
if (truncate_flags &0b100) {
	_f3_searchKey=getKey(_r);
	_f3_valueFound=_r->checkLocalSet(_f3_searchKey);
updateGlobalResult(_f3_valueFound,_r);}
_label_B3F3_Exit:
return ;

};
int main(){
	CompleteBinaryTree * root; 
	//assumed the tree is built
	//findMin(root);
	//findAverage(root);
	//searchSets(root);	

	//added by fuse transformer 
	_fuse_1_F1F2F3(root,0b111);
}

