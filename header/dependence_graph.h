//
//  DependeceGraph.hpp
//  libtooling
//
//  Created by lsakka on 5/3/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#ifndef DependeceGraph_hpp
#define DependeceGraph_hpp

#include <stdio.h>
#include "statemant_info.h"
#include <vector>
#include <map>
#include "logger.h"
#include <stack>
class DG_Node;
class DDG;
enum DepType{global_dep , local_dep, ontree_dep ,ontree_dep_fusable,control_dep };

struct MergeInfo{
    public :
    set<DG_Node * > mergedNodes;
    bool isInMergedNodes(DG_Node * node ){
        return mergedNodes.find(node)!=mergedNodes.end();
    }
};
struct DepInfo{
public:

    bool global_dep=false;
    bool local_dep=false;
    bool ontree_dep=false;
    bool ontree_dep_fusable=false;
    bool control_dep=false;
    bool nonFusable(){
        return global_dep||local_dep||ontree_dep||control_dep;
        
    }
    
};



class DG_Node{
    friend DDG;
public:
    map< DG_Node * , DepInfo> succList;
    map< DG_Node * , DepInfo> predList;


    DG_Node(StatmentInfo * StatmentInfo_, int traversalId_){
        StatmentInfo=StatmentInfo_;
        traversalId=traversalId_;
    }
    
    StatmentInfo * StatmentInfo;
    int traversalId;
    bool isMerged=false;
    MergeInfo *  mergeInfo=NULL;
    
    bool isRootNode();
    
};

class DDG{
    
public:
    
    vector<DG_Node* > nodes;
    void dump();
    void dumpToPrint();

    void dumpMergeInfo();
    void merge(DG_Node *  n1 , DG_Node * n2);
    void unmerge(DG_Node * n);
    void addDep(DepType debType, DG_Node * src , DG_Node * dst);
    bool hasWrongFuse(MergeInfo * mergeInfo);

    DG_Node * createNode(pair<StatmentInfo *, int> value);
    bool hasCycle();
    bool hasWrongFuse();
    bool hasIllegalMerge();
private:
    bool hasCycleRec(DG_Node * node, map<DG_Node *, int> & visited, stack<DG_Node * >& cyclePath);

};
#endif /* DependeceGraph_hpp */
