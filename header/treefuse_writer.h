//
//  treefuse-writer.hpp
//  libtooling
//
//  Created by lsakka on 7/14/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#ifndef treefuse_writer_hpp
#define treefuse_writer_hpp

#define NO_CALL 0

#define SINGLE_CALL 1
#define MULTI_CALL 2

#include "access_path.h"
#include <stdio.h>
#include <map>
#include "llvmdep.h"
#include <set>
#include <vector>
#include "function_analyser.h"
#include "function_finder.h"
#include "dependence_graph.h"

class StatmentPrinter {
public:
    string ret;
    clang::ValueDecl * rootDecl;
    string nextLabel;
    int funId;
    int insideExp=0;
    string printStmt(clang::Stmt * stmt, SourceManager &sm,clang::ValueDecl * rootDecl_, string nextLabel_,int funId_){
        ret="";
        rootDecl=rootDecl_;
        nextLabel=nextLabel_;
        funId=funId_;
        
        print_handleStmt(stmt,sm);
        
        
        return ret;
        
    }
    string printStmt(clang::Stmt * stmt, SourceManager &sm){
        ret="";
        print_handleStmt(stmt,sm);
        
        return ret;
        
    }
    std::string stmtTostr(clang::Stmt *d, SourceManager &sm) {
        // (T, U) => "T,,"
        string text = Lexer::getSourceText(CharSourceRange::getTokenRange(d->getSourceRange()), sm, LangOptions(), 0);
        if (text.at(text.size()-1) == ',')
                return Lexer::getSourceText(CharSourceRange::getCharRange(d->getSourceRange()), sm, LangOptions(), 0);
        
        return text;
    }
    
    
    void print_handleStmt(clang::Stmt * stmt, SourceManager &sm);
};


struct FusedTraversalWriteBackInfo{
    
    string body;
    string forwardDecl;
    string name;
    map<int, clang::CallExpr *> particiaptingTraversal_and_Calls;
    
};

class TreeFuseWriter{
    
private:
    static int  counter;
    int fuseId;
    
    clang::FunctionDecl * enclosingFunDecl;
    ASTContext  * context;
    Rewriter & rewriter;
    vector<DG_Node* > & statmentsTopologicalOrder;
    vector<clang::CallExpr *> & traversalsCAllExprList;
    vector<clang::FunctionDecl * > traversalsDeclList;
    
    string createName(vector<bool>&  participatingTraversals);
    
    int findTraversalIdOfFirstCall(vector<bool>&  participatingTraversals);
    
    int countNumberOfParticipatingTraversals(vector<bool>&  participatingTraversals);
    
    
    map<string,FusedTraversalWriteBackInfo * > availableFuseCombonations;
    void generateWriteBackInfo( vector<bool> & participatingTraversals, map<int, clang::CallExpr *> participatingTraversals_and_Calls);
    void setBlockSubPart(string & decls, string & blockPart,vector<bool> &participatingTraversals,int blockId , vector< vector<DG_Node * > > stmtList);
    void setCallPart( string & callPart,vector<bool> &participatingTraversals,DG_Node * callNode, FusedTraversalWriteBackInfo * writeBackInfo);
    
    bool isGenerated( vector<bool> & participatingTraversals);
    pair<int , int> isSingleActiveTraversal(vector<bool>&  participatingTraversals);
public:
    TreeFuseWriter( vector<clang::CallExpr *> & traversalsCAllExprList_,clang::FunctionDecl * enclosingFunDecls_, ASTContext  * context_, Rewriter & rewriter_, vector<DG_Node* > & statmentsTopologicalOrders_ );
    
    //this will check for a set of traversals a body that represents there fusre version is genared or not.
    //set of traversals are identfied by the key ex "_fuse0F1F3F4"
    void  writeBackFusedVersion();
    
};

#endif