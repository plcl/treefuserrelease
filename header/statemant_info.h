//
//  StatmentInfo.hpp
//  libtooling
//
//  Created by lsakka on 5/3/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#ifndef StatmentInfo_hpp
#define StatmentInfo_hpp

#include <stdio.h>
#include "function_analyser.h"



class StatmentInfo{
    
 private :
    
    FunctionAnalyser  * enclosingFunction ;
public:
    bool hasReturn=false;
    clang::FieldDecl       * calledChild  ;

    bool isCallStmt;
    clang::Stmt * stmt;
    int idInTraversals;
 
    
    StatmentInfo( clang::Stmt * stmt_,FunctionAnalyser * enclosingFunction_,bool isCallStmt_,int idInTraversals_){
        isCallStmt=isCallStmt_;
        enclosingFunction=enclosingFunction_;
        stmt=stmt_;
        idInTraversals=idInTraversals_;
    }
    
    FunctionAnalyser  * getEnclosingFunction(){
        return enclosingFunction;
    }
    clang::Decl * getCalledChild(){
        return calledChild;
    }
    
    AccessPathSet      accessPaths ;

};

#endif /* StatmentInfo_hpp */
