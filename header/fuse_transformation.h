//
//  FuseTransformation.hpp
//  libtooling
//
//  Created by lsakka on 4/4/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#ifndef FuseTransformation_hpp
#define FuseTransformation_hpp

#include "access_path.h"
#include <stdio.h>
#include <map>
#include "llvmdep.h"
#include <set>
#include <vector>
#include "function_analyser.h"
#include "function_finder.h"
using namespace std;


/*
 1)analyse record for tree data structures.
 2)anaylyse fuse methods and extract access paths.
 search for candidates using CanFuseCandidateSearchTraversal & perform fusing
 
 */
class FuseTransformation{


    
};


class CanFuseCandidateSearchTraversal: public RecursiveASTVisitor<CanFuseCandidateSearchTraversal>{
    
 public:
    bool VisitCompoundStmt(CompoundStmt * compoundStmt);
    bool VisitFunctionDecl(FunctionDecl * funDecl);

    CanFuseCandidateSearchTraversal(ASTContext  * context_,FunctionFinder * functionsInfo){
        
        rewriter.setSourceMgr(context_->getSourceManager(),context_->getLangOpts() );
        context=context_;
        functionsInfo_=functionsInfo;
    
    }
    Rewriter  rewriter;
    FunctionFinder * functionsInfo_;

private:
   // FuseTransformation * fuseTransfomer;
    ASTContext * context;
    clang::FunctionDecl  * currentFunDecl;
    bool areValidCandidates (clang::CallExpr * call1, clang::CallExpr * call2);
    void fuse(vector<clang::CallExpr *> & canFuseList);

};


#endif /* FuseTransformation_hpp */
