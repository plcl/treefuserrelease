//
//  function_finder.hpp
//  libtooling
//
//  Created by lsakka on 1/15/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

/*this class is responsible for traversing the ast , tracking methods with fuse attribute
and apply fuse attribute semantic checks to  , if this methods statisfies the semantic rules, 
it will be considered for accesspath analysis and for fusing later if applicable.
otherwise error will be printed 
 */

#ifndef function_finder_hpp
#define function_finder_hpp

#include <stdio.h>
#include "llvmdep.h"
#include "function_analyser.h"
#include <vector>

class FunctionFinder: public RecursiveASTVisitor<FunctionFinder> {
    
public:
    
    void findFunctions(ASTContext * context);
    map< clang::FunctionDecl * ,FunctionAnalyser * > functionsInformation;
    bool VisitFunctionDecl(FunctionDecl * funDeclaration);
    FunctionFinder(){
    }
    bool isValidFuse(clang::FunctionDecl *  funcDecl){
    
        if(this->functionsInformation.find(funcDecl)==functionsInformation.end())
            return false;
        else
            return functionsInformation[funcDecl]->isValidFuse();
    }
};


#endif /* function_finder_hpp */
