//
//  TreeNodeChecker.hpp
//  libtooling
//
//  Created by lsakka on 1/5/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#ifndef TreeNodeChecker_hpp
#define TreeNodeChecker_hpp


#include "llvmdep.h"
using namespace clang::tooling;
using namespace llvm;
using namespace std;
using namespace clang;
using namespace clang::ast_matchers;

class RecordInfo{
    friend class RecordsAnalyser;
private:
    int _isTreeNode=-1; //not known yet
    //set<string> childAccessSymbols;
    set<clang::FieldDecl *> childAccessDecls;

    //write ehst you mesn by this !! lol
    int isCompleteScaler=-1; //not known yet
public:
    bool isTreeNode() const {
        assert(_isTreeNode!=-1);
        return _isTreeNode;
    }
    /*
    const set<string> & getChildAccessSymbols()const{
        return childAccessSymbols;
    }*/
    const set<clang::FieldDecl *> & getChildAccessDecls() const {
        return childAccessDecls;
    }
    
    
    /*bool isChildAccessSymbol( string accessSymbol)const {
        
        assert(_isTreeNode==1);
        if(childAccessSymbols.find(accessSymbol)!=childAccessSymbols.end())
            return true;
        else
            return false;
    }*/
    bool isChildAccessDecl( clang::ValueDecl * decl)const {
        
        assert(_isTreeNode==1);
        clang::FieldDecl * feildDecl=dyn_cast<clang::FieldDecl> (decl);
       
        
        if(!clang::ValueDecl::classofKind(clang::Decl::Kind::Field)){
            assert(feildDecl==NULL);
            return false;
        }
        return (childAccessDecls.find(feildDecl)!=childAccessDecls.end());
        
    }
};

/**this class performs the samantic checks on the class types that have teh attribute "tree_node" ,
 TODO : this is supposed to be moved to anothe Semachecks in Clang at the end.
 the followign checks are done:
 1- must have one child with uniquechildattr (pointer to the same type).
 
 */

class RecordsAnalyser : public RecursiveASTVisitor<RecordsAnalyser>{
public:
    
    void analyseRecordDecls(clang::ASTContext  * context_);
    

    //static bool isTreeNodeRecord(const clang::CXXRecordDecl * recordDecl);
    //static const set<string>  & 	getChildAccessSymbols(const clang::RecordDecl * cxxRecord) ;
    static const set<clang::FieldDecl *> &   getChildAccessDecls  (const clang::RecordDecl * cxxRecord) ;

    static const RecordInfo &  getRecordInfo(const clang::RecordDecl * recordDecl) ;
    static bool isCompleteScaler( clang::ValueDecl  * const s) ;
    static bool isScaler( clang::ValueDecl  * const s);
    
    //overrided methods (ast originally)
    bool VisitCXXRecordDecl(clang::CXXRecordDecl * s );

private:
    static map<clang::ASTContext * , map<const clang::RecordDecl *,RecordInfo *  > >  recordsInfoStore;

};


#endif /* TreeNodeChecker_hpp */
