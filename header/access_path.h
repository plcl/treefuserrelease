//
//  accesspath.hpp
//  libtooling
//
//  Created by lsakka on 1/7/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

/**This class responsible for generating and representing the access path ,
 each acces path is an instance of this class
 */

#ifndef accesspath_hpp
#define accesspath_hpp

#define _def_AccessPathIterator  set<AccessPath *,accessPath_compare >::iterator
#define _def_AccessPathSet       set<AccessPath *,accessPath_compare >


#include "llvmdep.h"
#include "logger.h"
class FunctionAnalyser;


class AccessPath {
public:
    AccessPath(clang::Expr * s,FunctionAnalyser * enclosingFunction_,  StrictAccessInfo * attr = NULL);
    
    AccessPath( clang::VarDecl * decl, FunctionAnalyser * enclosingFunction_);
    AccessPath(clang::FunctionDecl * s,FunctionAnalyser * enclosingFunction_);

    //checkers
    bool hasValuePart() const ;
    bool isLegal()      const;
    bool isOnTree()     const;
    bool isOffTree()    const;
    bool isLocal()      const;
    bool isGlobal()     const;
    bool  onlyUses( clang::VarDecl * rootDecl,  const  set<clang::FieldDecl *> &  uniqueChildSymbol)  ;

    //getters
    int getValueStartIndex()         const;
    int getDepth()                   const;
    int getValuePathSize()           const;
    int getTreeAccessPathSize()      const;
    int getTreeAccessPathEndIndex()  const;
    const string & getAsStr()        const;
    clang::ValueDecl * getDeclAtIndex(int i)  const;
    
    //TODO : move to private and create getters!
    //public data memebers
    //string representation only to be used for printing
    vector < pair<string, clang::ValueDecl *> > splittedAccessPath;
    clang::Expr * accessPathStartNode;
    const FunctionAnalyser * enclosingFunction;
    bool isStrictAccessCall=false;
   // clang::StrictAccessAttr * attr;
    StrictAccessInfo attr;
    
private:
    
    //Traversals
    bool collectAPTerms_VisitMemberExpr(clang::MemberExpr * S);
    bool collectAPTerms_VisitDeclRefExpr(clang::DeclRefExpr * S);

    void setValueStartIndex();
    
    void appendSymbol(clang::ValueDecl * declAccess);
    
    //private data members
    //if valueStartIndex=-1 , it is onTree and point to node
    //if valueStartIndex=0  , ioffTree with no value part,
    //if valueStartIndex>0  , ontree with value

    int valueStartIndex=-1;
    bool isDummy=false;
    bool _isLegal=true;
    bool isDeclAccessPath=false;
    string accessPath;
};

struct accessPath_compare {
    bool operator() (const AccessPath *  lhs, const AccessPath *  rhs) const;

};

class AccessPathSet{
    
private:
    std::set <AccessPath * ,accessPath_compare> readSet;
    std::set <AccessPath * ,accessPath_compare> writeSet;
    
    
public:
    
    bool insert(AccessPath * accessPath,bool isWrite);
    void freeAccessPaths();
    const std::set <AccessPath * ,accessPath_compare> & getReadSet()const {
        return readSet;
    }

    const std::set <AccessPath * ,accessPath_compare> &  getWriteSet()const {
        return writeSet;
    }
    
};

class AccessPathAnalyser{
    static bool  collectAPTerms_VisitMemberExpr(clang::MemberExpr * S,AccessPath * accessPath );
    static  bool collectAPTerms_VisitDeclRefExpr(clang::DeclRefExpr * S,AccessPath * accessPath );

};

#endif /* accesspath_hpp */
