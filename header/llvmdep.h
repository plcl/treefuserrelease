//
//  llvmdep.h
//  libtooling
//
//  Created by lsakka on 1/7/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#ifndef llvmdep_h
#define llvmdep_h

#include <vector>
#include <set>
#include <string>
#include <stdio.h>
#include <iostream>
#include <string>

#include <assert.h>
//#include <CoreServices/CoreServices.h>
//#include <mach/mach.h>
//#include <mach/mach_time.h>
#include <unistd.h>

#define __STDC_LIMIT_MACROS
#define __STDC_CONSTANT_MACROS
#include "clang/AST/DeclBase.h"
#include "clang/Rewrite/Core/Rewriter.h"

// Declares clang::SyntaxOnlyAction.
#include "clang/Frontend/FrontendActions.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Lex/Lexer.h"
// Declares llvm::cl::extrahelp.
#include "llvm/Support/CommandLine.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/AST/DeclBase.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"
#include <map>
#include <sstream>


using namespace std;
using namespace clang;

struct StrictAccessInfo{
    bool isReadOnly;
    int id;
    bool isLocal;
    bool isGlobal;
    
};


extern bool hasFuseAnnotation(FunctionDecl * funDecl);
extern bool hasTreeNodeAnnotation(clang::CXXRecordDecl * recordDecl);
extern bool hasUniqueChildAnnotation(clang::FieldDecl * fieldDecl);
extern bool hasStrictAccessAnnotation(clang::Decl * decl);
extern vector<StrictAccessInfo> getStrictAccessInfo(clang::Decl * decl);
#endif /* llvmdep_h */
