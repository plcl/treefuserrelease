//
//  DependenceAnalyser.hpp
//  libtooling
//
//  Created by lsakka on 5/3/16.
//  Copyright © 2016 lsakka. All rights reserved.
//

#ifndef DependenceAnalyser_hpp
#define DependenceAnalyser_hpp

#include <stdio.h>
#include "dependence_graph.h"
#include "function_analyser.h"
#include "access_path.h"
//#include "accesspath_set.h"

/*this class is responsible to do build the dependece graph for a given sequence of traversals
  using the public method createDependnceGraph .
 
 note1: it is stateless and can be converted to a static set of methods.
 
 note3: this process is the most time consuming during in the transformation and it is not written in the most efficient way!.
 */
class DependenceAnalyser{
    
public:
    
    DDG *  createDependnceGraph(vector<FunctionAnalyser * > traversals);
    
private:
    
    void addIntraTraversalDependecies(DDG * depGraph , FunctionAnalyser * traversal , map<StatmentInfo* , DG_Node * > &stmtToGNode);
    
    
    void addInterTraversalDependecies(DDG * depGraph , FunctionAnalyser * traversal1,FunctionAnalyser * traversal2 , map<StatmentInfo* , DG_Node * > & stmtToGNodeT1 ,
                                      map<StatmentInfo* , DG_Node * > & stmtToGNodeT2  );
    
    void addStmtToStmtDependencies(DDG * depGraph, DG_Node * s1Node, DG_Node * s2Node,const _def_AccessPathSet & s1ApSet,const  _def_AccessPathSet &  s2ApSet , bool intra);
        
    bool haveSameValuePart(AccessPath * ap1, AccessPath * ap2 );
    
    void addStmtToCallDeps(DDG * depGraph, DG_Node * s1Node, DG_Node * c2Node);
   
    void addCallToStmtDeps(DDG * depGraph, DG_Node * c1Node, DG_Node * s2Node);
    
    void addCallToCallDeps(DDG * depGraph, DG_Node * c1Node, DG_Node * c2Node,bool intra);

    void addStmtToCallDeps_helper(_def_AccessPathIterator ap1,_def_AccessPathIterator ap2,   FunctionAnalyser *  f2,DDG * depGraph,DG_Node * s1Node,DG_Node * c2Node);
   
    void  addCallToStmtDeps_helper(_def_AccessPathIterator ap1,_def_AccessPathIterator ap2,   FunctionAnalyser *  f2,DDG * depGraph,DG_Node * s1Node,DG_Node * c2Node);

    
    };
#endif /* DependenceAnalyser_hpp */
