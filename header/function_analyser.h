

#ifndef FunctionAnalyser_hpp
#define FunctionAnalyser_hpp

#include "llvmdep.h"
#include <string>
#include "access_path.h"
//#include "accesspath_set.h"
#include "record_analyser.h"
#include "statemant_info.h"
class StatmentInfo;
using namespace clang;
using namespace std;

/**This class is responbile for traversing the functions with fuse attribute that passed the fuse seamntic checks,
 it will soters access paths of each functions and their types and other informations that are needed for dependece 
 checks
 
 */
class AccessPath;
class accessPath_compare;

class FunctionAnalyser {
    friend class AccessPath;
public:
    
    FunctionAnalyser(clang::FunctionDecl * funDeclaration);
    
    //is checkers
    bool isInCalledChildList(clang::FieldDecl * childDecl )const;
    bool isInChildList(clang::ValueDecl * decl) const;
    bool isValidFuse()const {
        return semanticsStasified;
    }
    
    //getters
    int   getNumberOfRecursiveCalls()const ;
    clang::FunctionDecl *               getFunctioneDecl()  const ;
    const clang::VarDecl *              getRootDecl()       const ;
    const clang::RecordDecl *           getRootRecordDecl() const ;
    const vector<clang::FieldDecl *> &  getCalledChildsList()  const ;

    void addToCalledChildList(clang::FieldDecl * childDecl ){
        calledChildrenOrderedList.push_back(childDecl);
    }
    
    //TODO : make them private
    //vector<AccessPathSet * > onTreeAccessPathsArr;
    //AccessPathSet *  offTreeAccessPaths;
    vector<StatmentInfo *> statments;

    ~FunctionAnalyser();
private:
    bool collectAccessPath_handleStmt(clang::Stmt * stmt);
    bool collectAccessPath_handleSubExpr(clang::Expr * expr);
    bool collectAccessPath_VisitCompoundStmt(clang::CompoundStmt* S);
    bool collectAccessPath_VisitCallExpr(clang::CallExpr * S);
    bool collectAccessPath_VisitBinaryOperator(clang::BinaryOperator * s);
    bool collectAccessPath_VisitIfStmt(clang::IfStmt * S);
    bool collectAccessPath_VisitDeclsStmt(clang::DeclStmt * S);
    bool collectAccessPath_VisitParenExpr(clang::ParenExpr * S);
    
    bool collectAccessPath_VisitCXXMemberCallExpr(clang::CXXMemberCallExpr *s );
    
    void addAccessPath(AccessPath * accessPath, bool isRead);
    bool checkFuseSema();

    
    
    //private data members
    clang::FunctionDecl * funDecNode;
    clang::VarDecl * rootDecl;
    const clang::RecordDecl * rootNodeRecordDecl;
    
    StatmentInfo * curStatmentInfo = NULL;
    int insideIfDepth=0;
    
    vector<clang::FieldDecl * > calledChildrenOrderedList;
   // void setStartEndPointsOfAccessPathSets();

    bool semanticsStasified=true;
    
};



#endif /* FunctionAnalyser_hpp */
